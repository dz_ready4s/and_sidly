package com.silvermedia.sidly.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BootReceivedHandler extends BroadcastReceiver {
	private static final Logger LOGGER = LoggerFactory.getLogger(BootReceivedHandler.class);

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			App.getInstance().getDeviceInformation();
			LOGGER.info("Device boot, application initialized");
		}
	}
}
