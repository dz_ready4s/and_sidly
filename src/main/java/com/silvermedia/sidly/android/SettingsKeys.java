package com.silvermedia.sidly.android;

public class SettingsKeys {
	public static final String PREF_API_URL = "pref_api_url";

	public static final String PREF_BAND_DETAILS_TIME_RANGE = "pref_band_details_time_range";

	public static final String PREF_BAND_DISPLAY = "pref_band_display";

	public static final String PREF_CATEGORY_DEVELOPMENT = "pref_category_development";

	public static final String PREF_HELPDESK_URL = App.getInstance().getString(R.string.pref_helpdesk_url_address);

	public static final String PREF_NOTIFICATIONS_EMERGENCY = "pref_notifications_emergency";

	public static final String PREF_NOTIFICATIONS_NEW_MEASUREMENTS = "pref_notifications_new_measurements";

	public static final String PREF_PASSWORD_CHANGE_URL = "pref_password_change_url";

	public static final String PREF_PRIVACY_POLICY_URL = "pref_privacy_policy_url";

	public static final String PREF_REGISTER_URL = "pref_register_url";

	public static final String PREF_TAC_URL = "pref_tac_url";

	public static final String RT_CURRENT_USER = "rt_current_user";

	public static final String RT_GCM_INSTANCE_ID = "rt_instance_id";

	public static final String RT_SEEN_DRAWER = "rt_seen_drawer";


	private SettingsKeys() {}
}
