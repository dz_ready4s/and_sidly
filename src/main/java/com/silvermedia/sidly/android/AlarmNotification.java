package com.silvermedia.sidly.android;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;

import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.MainActivity;
import com.silvermedia.sidly.android.cdm.BandStatusNotification;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.NewMeasurementNotification;
import com.silvermedia.sidly.android.cdm.UpgradeFirmwareNotification;
import com.silvermedia.sidly.android.util.BandFirmwareUpgradeUtil;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Random;

public class AlarmNotification {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlarmNotification.class);

    public static final String NOTIFICATION = "notificationType";
    public static final String UPDATE_INFORMATION = "dashboardUpdate";
    private String CHANNEL_ID = "channel_01";

    public static final int FIRMWARE_NOTIFICATION = 10;

    private final com.silvermedia.sidly.android.cdm.base.Notification notification;

    private final App app;

    public AlarmNotification(com.silvermedia.sidly.android.cdm.base.Notification notification) {
        this.notification = notification;
        this.app = App.getInstance();
    }

    public static com.silvermedia.sidly.android.cdm.base.Notification parse(String message) throws JsonException {
        if (message.contains("newFirmwareVersion")
                && !message.contains("displayName")
                && message.contains("oldFirmwareVersion")) {
            LOGGER.info("UpgradeFirmwareNotification");
            return UpgradeFirmwareNotification
                    .fromString(message, UpgradeFirmwareNotification.class);
        } else if((message.contains("notificationType\":5")) || (message.contains("notificationType\":6"))
                || (message.contains("notificationType\":7")) || (message.contains("notificationType\":8"))
        || (message.contains("notificationType\":9"))){
            return BandStatusNotification.fromString(message, BandStatusNotification.class);
        }else{
            LOGGER.info("NewMeasurementNotification");
            return NewMeasurementNotification.fromString(message, NewMeasurementNotification.class);
        }
    }

    public void show() {
        if (notification instanceof NewMeasurementNotification) {
            showMeasurementNotification();
        } else if (notification instanceof UpgradeFirmwareNotification) {
            BandFirmwareUpgradeUtil.getInstance().resetCheckFirmwareUpgrade();
            showUpgradeNotification();
        } else  if (notification instanceof BandStatusNotification){
            showBandStatusNotification();
        } else {
            LOGGER.warn("Unsupported notification type");
        }
    }

    public void showMeasurementNotification() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app);
        boolean notifyAboutMeasurements = preferences.getBoolean(SettingsKeys.PREF_NOTIFICATIONS_NEW_MEASUREMENTS, true);
        boolean notifyAboutAlarms = preferences.getBoolean(SettingsKeys.PREF_NOTIFICATIONS_EMERGENCY, true);
        NewMeasurementNotification newMeasurementNotification = (NewMeasurementNotification) notification;
        if ((newMeasurementNotification.isFall() || newMeasurementNotification.isSos())
                && !notifyAboutAlarms) {
            LOGGER.info("Silencing notification about fall/sos due to user preference");
            return;
        }
        if (!(newMeasurementNotification.isSos() || newMeasurementNotification.isFall())
                && !notifyAboutMeasurements) {
            LOGGER.info("Silencing notification about measurement due to user preference");
            return;
        }

        NotificationManager notificationManager = ((NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE));
        Notification.Builder builder = new Notification.Builder(app);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = app.getResources().getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
            builder.setChannelId(CHANNEL_ID);
        }
        builder.setAutoCancel(true);
        builder.setContentTitle(app.getString(R.string.app_name));
        builder.setOngoing(false);


        final int displayBandAs = Integer.parseInt(preferences.getString(SettingsKeys.PREF_BAND_DISPLAY, "0"));
        final String displayName;
        switch (displayBandAs) {
            case 1:
                displayName = newMeasurementNotification.getLastName() + " " + newMeasurementNotification.getFirstName();
                break;
            case 2:
                displayName = newMeasurementNotification.getFirstName() + " " + newMeasurementNotification.getLastName();
                break;
            case 0:
                displayName = newMeasurementNotification.getDisplayName();
                break;
            default:
                displayName = newMeasurementNotification.getDisplayName();
                break;
        }

        Intent intent = new Intent(app, BandDetailsActivity.class);
        intent.putExtra(BandDetailsActivity.IMEI_KEY, newMeasurementNotification.getImei());
        intent.putExtra(BandDetailsActivity.DISPLAY_NAME_KEY, displayName);
        intent.putExtra(BandDetailsActivity.IS_BASIC, newMeasurementNotification.isBasic());

        if ((newMeasurementNotification.isFall() || newMeasurementNotification.isSos())
                && !notifyAboutAlarms) {
            LOGGER.info("Silencing notification about fall/sos due to user preference");
            return;
        }

        if (newMeasurementNotification.isFall() || newMeasurementNotification.isSos()){
            intent.putExtra(BandDetailsActivity.DISPLAY_ALARM_DIALOG, true);
            builder.setContentIntent(PendingIntent.getActivity(app, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        }else{
            intent.putExtra(BandDetailsActivity.DISPLAY_ALARM_DIALOG, false);
            builder.setContentIntent(PendingIntent.getActivity(app, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        }


        if(newMeasurementNotification.getSound() == 1)
            builder.setSound(Uri.parse("android.resource://"+app.getPackageName()+"/"+R.raw.notification));
        else
            builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        int notificationId;
        if (newMeasurementNotification.isFall() || newMeasurementNotification.isSos() ||
                newMeasurementNotification.isZone()) {
            // This is FALL or SOS notification
            notificationId = (int) newMeasurementNotification.getImei() + new Random().nextInt();

            builder.setDefaults(Notification.DEFAULT_VIBRATE);
            builder.setPriority(Notification.PRIORITY_MAX);
            builder.setSmallIcon(R.drawable.ic_stat_warning);
            builder.setLights(0xFFFFFFFF, 500, 250);
        } else {
            // This is timer- or manual-triggered measurement
            notificationId = (int) newMeasurementNotification.getImei() + new Random().nextInt();

            builder.setDefaults(Notification.DEFAULT_VIBRATE);
            builder.setPriority(Notification.PRIORITY_LOW);
            builder.setSmallIcon(R.drawable.ic_stat_info_outline);
        }

        if (newMeasurementNotification.isWearError()) {
            int stringId = R.string.notification_new_measurement_not_worn_correctly;
            if (newMeasurementNotification.isFall()) {
                stringId = R.string.notification_new_fall_not_worn_correctly;
            }
            if (newMeasurementNotification.isSos()) {
                stringId = R.string.notification_new_sos_not_worn_correctly;
            }
            if (newMeasurementNotification.isZone()) {
                stringId = R.string.notification_new_alarmzone_not_worn_correctly;
            }
            if(newMeasurementNotification.isBasic()) {
                builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId), displayName));
                builder.setStyle(new Notification.BigTextStyle().bigText(String.format(Locale.getDefault(), app.getString(stringId), displayName)));
            }else {
                builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId), displayName) + app.getString(R.string.notification_not_worn_correctly_substring));
                builder.setStyle(new Notification.BigTextStyle().bigText(String.format(Locale.getDefault(), app.getString(stringId), displayName)
                        + app.getString(R.string.notification_not_worn_correctly_substring)));
            }

        } else {
            int stringId = R.string.notification_new_measurement;
            if (newMeasurementNotification.isPulseExceeded()){
                stringId = R.string.notification_new_pulse_exceeded;
            }
            if (newMeasurementNotification.isFall()) {
                stringId = R.string.notification_new_fall;
            }
            if (newMeasurementNotification.isSos()) {
                stringId = R.string.notification_new_sos;
            }
            if (newMeasurementNotification.isZone()) {
                stringId = R.string.notification_new_alarm_zone;
            }

            if (newMeasurementNotification.isFall() || newMeasurementNotification.isSos() || newMeasurementNotification.isZone() || newMeasurementNotification.isPulseExceeded()){
                if(newMeasurementNotification.isBasic()) {
                    builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId).substring(0,app.getString(stringId).length()-1), newMeasurementNotification.getPulse(),
                            displayName));
                }else {
                    builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId), newMeasurementNotification.getPulse(),
                            displayName) + " " + ImperialUtil.getStringTemperature(newMeasurementNotification.getTemperature()));
                    builder.setStyle(new Notification.BigTextStyle().bigText(String.format(Locale.getDefault(), app.getString(stringId), newMeasurementNotification.getPulse(),
                            displayName) + " " + ImperialUtil.getStringTemperature(newMeasurementNotification.getTemperature())));
                }
            }else {
                builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId), newMeasurementNotification.getPulse(),
                        displayName)+ " " + ImperialUtil.getStringTemperature(newMeasurementNotification.getTemperature()));
                builder.setStyle(new Notification.BigTextStyle().bigText(String.format(Locale.getDefault(), app.getString(stringId), newMeasurementNotification.getPulse(),
                        displayName) + " " + ImperialUtil.getStringTemperature(newMeasurementNotification.getTemperature())));
            }
        }

        app.getAssociatedBandsCache().invalidateAll();
        notificationManager.notify(notificationId, builder.build());

        if(newMeasurementNotification.isFall() || newMeasurementNotification.isSos()){
            Intent broadcastIntent = new Intent(MainActivity.FILTER_UPDATE_UI);
            LocalBroadcastManager.getInstance(App.getInstance().getApplicationContext())
                    .sendBroadcast(broadcastIntent);
        }
    }

    public void showBandStatusNotification(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app);
        boolean notifyAboutAlarms = preferences.getBoolean(SettingsKeys.PREF_NOTIFICATIONS_EMERGENCY, true);
        BandStatusNotification newBandStatusNotification = (BandStatusNotification) notification;
        if (!notifyAboutAlarms){
            LOGGER.info("Silencing notification about measurement due to user preference");
            return;
        }

        NotificationManager notificationManager = ((NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE));
        Notification.Builder builder = new Notification.Builder(app);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = app.getResources().getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
            builder.setChannelId(CHANNEL_ID);
        }

        builder.setAutoCancel(true);
        builder.setContentTitle(app.getString(R.string.app_name));
        builder.setOngoing(false);

        final int displayBandAs = Integer.parseInt(preferences.getString(SettingsKeys.PREF_BAND_DISPLAY, "0"));
        final String displayName;
        switch (displayBandAs) {
            case 1:
                displayName = newBandStatusNotification.getLastName() + " " + newBandStatusNotification.getFirstName();
                break;
            case 2:
                displayName = newBandStatusNotification.getFirstName() + " " + newBandStatusNotification.getLastName();
                break;
            case 0:
            default:
                displayName = newBandStatusNotification.getDisplayName();
                break;
        }

        Intent intent = new Intent(app, BandDetailsActivity.class);
        intent.putExtra(BandDetailsActivity.IMEI_KEY, newBandStatusNotification.getImei());
        intent.putExtra(BandDetailsActivity.DISPLAY_NAME_KEY, displayName);
        intent.putExtra(BandDetailsActivity.IS_BASIC, newBandStatusNotification.isBasic());
        builder.setContentIntent(PendingIntent.getActivity(app, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));


        int notificationId;

            notificationId = (int) newBandStatusNotification.getImei() + new Random().nextInt();

            builder.setDefaults(Notification.DEFAULT_VIBRATE);
            builder.setPriority(Notification.PRIORITY_MAX);
            builder.setSmallIcon(R.drawable.ic_stat_info_outline);
            builder.setLights(0xFFFFFFFF, 500, 250);
            if(newBandStatusNotification.getSound() == 1)
                builder.setSound(Uri.parse("android.resource://"+app.getPackageName()+"/"+R.raw.notification));
            else
                builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);


        int stringId;

        switch(newBandStatusNotification.getNotificationType()){
            case 5:
                stringId = R.string.notification_new_band_status_not_worn;
                break;
            case 6:
                stringId = R.string.notification_new_band_status_worn;
                break;
            case 8:
                 stringId = R.string.notification_new_band_status_discharged;
                break;
            case 7:
                stringId = R.string.notification_new_band_status_fullcharged;
                break;
            case 9:
                stringId = R.string.notification_new_band_status_wrong_bpm;
                break;
            default:
                stringId = R.string.notification_new_band_status_wrong_parameter;
        }

            builder.setContentText(String.format(Locale.getDefault(), app.getString(stringId), displayName));
            builder.setStyle(new Notification.BigTextStyle().bigText(String.format(Locale.getDefault(), app.getString(stringId), displayName)));

        app.getAssociatedBandsCache().invalidateAll();
        notificationManager.notify(notificationId, builder.build());

    }

    public void showUpgradeNotification() {
        NotificationManager notificationManager = ((NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE));
        Notification.Builder builder = new Notification.Builder(app);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = app.getResources().getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
            builder.setChannelId(CHANNEL_ID);
        }

        builder.setAutoCancel(true);
        builder.setContentTitle(app.getString(R.string.app_name));
        builder.setOngoing(false);

        builder.setDefaults(Notification.DEFAULT_VIBRATE);
        builder.setPriority(Notification.PRIORITY_DEFAULT);
        builder.setSmallIcon(R.drawable.ic_stat_info_outline);
        builder.setSound(Uri.parse("android.resource://"+app.getPackageName()+"/"+R.raw.notification));
        builder.setContentText(app.getString(R.string.notification_new_firmware_update));
        builder.setStyle(new Notification.BigTextStyle().bigText(app.getString(R.string.notification_new_firmware_update)));

        Intent intent = new Intent(app, MainActivity.class);
        intent.putExtra(NOTIFICATION, FIRMWARE_NOTIFICATION);
        builder.setContentIntent(PendingIntent.getActivity(app, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        LOGGER.warn("showUpgradeNotification");
        notificationManager.notify(FIRMWARE_NOTIFICATION, builder.build());
    }
}
