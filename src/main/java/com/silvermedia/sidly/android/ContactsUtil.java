package com.silvermedia.sidly.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContactsUtil {
	public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

	private static final Logger LOGGER = LoggerFactory.getLogger(ContactsUtil.class);

	private final Context context;

	public ContactsUtil(Context context) {
		this.context = context;
	}

	public String getDisplayNameForNumber(String phoneNumber) throws SecurityException {
		if(hasPhoneCapabilities(context)){
			Cursor cursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode
					(phoneNumber)), new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

			if (!cursor.moveToFirst()) {
				LOGGER.info("Could not resolve {} to a display name", phoneNumber);
				return phoneNumber;
			}

			String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
			cursor.close();
			if (displayName == null || displayName.isEmpty()) {
				return phoneNumber;
			}
			return String.format(context.getString(R.string.format_contact), displayName, phoneNumber);
		}
		else{
			return phoneNumber;
		}
	}

	public static boolean hasPhoneCapabilities(Context context){
		TelephonyManager tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
		if(tm.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
			return false;
		}else{
			return true;
		}
	}

	public static boolean checkPermissions(Activity activity){
		if (ContextCompat.checkSelfPermission(activity,
				android.Manifest.permission.READ_CONTACTS)
				!= PackageManager.PERMISSION_GRANTED) {
			// No explanation needed, we can request the permission.
			ActivityCompat.requestPermissions(activity,
					new String[]{android.Manifest.permission.READ_CONTACTS},
					ContactsUtil.MY_PERMISSIONS_REQUEST_READ_CONTACTS);
			return false;
		} else {
			return true;
		}
	}
}
