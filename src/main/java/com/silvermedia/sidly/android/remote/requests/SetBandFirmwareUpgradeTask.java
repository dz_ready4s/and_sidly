package com.silvermedia.sidly.android.remote.requests;

import android.os.AsyncTask;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.SetBandFirmwareUpgradeStateRequest;
import com.silvermedia.sidly.android.cdm.SetBandFirmwareUpgradeStateResponse;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-12.
 */
public class SetBandFirmwareUpgradeTask extends AsyncTask<Integer,Void,Void> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetBandFirmwareUpgradeTask.class);
    public static final int STATE_UPGRADE = 3;
    public static final int STATE_CANCEL = 2;

    @Override
    protected Void doInBackground(Integer... params) {
        String json;
        App app = App.getInstance();
        SetBandFirmwareUpgradeStateResponse response = null;
        if(params.length==0 || (params[0]!=STATE_CANCEL && params[0]!=STATE_UPGRADE)){
            LOGGER.error("Bad state params");
            return null;
        }
        try {
            json = new HttpClient(ApiEndPoint.SET_BAND_FIRMWARE_STATE)
                    .doPost(new SetBandFirmwareUpgradeStateRequest(app.getCurrentUser().getUserId(),
                            app.getCurrentUser().getAuthenticationToken(),
                            params[0]));

            LOGGER.debug("JSON: {}", json.toString());
            response = SetBandFirmwareUpgradeStateResponse
                    .fromString(json, SetBandFirmwareUpgradeStateResponse.class);
        } catch (IOException e) {
            LOGGER.error("Failed to get a list of bands, server issue", e);
            return null;
        } catch (JsonException e) {
            LOGGER.error("Failed to deserialize JSON data", e);
            return null;
        }

        if (isCancelled()) {
            return null;
        }

        LOGGER.info("Server returned ", "Success: " + response.isSuccess());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
