package com.silvermedia.sidly.android.remote.requests;

import android.os.AsyncTask;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.SetTimezoneRequest;
import com.silvermedia.sidly.android.cdm.SetTimezoneResponse;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.TimeZone;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-04-12.
 */
public class SetTimezoneTask extends AsyncTask<String, Void, Void> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetTimezoneTask.class);

    @Override
    protected Void doInBackground(String... params) {

        String json;
        App app = App.getInstance();
        TimeZone timeZone = TimeZone.getDefault();
        SetTimezoneResponse response = null;
        LOGGER.info("Timezone:", timeZone.getID());

        try {
            json = new HttpClient(ApiEndPoint.SET_TIMEZONE)
                    .doPost(new SetTimezoneRequest(timeZone.getID(),
                            app.getCurrentUser().getAuthenticationToken(),
                            app.getCurrentUser().getUserId()));

            LOGGER.debug("JSON: {}", json.toString());
            response = SetTimezoneResponse.fromString(json, SetTimezoneResponse.class);
        } catch (IOException e) {
            LOGGER.error("Failed to get a list of bands, server issue", e);
            return null;
        } catch (JsonException e) {
            LOGGER.error("Failed to deserialize JSON data", e);
            return null;
        }

        if (isCancelled()) {
            return null;
        }

        LOGGER.info("Server returned ", "Success: " + response.isSuccess());
        return null;
    }
}
