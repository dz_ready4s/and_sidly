package com.silvermedia.sidly.android.remote;

import android.os.AsyncTask;

import com.silvermedia.sidly.android.cdm.AssociateBandRequest;
import com.silvermedia.sidly.android.cdm.GetErrorResponse;
import com.silvermedia.sidly.android.cdm.JsonException;

/**
 * Created by dabu on 09.02.16.
 */

public class ApiResponseAddBandTask extends AsyncTask<AssociateBandRequest, Void, Void> {
    private GetErrorResponse getErrorResponse;

    @Override
    protected Void doInBackground(AssociateBandRequest... params) {
        return null;
    }

    public boolean isApiResponseCorrect(String json){
        try{
            getErrorResponse = GetErrorResponse.fromString(json, GetErrorResponse.class);
        }catch(JsonException e){
            return false;
        }
        if(getErrorResponse.getError() != null) {
            return false;
        }
        return true;
    }

}