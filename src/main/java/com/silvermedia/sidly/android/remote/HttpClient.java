package com.silvermedia.sidly.android.remote;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.cdm.base.Request;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpClient.class);

	private final URL url;

	public HttpClient(ApiEndPoint apiEndPoint) throws IOException {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(App.getInstance());
		String urlBase = App.getInstance().getString(R.string.api_url);//preferences.getString (SettingsKeys .PREF_API_URL, null);
		if (urlBase == null) {
			throw new IOException("Failed to obtain URL base");
		}
		if (!urlBase.endsWith("/")) {
			urlBase += "/";
		}

		url = new URL(urlBase + apiEndPoint);
		LOGGER.info("Prepared HTTP connection to {}", url);
	}

	public String doPost(Request payload) throws IOException {
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();

		String jsonPayload = payload.toJson();
		String serverResponse;
		try {
			connection.setDoInput(true);
            connection.setDoOutput(true);
			//connection.setChunkedStreamingMode(0);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			//connection.setRequestProperty("Content-Length", Integer.toString(300));
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(60000);
			connection.setFixedLengthStreamingMode(jsonPayload.getBytes().length);

			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			LOGGER.debug("Sending: {}", jsonPayload);
			writer.write(jsonPayload);
			writer.flush();

			int responseCode = connection.getResponseCode();
			if (responseCode != HttpURLConnection.HTTP_OK) {
				connection.disconnect();
				throw new IOException("Server returned error " + responseCode);
			}
			serverResponse = IOUtils.toString(connection.getInputStream());
			LOGGER.debug("Response: {}", serverResponse);
		} finally {
			connection.disconnect();
		}

		return serverResponse;
	}
}
