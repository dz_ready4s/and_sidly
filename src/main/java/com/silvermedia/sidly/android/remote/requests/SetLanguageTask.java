package com.silvermedia.sidly.android.remote.requests;

import android.os.AsyncTask;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.SetLanguageRequest;
import com.silvermedia.sidly.android.cdm.SetLanguageResponse;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-17.
 */
public class SetLanguageTask extends AsyncTask<String, Void, Void> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetLanguageTask.class);

    private static String[] AVAILABLE_LANG = {"EN", "PL", "DE", "NL"};

    @Override
    protected Void doInBackground(String... params) {

        String json;
        App app = App.getInstance();
        String lang = Locale.getDefault().getLanguage().toUpperCase();
        LOGGER.info("Get language: {}", lang);
        lang = (Arrays.asList(AVAILABLE_LANG).contains(lang)) ? lang : "EN";
        SetLanguageResponse response = null;
        LOGGER.info("Try to setLanguage: {}", lang);

        try {
            json = new HttpClient(ApiEndPoint.SET_LANGUAGE)
                    .doPost(new SetLanguageRequest(app.getCurrentUser().getUserId(),
                            app.getCurrentUser().getAuthenticationToken(),
                            lang));

            LOGGER.debug("JSON: {}", json.toString());
            response = SetLanguageResponse.fromString(json, SetLanguageResponse.class);
        } catch (IOException e) {
            LOGGER.error("Failed to get a list of bands, server issue", e);
            return null;
        } catch (JsonException e) {
            LOGGER.error("Failed to deserialize JSON data", e);
            return null;
        }

        if (isCancelled()) {
            return null;
        }

        LOGGER.info("Server returned ", "Success: " + response.isSuccess());
        return null;
    }
}
