package com.silvermedia.sidly.android.remote;

public enum ApiEndPoint {
	ASSOCIATE_BAND("AssociateBand"),
	AUTHENTICATION("Authentication"),
	DISASSOCIATE_BAND("DisassociateBand"),
	GET_BAND_OVERVIEW("GetBandOverview"),
	GET_BAND_PROPERTIES("GetBandProperties"),
	GET_FALL_LIST("GetFallList"),
	GET_MEDICINE_ALARM_LIST("GetMedicineAlarmList"),
	GET_WIFI_LIST("GetBandWifiList"),
	GET_PULSE_LIST("GetPulseList"),
	GET_SOS_LIST("GetSosList"),
	GET_ALARM_ZONE_LIST("GetAlarmZoneList"),
	GET_TEMPERATURE_LIST("GetTemperatureList"),
	GET_BAROMETER_LIST("GetBarometerList"),
	GET_STEP_LIST("GetStepList"),
	LIST_BANDS("ListBands"),
	SET_BAND_PROPERTIES("SetBandProperties"),
	SET_MEDICINE_ALARM_LIST("SetMedicineAlarmList"),
	SET_WIFI_LIST("SetBandWifiList"),
	UPDATE_INSTANCE_ID("UpdateInstanceId"),
	SET_TIMEZONE("SetTimezone"),
	GET_BAND_FIRMWARE_UPGRADE("GetBandFirmwareUpgrade"),
	SET_BAND_FIRMWARE_STATE("SetBandFirmwareUpgradeState"),
	SET_LANGUAGE("SetLanguage"),
	SET_ACCEPTED_ALARM("HandleAlarm");

	private final String path;

	ApiEndPoint(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return path;
	}
}
