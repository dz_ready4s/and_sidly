package com.silvermedia.sidly.android.remote.requests;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.cdm.GetBandFirmwareUpgradeRequest;
import com.silvermedia.sidly.android.cdm.GetBandFirmwareUpgradeResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-12.
 */
public class GetBandFirmwareUpgradeTask extends AsyncTask<Void, Void, GetBandFirmwareUpgradeResponse> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetTimezoneTask.class);

    @Override
    protected GetBandFirmwareUpgradeResponse doInBackground(Void... params) {
        String json;
        App app = App.getInstance();
        GetBandFirmwareUpgradeResponse response = null;

        try {
            json = new HttpClient(ApiEndPoint.GET_BAND_FIRMWARE_UPGRADE)
                    .doPost(new GetBandFirmwareUpgradeRequest(app.getCurrentUser().getUserId(),
                            app.getCurrentUser().getAuthenticationToken()));

            LOGGER.debug("JSON: {}", json.toString());
            response = GetBandFirmwareUpgradeResponse
                    .fromString(json, GetBandFirmwareUpgradeResponse.class);
        } catch (IOException e) {
            LOGGER.error("Failed to get a list of bands, server issue", e);
            return null;
        } catch (JsonException e) {
            LOGGER.error("Failed to deserialize JSON data", e);
            return null;
        }

        if (isCancelled()) {
            return null;
        }

        LOGGER.info("Server returned ", "Success: " + response.isSuccess());
        return response;
    }

    @Override
    protected void onPostExecute(GetBandFirmwareUpgradeResponse response) {
        super.onPostExecute(response);
        if (response == null) {
            return;
        }
        if (!response.isSuccess()) {
            return;
        }
        if (response.isCanUpgradeFirmware()) {
//            Toast.makeText(App.getInstance().getApplicationContext(), "Upgrade Available",
//                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(BaseActivity.FILTER_UPGRADE);
            intent.putExtra(BaseActivity.UPGRADE_MESSAGE, response.getMessage());
            LocalBroadcastManager.getInstance(App.getInstance().getApplicationContext())
                    .sendBroadcast(intent);
        } else {
            LOGGER.debug("Band upgrade not available");
        }
    }

}
