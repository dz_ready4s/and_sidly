package com.silvermedia.sidly.android;

public class CurrentUser {
	private final long userId;

	private final String authenticationToken;

	private final boolean isPremium;

	private final boolean isImperial;

	private final boolean isUser;

	public CurrentUser(long userId, String authenticationToken, boolean isPremium, boolean type) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.isPremium = isPremium;
		this.isImperial = true;
		this.isUser = type;
	}

	@Override
	public String toString() {
		return "CurrentUser{userId=" + userId + ", authenticationToken='" + authenticationToken + '\'' + '}';
	}

	public long getUserId() {
		return userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public boolean isUser(){return isUser;}
}
