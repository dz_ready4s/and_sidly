package com.silvermedia.sidly.android.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.ColorUtil;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.cdm.GetStepListRequest;
import com.silvermedia.sidly.android.cdm.GetStepListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.PieChartView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BandDetailsPedometerFragment extends Fragment {
    private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsPedometerFragment.class);

    private View root;
    private int highlightPosition = -1;
    private ListView mListViewSteps;

    public BandDetailsPedometerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.band_details_pedometer_fragment, container, false);

        root.findViewById(R.id.band_details_pedometer_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        return root;
    }

/*

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d("FunctionOrder", "setUserVisibleHint");
        if (isVisibleToUser) {
            ColumnChartView columnChartView = (ColumnChartView)root.findViewById(R.id.band_details_pedometer_fragment_chart_column);
            //columnChartView.setCurrentViewportWithAnimation(columnChartView.getMaximumViewport());
            Log.d("FunctionOrder", "columnVisible" + columnChartView.getMaximumViewport());
        }
    }
*/

    @SuppressWarnings("unchecked")
    private void loadingFinished(GetStepListResponse getStepListResponse) {
        root.findViewById(R.id.band_details_pedometer_fragment).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        if (getStepListResponse.getStepList().isEmpty()) {
            root.findViewById(R.id.band_details_pedometer_fragment_chart_column).setVisibility(View.GONE);
            root.findViewById(R.id.band_details_pedometer_fragment_chart_pie).setVisibility(View.GONE);
            root.findViewById(R.id.band_details_pedometer_fragment_percent).setVisibility(View.GONE);
            root.findViewById(R.id.band_details_pedometer_title).setVisibility(View.GONE);
            root.findViewById(R.id.band_details_pedometer_fragment_no_data).setVisibility(View.VISIBLE);
            return;
        }
        root.findViewById(R.id.band_details_pedometer_fragment_chart_column).setVisibility(View.VISIBLE);
        root.findViewById(R.id.band_details_pedometer_fragment_chart_pie).setVisibility(View.VISIBLE);
        root.findViewById(R.id.band_details_pedometer_fragment_percent).setVisibility(View.VISIBLE);
        root.findViewById(R.id.band_details_pedometer_fragment_no_data).setVisibility(View.GONE);

        mListViewSteps = (ListView)root.findViewById(R.id.band_details_pedometer_fragment_list);

        List<GetStepListResponse.Step> stepList = new ArrayList<>(getStepListResponse.getStepList());
        Collections.reverse(stepList);

        mListViewSteps.setAdapter(new ArrayAdapter<GetStepListResponse.Step>(getActivity(), R.layout.list_entry_band_details_value, R.id
                .list_entry_band_details_value_date, stepList) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                GetStepListResponse.Step step = getItem(position);
                DateTime dateTime = new DateTime(step.getTimestampMillis());
                ((TextView) view.findViewById(R.id.list_entry_band_details_value_date)).setText(ImperialUtil.getYearsDate(dateTime));
                ((TextView) view.findViewById(R.id.list_entry_band_details_value_value)).setText(String.valueOf(step.getSteps()));

                view.setBackgroundColor(ColorUtil.getColor(getActivity(), android.R.color.background_light));
                if (highlightPosition == position) {
                    view.setBackgroundColor(ColorUtil.getColor(getActivity(), R.color.accent_faded));
                }

                return view;
            }
        });
        //Collections.reverse(getStepListResponse.getStepList());
        initializePieChart(getStepListResponse);
        initializeColumnChart(getStepListResponse);

    }

    private long getStartDayInMillis(){
        Calendar cal = Calendar.getInstance();
        int year  = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date  = cal.get(Calendar.DATE);
        cal.clear();
        cal.set(year, month, date);
        return cal.getTimeInMillis();
    }

    private void initializePieChart(GetStepListResponse getStepListResponse){
        final PieChartView chart = (PieChartView)root.findViewById(R.id.band_details_pedometer_fragment_chart_pie);
        TextView percentText = (TextView)root.findViewById(R.id.band_details_pedometer_fragment_percent);
        chart.setInteractive(false);
        PieChartData data;
        List<SliceValue> values = new ArrayList<>();

        float maxValue = Float.MIN_VALUE;
        int daySteps = 0;
        for(GetStepListResponse.Step step : getStepListResponse.getStepList()){
            maxValue = Math.max(step.getSteps(), maxValue);
            if(step.getTimestampMillis() >= getStartDayInMillis()){
                daySteps += step.getSteps();
            }
        }

        float dailySteps = getStepListResponse.getStepList().get(0).getDailySteps();
        if(dailySteps == 0){
            dailySteps = 2000;
        }
        float max = 100;
        float percent = ((float)daySteps / (float)dailySteps) * (float)100;
        float other = max - percent;

        SliceValue percentValue;
        SliceValue otherValue;
        if(percent < max){
            percentValue = new SliceValue(percent, ColorUtil.getColor(getActivity(), R.color.accent));
            otherValue = new SliceValue(other, ColorUtil.getColor(getActivity(), R.color.foreground));
        } else {
            percentValue = new SliceValue(max, ColorUtil.getColor(getActivity(), R.color.accent));
            otherValue = new SliceValue(0, ColorUtil.getColor(getActivity(), R.color.foreground));
        }
        values.add(percentValue);
        values.add(otherValue);

        data = new PieChartData(values);
        data.setHasLabels(false);
        data.setHasLabelsOnlyForSelected(false);
        data.setHasLabelsOutside(false);
        data.setHasCenterCircle(true);
        data.setCenterCircleScale(0.9f);

        percentText.setText(String.format(Locale.US, "%.1f",percent) + "%");

        chart.setPieChartData(data);
        chart.setChartRotation(270, false);

        Viewport viewport = chart.getMaximumViewport();
        viewport.top += maxValue * 0.2;
        chart.setCurrentViewport(viewport);
        chart.setMaximumViewport(viewport);
    }

    private void initializeColumnChart(final GetStepListResponse getStepListResponse){
        ColumnChartView chart;
        ColumnChartData data;
        chart = (ColumnChartView)root.findViewById(R.id.band_details_pedometer_fragment_chart_column);

        float minValue = Float.MAX_VALUE;
        float maxValue = Float.MIN_VALUE;
        final List<Column> columns = new ArrayList<>();
        for (GetStepListResponse.Step step :getStepListResponse.getStepList()) {
            List<SubcolumnValue> subcolumns = new ArrayList<>();
            minValue = Math.min(step.getSteps(), minValue);
            maxValue = Math.max(step.getSteps(), maxValue);
            subcolumns.add(new SubcolumnValue(step.getSteps(), ColorUtil.getColor(getActivity(), R.color.accent)));

            Column column = new Column(subcolumns);
            column.setHasLabelsOnlyForSelected(false);
            columns.add(column);
        }

        List<AxisValue> axisValues = new ArrayList<>();
        int size = 0;
        if (getStepListResponse.getStepList().size() <= 4) {
            for (GetStepListResponse.Step step : getStepListResponse.getStepList()) {
                axisValues.add(new AxisValue(size++).setLabel(
                        ImperialUtil.getYearsDate(new DateTime(step.getTimestampMillis()))));
            }
        } else {
            for (int i = 0; i < columns.size(); i += columns.size() / 3.2) {
                AxisValue axisValue = new AxisValue(i);
                axisValue.setLabel(
                        ImperialUtil.getYearsDate(new DateTime(getStepListResponse.getStepList().get(i).getTimestampMillis())));
                axisValues.add(axisValue);
            }
        }

        data = new ColumnChartData(columns);
        data.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(false));
        data.setAxisXBottom(new Axis(axisValues).setHasLines(true).setHasTiltedLabels(false).setName(""));

        chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {

            @Override
            public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                int count = getStepListResponse.getStepList().size() - 1;
                int current = count - columnIndex;
                highlightPosition = current;
                mListViewSteps.invalidateViews();
                mListViewSteps.smoothScrollToPosition(current);
            }

            @Override
            public void onValueDeselected() {
            }
        });

        chart.setColumnChartData(data);
        chart.setZoomType(ZoomType.HORIZONTAL);
        chart.setZoomEnabled(true);

        Viewport viewport = chart.getMaximumViewport();
        viewport.top += maxValue * 0.1;
        if(getStepListResponse.getStepList().size() == 1){
            viewport.left -= 4;
            viewport.right += 4;
        } else {
            viewport.left -= columns.size() * 0.05;
            viewport.right += columns.size() * 0.05;
        }
        chart.setCurrentViewport(viewport);
        chart.setMaximumViewport(viewport);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadingFailed() {
        root.findViewById(R.id.band_details_pedometer_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
    }


    private class DataSyncTask extends ApiResponseTask {
        private final long imei;

        public DataSyncTask(long imei) {
            super();

            this.imei = imei;
        }

        @Override
        protected Void doInBackground(Void... params) {
            BandDetailsActivity activity = (BandDetailsActivity)getActivity();
            CurrentUser user = App.getInstance().getCurrentUser();

            try {
                /*int days;
                if(activity.getQueryStartTime() == new DateTime(0).toInstant().getMillis()){
                    days = 999;
                } else {
                    days = (int) ((activity.getQueryEndTime() - activity.getQueryStartTime()) / (1000*60*60*24));
                }*/
                String json = new HttpClient(ApiEndPoint.GET_STEP_LIST).doPost(new GetStepListRequest(user.getUserId(), user
                        .getAuthenticationToken(), imei, activity.getQueryStartTime(), activity.getQueryEndTime()));
                final GetStepListResponse getStepListResponse = GetStepListResponse.fromString(json, GetStepListResponse
                        .class);
                if(isApiResponseCorrect(json)) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFinished(getStepListResponse);
                        }
                    });
                }
            } catch (IOException e) {
                LOGGER.error("Failed to get data from server, server issue", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            } catch (NullPointerException e){
                LOGGER.error("Failed to update user credentials", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            } catch (JsonException e) {
                LOGGER.error("Failed to deserialize JSON data", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            }

            return null;
        }
    }

}
