package com.silvermedia.sidly.android.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.cache.Cache;
import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.activity.AddBandActivity;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.BandSettingsActivity;
import com.silvermedia.sidly.android.activity.MainActivity;
import com.silvermedia.sidly.android.activity.SettingsActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.ListBandsRequest;
import com.silvermedia.sidly.android.cdm.ListBandsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardFragment extends Fragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardFragment.class);

	private static final String CACHE_KEY = "__associatedBands";

	final private ArrayList<ListBandsResponse.BandDetails> associatedBands = new ArrayList<>();

	private ActionBarDrawerToggle actionBarDrawerToggle;

	private DrawerLayout drawerLayout;

	private ListView drawerList;

	private ListBandsTask listBandsTask;

	public DrawerLayout getDrawerLayout() {
		return drawerLayout;
	}


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.dashboard_fragment, container, false);

		drawerLayout = (DrawerLayout)root.findViewById(R.id.dashboard_fragment_drawer_layout);
		drawerList = (ListView)drawerLayout.findViewById(R.id.dashboard_fragment_drawer);
		drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				@SuppressWarnings("unchecked") Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(position);

				if ("static".equals(item.get("type"))) {
					switch ((Integer)item.get("key")) {
						case 1:
							closeDrawer();
							listBandsTask = new ListBandsTask();
							listBandsTask.execute();
							break;
						case 2:
							closeDrawer();
							startActivity(new Intent(getActivity(), SettingsActivity.class));
							break;
						case 3:
							SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SettingsKeys.PREF_HELPDESK_URL)));
							break;
						//Logout
						case 4:
							closeDrawer();
							App.getInstance().setCurrentUser(null);
							App.getInstance().clearAssociatedBandsCache();
							startActivity(new Intent(getActivity(), MainActivity.class));
							break;
						case 5:
							closeDrawer();
							startActivityForResult(new Intent(getActivity(), AddBandActivity.class), 0xf001);
							break;
						default:
							LOGGER.info("Unhandled static button: {}", item);
							break;
					}
					return;
				}
				if ("band".equals(item.get("type"))) {
					closeDrawer();
					showDetailsForBand((Long)item.get("imei"), (String)item.get("text"), (int)item.get("hardwareLocation"), (boolean)item.get("isBasic"));
					return;
				}
				LOGGER.warn("Unhandled drawer click: {}", item);
			}
		});


			actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.dashboard_fragment_drawer_open, R.string
					.dashboard_fragment_drawer_close) {
				@Override
				public void onDrawerOpened(View drawerView) {
					super.onDrawerOpened(drawerView);
					getActivity().invalidateOptionsMenu();
				}

				@Override
				public void onDrawerClosed(View drawerView) {
					super.onDrawerClosed(drawerView);
					getActivity().invalidateOptionsMenu();
				}
			};

		actionBarDrawerToggle.setDrawerIndicatorEnabled(true);


		drawerLayout.setDrawerListener(actionBarDrawerToggle);

		ActionBar actionBar = getActivity().getActionBar();
		if (actionBar != null) {
			if(Build.VERSION.SDK_INT == 16 || Build.VERSION.SDK_INT == 17) {
				LayoutInflater inflator = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View v = inflator.inflate(R.layout.actionbar_custom, null);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setDisplayShowHomeEnabled(false);
				actionBar.setDisplayShowCustomEnabled(true);
				actionBar.setDisplayShowTitleEnabled(false);
				actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
				actionBar.setCustomView(v);
				getActivity().findViewById(R.id.action_bar_custom_button).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (drawerLayout.isDrawerOpen(GravityCompat.START))
							closeDrawer();
						else
							openDrawer();
					}
				});
			}
			else{
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setHomeButtonEnabled(true);
			}
		} else {
			LOGGER.error("Failed to get acti bar for activity from DashboardFragment!");
		}

		Bundle arguments = getArguments();
		if (arguments.containsKey("openDrawer")) {
			openDrawer();
		}
		syncDrawerState();

		Cache<String, List<ListBandsResponse.BandDetails>> cache = App.getInstance().getAssociatedBandsCache();
			List<ListBandsResponse.BandDetails> cachedAssociatedBands = cache.getIfPresent(CACHE_KEY);
			if (cachedAssociatedBands != null && !cachedAssociatedBands.isEmpty()) {
				LOGGER.info("Cache hit for associatedBands");
				associatedBands.addAll(cachedAssociatedBands);
				updateFragmentState(false);
			} else {
				LOGGER.info("Cache miss for associatedBands");
				getActivity().getFragmentManager().beginTransaction().add(R.id.dashboard_fragment_frame, new DashboardLoadingFragment()).commit();
				listBandsTask = new ListBandsTask();
				listBandsTask.execute();
			}

		return root;
	}

	@Override
	public void onResume(){
		super.onResume();
		synchronized (this) {
			new ListBandsTask().execute();
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return actionBarDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.info("onActivityResult({}, {}, {})", Integer.toHexString(requestCode), resultCode, data);
		if (requestCode == 0xf002 || (requestCode == 0xf001 && (resultCode == Activity.RESULT_OK || resultCode == BandSettingsActivity
			.RESULT_DELETED))) {
			LOGGER.info("Forcing cache reload");
			App.getInstance().getAssociatedBandsCache().invalidateAll();
			listBandsTask = new ListBandsTask();
			listBandsTask.execute();
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		synchronized (this) {
			if (listBandsTask != null) {
				listBandsTask.cancel(true);
			}
		}
	}

	public void syncDrawerState() {
		actionBarDrawerToggle.syncState();
	}

	public void showDetailsForBand(long imei, String initialDisplayName, int hardwareLocation, boolean isBasic) {
		LOGGER.info("Showing band details for {}", imei);

		Intent intent = new Intent(getActivity(), BandDetailsActivity.class);
		intent.putExtra(BandDetailsActivity.IMEI_KEY, imei);
		intent.putExtra(BandDetailsActivity.DISPLAY_NAME_KEY, initialDisplayName);
		intent.putExtra(BandDetailsActivity.HARDWARE_LOCATION, hardwareLocation);
		intent.putExtra(BandDetailsActivity.IS_BASIC, isBasic);
		startActivityForResult(intent, 0xf001);
	}

	private void updateFragmentState(boolean loading) {
		final List<Map<String, Object>> menuContent = new ArrayList<>();
		Map<String, Object> menuEntry;

		menuEntry = new HashMap<>();
		menuEntry.put("type", "label");
		menuEntry.put("text", R.string.dashboard_fragment_drawer_menu);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 2);
		menuEntry.put("icon", R.drawable.ic_settings_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_settings);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 3);
		menuEntry.put("icon", R.drawable.ic_help_outline_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_helpdesk);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 4);
		menuEntry.put("icon", R.drawable.ic_power_settings_new_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_logout);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "label");
		menuEntry.put("text", R.string.dashboard_fragment_drawer_bands);
		menuContent.add(menuEntry);

		if (associatedBands.isEmpty()) {
			if (loading) {
				getActivity().getFragmentManager().beginTransaction().replace(R.id.dashboard_fragment_frame, new DashboardLoadingFragment())
					.commit();
			} else {
				getActivity().getFragmentManager().beginTransaction().replace(R.id.dashboard_fragment_frame, new DashboardNoBandsFragment())
					.commit();
			}
		} else {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
			final int displayBandAs = Integer.parseInt(preferences.getString(SettingsKeys.PREF_BAND_DISPLAY, "0"));

			DashboardBandsFragment dashboardBandsFragment = new DashboardBandsFragment();
			Bundle arguments = new Bundle();
			arguments.putSerializable(DashboardBandsFragment.BANDS_ARGUMENT_KEY, associatedBands);
			dashboardBandsFragment.setArguments(arguments);
			getActivity().getFragmentManager().beginTransaction().replace(R.id.dashboard_fragment_frame, dashboardBandsFragment).commit();

			for (ListBandsResponse.BandDetails band : associatedBands) {
				menuEntry = new HashMap<>();
				menuEntry.put("type", "band");
				menuEntry.put("icon", R.drawable.ic_person_outline_black_36dp);
				menuEntry.put("imei", band.getImei());
				menuEntry.put("hardwareLocation", band.getBandHardwareLocation());
				menuEntry.put("isBasic", band.isBasic());

				String displayName;
				switch (displayBandAs) {
					case 1:
						displayName = band.getLastName() + " " + band.getFirstName();
						break;
					case 2:
						displayName = band.getFirstName() + " " + band.getLastName();
						break;
					case 0:
					default:
						displayName = band.getDisplayName();
						break;
				}

				menuEntry.put("text", displayName);
				menuContent.add(menuEntry);
			}
		}

		/*menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 5);
		menuEntry.put("icon", R.drawable.ic_add_circle_outline_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_add_band);
		menuContent.add(menuEntry);*/

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 1);
		menuEntry.put("icon", R.drawable.ic_refresh_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_refresh);
		menuContent.add(menuEntry);

		drawerList.setAdapter(new ArrayAdapter<Map<String, Object>>(getActivity(), R.layout.list_entry_dashboard_drawer, R.id
			.list_entry_dashboard_drawer_label, menuContent) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				Map<String, Object> item = getItem(position);
				if ("label".equals(item.get("type"))) {
					View view = View.inflate(getActivity(), R.layout.list_entry_dashboard_drawer_label, null);
					((TextView)view.findViewById(R.id.list_entry_dashboard_drawer_label_label)).setText((Integer)item.get("text"));
					return view;
				} else if ("static".equals(item.get("type"))) {
					View view = View.inflate(getActivity(), R.layout.list_entry_dashboard_drawer, null);
					view.findViewById(R.id.list_entry_dashboard_drawer_icon).setBackgroundResource((Integer)item.get("icon"));
					((TextView)view.findViewById(R.id.list_entry_dashboard_drawer_label)).setText((Integer)item.get("text"));
					return view;
				} else if ("band".equals(item.get("type"))) {
					View view = View.inflate(getActivity(), R.layout.list_entry_dashboard_drawer, null);
					view.findViewById(R.id.list_entry_dashboard_drawer_icon).setBackgroundResource((Integer)item.get("icon"));
					((TextView)view.findViewById(R.id.list_entry_dashboard_drawer_label)).setText((String)item.get("text"));
					return view;
				}
				throw new RuntimeException("Type " + item.get("type") + " not supported");
			}

			@Override
			public boolean isEnabled(int position) {
				return !"label".equals(getItem(position).get("type"));
			}
		});
	}

	private void openDrawer() {
		drawerLayout.openDrawer(GravityCompat.START);
	}

	private void closeDrawer() {
		drawerLayout.closeDrawer(GravityCompat.START);
	}

	private class  ListBandsTask extends ApiResponseTask {
		@Override
		protected Void doInBackground(Void... params) {
			associatedBands.clear();
			if(getActivity() != null) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						updateFragmentState(true);
					}
				});
			}
			String json;
			App app = App.getInstance();
			final ListBandsResponse listBandsResponse;
			try {
				json = new HttpClient(ApiEndPoint.LIST_BANDS).doPost(new ListBandsRequest(app.getCurrentUser().getUserId(), app
					.getCurrentUser().getAuthenticationToken()));
				LOGGER.debug("JSON: {}", json.toString());
				listBandsResponse = ListBandsResponse.fromString(json, ListBandsResponse.class);
			} catch (IOException e) {

				LOGGER.error("Failed to get a list of bands, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							updateFragmentState(false);
							Toast.makeText(getActivity(), R.string.dashboard_fragment_refresh_failed, Toast.LENGTH_SHORT).show();
						}
					});
				}

				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							updateFragmentState(false);
							Toast.makeText(getActivity(), R.string.dashboard_fragment_refresh_failed, Toast.LENGTH_SHORT).show();
						}
					});
				}
				return null;
			}

			if (listBandsResponse.getAssociatedBands() == null) {
				// Invalid token
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), R.string.dashboard_fragment_not_authenticated, Toast.LENGTH_SHORT).show();
						}
					});
				}
				App.getInstance().setCurrentUser(null);
				getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id
						.main_activity_frame, new LoginFragment()).commit();
				return null;
			}

			if (isCancelled()) {
				return null;
			}

			LOGGER.info("Server returned bands: {}", listBandsResponse.getAssociatedBands().size());
			associatedBands.addAll(listBandsResponse.getAssociatedBands());
			App.getInstance().getAssociatedBandsCache().put(CACHE_KEY, associatedBands);
			if(getActivity() != null) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						updateFragmentState(false);
					}
				});
			}
			return null;
		}

	}

	public void refreshFragment(){
		new ListBandsTask().execute();
	}
}
