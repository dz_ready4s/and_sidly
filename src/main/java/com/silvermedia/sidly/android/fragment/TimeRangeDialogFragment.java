package com.silvermedia.sidly.android.fragment;

import android.annotation.SuppressLint;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RadioButton;

import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;

public class TimeRangeDialogFragment extends DialogFragment {
	private RadioButton timeRangeDialogLastDay;

	private RadioButton timeRangeDialogLastWeek;

	private RadioButton timeRangeDialogLastMonth;

	private RadioButton timeRangeDialogLastYear;

	private RadioButton timeRangeDialogAll;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		@SuppressLint("InflateParams") final View root = getActivity().getLayoutInflater().inflate(R.layout.time_range_dialog, null);

		timeRangeDialogLastDay = (RadioButton)root.findViewById(R.id.time_range_dialog_last_day);
		timeRangeDialogLastWeek = (RadioButton)root.findViewById(R.id.time_range_dialog_last_week);
		timeRangeDialogLastMonth = (RadioButton)root.findViewById(R.id.time_range_dialog_last_month);
		timeRangeDialogLastYear = (RadioButton)root.findViewById(R.id.time_range_dialog_last_year);
		timeRangeDialogAll = (RadioButton)root.findViewById(R.id.time_range_dialog_all);

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		final int originalTimeRange = preferences.getInt(SettingsKeys.PREF_BAND_DETAILS_TIME_RANGE, 0);
		switch (originalTimeRange) {
			case 0:
				timeRangeDialogLastDay.setChecked(true);
				break;
			case 1:
				timeRangeDialogLastWeek.setChecked(true);
				break;
			case 2:
				timeRangeDialogLastMonth.setChecked(true);
				break;
			case 3:
				timeRangeDialogLastYear.setChecked(true);
				break;
			case 4:
				timeRangeDialogAll.setChecked(true);
				break;
			default:
				timeRangeDialogLastDay.setChecked(true);
				break;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),  R.style.AlertDialogTheme);
		builder.setView(root);
		builder.setTitle(R.string.time_range_dialog_title);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
				SharedPreferences.Editor editor = preferences.edit();

				int selectedRange = 0;
				if (timeRangeDialogLastDay.isChecked()) {
					selectedRange = 0;
				}
				if (timeRangeDialogLastWeek.isChecked()) {
					selectedRange = 1;
				}
				if (timeRangeDialogLastMonth.isChecked()) {
					selectedRange = 2;
				}
				if (timeRangeDialogLastYear.isChecked()) {
					selectedRange = 3;
				}
				if (timeRangeDialogAll.isChecked()) {
					selectedRange = 4;
				}

				editor.putInt(SettingsKeys.PREF_BAND_DETAILS_TIME_RANGE, selectedRange);
				editor.apply();

				dialog.dismiss();

				if (selectedRange != originalTimeRange) {
					((BandDetailsActivity)getActivity()).onTimeRangeChanged();
				}
			}
		});
		return builder.create();
	}
}
