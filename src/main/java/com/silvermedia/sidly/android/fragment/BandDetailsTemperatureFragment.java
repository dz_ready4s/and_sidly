package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.ColorUtil;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.GetTemperatureListRequest;
import com.silvermedia.sidly.android.cdm.GetTemperatureListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lecho.lib.hellocharts.formatter.AxisValueFormatter;
import lecho.lib.hellocharts.formatter.ValueFormatterHelper;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class BandDetailsTemperatureFragment extends Fragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsTemperatureFragment.class);

	private View root;

	private int highlightPosition = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.band_details_temperature_fragment, container, false);

		root.findViewById(R.id.band_details_temperature_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		return root;
	}
/*
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (isVisibleToUser) {
			LineChartView lineChartView = (LineChartView)root.findViewById(R.id.band_details_temperature_fragment_chart);
			lineChartView.setCurrentViewportWithAnimation(lineChartView.getMaximumViewport());
		}
	}*/

	private void loadingFinished(GetTemperatureListResponse getTemperatureListResponse) {
		root.findViewById(R.id.band_details_temperature_fragment).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		final ListView listView = (ListView)root.findViewById(R.id.band_details_temperature_fragment_list);
        Collections.reverse(getTemperatureListResponse.getTemperatureList());
		listView.setAdapter(new ArrayAdapter<GetTemperatureListResponse.Temperature>(getActivity(), R.layout.list_entry_band_details_value, R.id
			.list_entry_band_details_value_date, getTemperatureListResponse.getTemperatureList()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				GetTemperatureListResponse.Temperature temperature = getItem(position);
				DateTime dateTime = new DateTime(temperature.getTimestampMillis());
				((TextView)view.findViewById(R.id.list_entry_band_details_value_date)).setText(ImperialUtil.getYearsDate(dateTime));
				((TextView)view.findViewById(R.id.list_entry_band_details_value_time)).setText(ImperialUtil.getHoursDate(dateTime));
				((TextView)view.findViewById(R.id.list_entry_band_details_value_value)).setText(ImperialUtil.getStringTemperature(temperature.getTemperature()));

				view.setBackgroundColor(ColorUtil.getColor(getActivity(), android.R.color.background_light));
				if (highlightPosition == position) {
					view.setBackgroundColor(ColorUtil.getColor(getActivity(), R.color.accent_faded));
				}

				return view;
			}
		});

		if (getTemperatureListResponse.getTemperatureList().isEmpty()) {
			root.findViewById(R.id.band_details_temperature_fragment_chart).setVisibility(View.GONE);
			root.findViewById(R.id.band_details_temperature_fragment_no_data).setVisibility(View.VISIBLE);
			return;
		}
		root.findViewById(R.id.band_details_temperature_fragment_chart).setVisibility(View.VISIBLE);
		root.findViewById(R.id.band_details_temperature_fragment_no_data).setVisibility(View.GONE);

		float minTime = Float.MAX_VALUE;
		float maxTime = Float.MIN_VALUE;
		float minValue = Float.MAX_VALUE;
		float maxValue = Float.MIN_VALUE;
		final List<PointValue> points = new ArrayList<>();
		for (GetTemperatureListResponse.Temperature temperature : getTemperatureListResponse.getTemperatureList()) {
//			float temp = ImperialUtil.changeToFahrenheita(temperature.getTemperature());
			minTime = Math.min(temperature.getTimestampMillis(), minTime);
			maxTime = Math.max(temperature.getTimestampMillis(), maxTime);

			minValue = Math.min(temperature.getTemperature(), minValue);
			maxValue = Math.max(temperature.getTemperature(), maxValue);

			points.add(new PointValue(temperature.getTimestampMillis(), temperature.getTemperature()));
		}

		if (maxTime - minTime < 1000) {
			maxTime += 1000;
		}
		if (maxValue - minValue < 5) {
			maxValue += 5;
		}

		List<AxisValue> axisValues = new ArrayList<>();
		if (getTemperatureListResponse.getTemperatureList().size() <= 4) {
			for (GetTemperatureListResponse.Temperature temperature : getTemperatureListResponse.getTemperatureList()) {
				axisValues.add(new AxisValue(temperature.getTimestampMillis()).setLabel(new DateTime(temperature.getTimestampMillis()).toString
					(getString(R.string.format_time))));
			}
		} else {
			float axisIncrements = (maxTime - minTime) / 3.0f;
			for (int i = 0; i < 4; i++) {
				float time = minTime + i * axisIncrements;
				AxisValue axisValue = new AxisValue(time);
				if (maxTime - minTime < 25 * 60 * 60 * 1000) {
					axisValue.setLabel(ImperialUtil.getHoursDate(new DateTime((long)time)));
				} else {
					axisValue.setLabel(ImperialUtil.getYearsDate(new DateTime((long)time)));
				}
				axisValues.add(axisValue);
			}
		}

		Line line = new Line(points).setColor(ColorUtil.getColor(getActivity(), R.color.accent)).setStrokeWidth(2).setHasLines(true).setHasPoints
			(true).setPointRadius(4);

		LineChartData lineChartData = new LineChartData(Lists.newArrayList(line));
		lineChartData.setAxisXBottom(new Axis(axisValues).setHasLines(true).setHasTiltedLabels(false));
		AxisValueFormatter axisValueFormatter = new AxisValueFormatter() {
			private final ValueFormatterHelper valueFormatterHelper = new ValueFormatterHelper();

			@Override
			public int formatValueForManualAxis(char[] formattedValue, AxisValue axisValue) {
				return 0;
			}

			@Override
			public int formatValueForAutoGeneratedAxis(char[] formattedValue, float value, int autoDecimalDigits) {
				return valueFormatterHelper.formatFloatValue(formattedValue, value, 1);
			}
		};
		lineChartData.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(true).setName(getString(R.string
			.band_details_temperature_fragment_label)).setFormatter(axisValueFormatter));

		LineChartView lineChartView = (LineChartView)root.findViewById(R.id.band_details_temperature_fragment_chart);
		lineChartView.setLineChartData(lineChartData);
		lineChartView.setZoomType(ZoomType.HORIZONTAL);
		lineChartView.setZoomEnabled(true);
		lineChartView.setOnValueTouchListener(new LineChartOnValueSelectListener() {
			@Override
			public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
				highlightPosition = points.indexOf(value);
				listView.invalidateViews();
				listView.smoothScrollToPosition(points.indexOf(value));
			}

			@Override
			public void onValueDeselected() { }
		});

		Viewport viewport = lineChartView.getMaximumViewport();
		viewport.left -= (maxTime - minTime) * 0.05;
		viewport.right += (maxTime - minTime) * 0.05;
		viewport.top += (maxValue - minValue) * 0.1;
		viewport.bottom -= (maxValue - minValue) * 0.1;
		lineChartView.setCurrentViewport(viewport);
		lineChartView.setMaximumViewport(viewport);
	}

	private void loadingFailed() {
		root.findViewById(R.id.band_details_temperature_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
	}

	private class DataSyncTask extends ApiResponseTask {
		private final long imei;

		public DataSyncTask(long imei) {
			super();

			this.imei = imei;
		}

		@Override
		protected Void doInBackground(Void... params) {
			BandDetailsActivity activity = (BandDetailsActivity)getActivity();
			CurrentUser user = App.getInstance().getCurrentUser();

			try {
				String json = new HttpClient(ApiEndPoint.GET_TEMPERATURE_LIST).doPost(new GetTemperatureListRequest(user.getUserId(), user
					.getAuthenticationToken(), imei, activity.getQueryStartTime(), activity.getQueryEndTime()));
                final GetTemperatureListResponse getTemperatureListResponse = GetTemperatureListResponse.fromString(json, GetTemperatureListResponse
					.class);
				if(isApiResponseCorrect(json)) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFinished(getTemperatureListResponse);
						}
					});
				}
			} catch (IOException e) {
				LOGGER.error("Failed to get data from server, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (NullPointerException e){
				LOGGER.error("Failed to update user credentials", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			}

			return null;
		}
	}
}
