package com.silvermedia.sidly.android.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.ColorUtil;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.cdm.GetBarometerListRequest;
import com.silvermedia.sidly.android.cdm.GetBarometerListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.ColumnChartView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BandDetailsBarometerFragment extends Fragment {

    private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsBarometerFragment.class);

    private View root;

    private int highlightPosition = -1;

    public BandDetailsBarometerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.band_details_barometer_fragment, container, false);

        root.findViewById(R.id.band_details_barometer_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        return root;
    }

/*    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            ColumnChartView lineChartView = (ColumnChartView)root.findViewById(R.id.band_details_barometer_fragment_chart);
            lineChartView.setCurrentViewportWithAnimation(lineChartView.getMaximumViewport());
        }
    }*/

    @SuppressWarnings("unchecked")
    private void loadingFinished(final GetBarometerListResponse getBarometerListResponse) {
        root.findViewById(R.id.band_details_barometer_fragment).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        final ListView listView = (ListView)root.findViewById(R.id.band_details_barometer_fragment_list);

        List<GetBarometerListResponse.Barometer> barometerList = new ArrayList<>(getBarometerListResponse.getBarometerList());
        Collections.reverse(barometerList);

        listView.setAdapter(new ArrayAdapter<GetBarometerListResponse.Barometer>(getActivity(), R.layout.list_entry_band_details_value, R.id
                .list_entry_band_details_value_date, barometerList) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                GetBarometerListResponse.Barometer barometer = getItem(position);
                DateTime dateTime = new DateTime(barometer.getTimestampMillis());
                ((TextView) view.findViewById(R.id.list_entry_band_details_value_date)).setText(ImperialUtil.getYearsDate(dateTime));
                ((TextView) view.findViewById(R.id.list_entry_band_details_value_value)).setText(String.valueOf(barometer.getExternalPressure()));

                view.setBackgroundColor(ColorUtil.getColor(getActivity(), android.R.color.background_light));
                if (highlightPosition == position) {
                    view.setBackgroundColor(ColorUtil.getColor(getActivity(), R.color.accent_faded));
                }

                return view;
            }
        });

        if (getBarometerListResponse.getBarometerList().isEmpty()) {
            root.findViewById(R.id.band_details_barometer_fragment_chart).setVisibility(View.GONE);
            root.findViewById(R.id.band_details_barometer_fragment_no_data).setVisibility(View.VISIBLE);
            return;
        }
        root.findViewById(R.id.band_details_barometer_fragment_chart).setVisibility(View.VISIBLE);
        root.findViewById(R.id.band_details_barometer_fragment_no_data).setVisibility(View.GONE);

        ColumnChartView chart;
        chart = (ColumnChartView)root.findViewById(R.id.band_details_barometer_fragment_chart);
        ColumnChartData data;

        float minTime = Float.MAX_VALUE;
        float maxTime = Float.MIN_VALUE;
        float minValue = Float.MAX_VALUE;
        float maxValue = Float.MIN_VALUE;
        final List<Column> columns = new ArrayList<>();
        for (GetBarometerListResponse.Barometer barometer : getBarometerListResponse.getBarometerList()) {
            List<SubcolumnValue> subcolumns = new ArrayList<>();
            minTime = Math.min(barometer.getTimestampMillis(), minTime);
            maxTime = Math.max(barometer.getTimestampMillis(), maxTime);

            minValue = Math.min(barometer.getExternalPressure(), minValue);
            maxValue = Math.max(barometer.getExternalPressure(), maxValue);

            subcolumns.add(new SubcolumnValue(barometer.getExternalPressure(), ColorUtil.getColor(getActivity(), R.color.accent)));

            Column column = new Column(subcolumns);
            column.setHasLabels(false);
            column.setHasLabelsOnlyForSelected(false);
            columns.add(column);
        }

        List<AxisValue> axisValues = new ArrayList<>();
        int size = 0;
        if (getBarometerListResponse.getBarometerList().size() <= 4) {
            for (GetBarometerListResponse.Barometer barometer : getBarometerListResponse.getBarometerList()) {
                axisValues.add(new AxisValue(size++).setLabel(ImperialUtil.getHoursDate(new DateTime(barometer.getTimestampMillis()))));
            }
        } else {
            for (int i = 0; i < columns.size(); i += columns.size() / 3.2) {
                AxisValue axisValue = new AxisValue(i);
                axisValue.setLabel(ImperialUtil.getYearsDate
                        (new DateTime(getBarometerListResponse.getBarometerList().get(i).getTimestampMillis())));
                axisValues.add(axisValue);
            }
        }


        data = new ColumnChartData(columns);

        data.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(false));

        Axis axisX = new Axis(axisValues);
        axisX.setHasLines(true).setHasTiltedLabels(false);
        data.setAxisXBottom(axisX);

        chart.setColumnChartData(data);
        chart.setZoomType(ZoomType.HORIZONTAL);
        chart.setZoomEnabled(true);
        chart.setScrollEnabled(true);
        chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {

            @Override
            public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                int count = getBarometerListResponse.getBarometerList().size() - 1;
                int current = count - columnIndex;
                highlightPosition = current;
                listView.invalidateViews();
                listView.smoothScrollToPosition(current);
            }

            @Override
            public void onValueDeselected() { }
        });

        Viewport viewport = chart.getMaximumViewport();
        viewport.top = 1050;
        viewport.bottom = 950;
        if(getBarometerListResponse.getBarometerList().size() == 1){
            viewport.left -= 4;
            viewport.right += 4;
        } else {
            viewport.left -= columns.size() * 0.05;
            viewport.right += columns.size() * 0.05;
        }
        chart.setCurrentViewport(viewport);
        chart.setMaximumViewport(viewport);

    }

    private void loadingFailed() {
        root.findViewById(R.id.band_details_barometer_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
    }


    private class DataSyncTask extends ApiResponseTask {
        private final long imei;

        public DataSyncTask(long imei) {
            super();

            this.imei = imei;
        }

        @Override
        protected Void doInBackground(Void... params) {
            BandDetailsActivity activity = (BandDetailsActivity)getActivity();
            CurrentUser user = App.getInstance().getCurrentUser();

            try {
                String json = new HttpClient(ApiEndPoint.GET_BAROMETER_LIST).doPost(new GetBarometerListRequest(user.getUserId(), user
                        .getAuthenticationToken(), imei, activity.getQueryStartTime(), activity.getQueryEndTime()));
                final GetBarometerListResponse getBarometerListResponse = GetBarometerListResponse.fromString(json, GetBarometerListResponse
                        .class);
                if(isApiResponseCorrect(json)) {
                    if(getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingFinished(getBarometerListResponse);
                            }
                        });
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Failed to get data from server, server issue", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            } catch (NullPointerException e){
                LOGGER.error("Failed to update user credentials", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            } catch (JsonException e) {
                LOGGER.error("Failed to deserialize JSON data", e);
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFailed();
                        }
                    });
                }
            }

            return null;
        }
    }

}
