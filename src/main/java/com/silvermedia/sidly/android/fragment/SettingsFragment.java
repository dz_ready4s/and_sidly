package com.silvermedia.sidly.android.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.silvermedia.sidly.android.BuildConfig;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;

public class SettingsFragment extends PreferenceFragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
}