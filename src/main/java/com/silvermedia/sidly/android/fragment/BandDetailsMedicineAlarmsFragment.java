package com.silvermedia.sidly.android.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.MainActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.GetMedicineAlarmListRequest;
import com.silvermedia.sidly.android.cdm.GetMedicineAlarmListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.SetMedicineAlarmListRequest;
import com.silvermedia.sidly.android.cdm.SetMedicineAlarmListResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Time;
import java.util.List;

public class BandDetailsMedicineAlarmsFragment extends Fragment implements NewMedicineAlarmDialogFragment.OnNewMedicineAlarmCreated {
	private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsMedicineAlarmsFragment.class);

	private View root;

	private List<GetMedicineAlarmListResponse.MedicineAlarm> medicineAlarms;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.band_details_medicine_alarms_fragment, container, false);

		root.findViewById(R.id.band_details_medicine_alarms_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		root.findViewById(R.id.band_details_medicine_alarms_fragment_add_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (medicineAlarms.size() >= 12) {
					Toast.makeText(getActivity(), R.string.band_details_medicine_alarms_fragment_add_full, Toast.LENGTH_SHORT).show();
					return;
				}

				NewMedicineAlarmDialogFragment newMedicineAlarmDialogFragment = new NewMedicineAlarmDialogFragment();
				newMedicineAlarmDialogFragment.setOnNewMedicineAlarmCreated(BandDetailsMedicineAlarmsFragment.this);
				newMedicineAlarmDialogFragment.show(getFragmentManager(), "TAG");
			}
		});

		return root;
	}

	@Override
	public void addNewMedicineAlarm(String newMedicineAlarmTime, String newMedicineAlarmDescription) {
		if (medicineAlarms.size() >= 12) {
			LOGGER.error("Refusing to add new medicine alarm, we already have 12!");
			return;
		}

		medicineAlarms.add(new GetMedicineAlarmListResponse.MedicineAlarm(newMedicineAlarmTime, newMedicineAlarmDescription));
		setupList();
		syncListToServer();
	}

	private void setupList() {
		((ListView)root.findViewById(R.id.band_details_medicine_alarms_fragment_list)).setAdapter(new ArrayAdapter<GetMedicineAlarmListResponse
				.MedicineAlarm>(getActivity(), R.layout.list_entry_band_details_medicine_alarm, R.id.list_entry_band_details_medicine_alarm_time,
				medicineAlarms) {
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);

				final GetMedicineAlarmListResponse.MedicineAlarm medicineAlarm = getItem(position);
				Time time = Time.valueOf(medicineAlarm.getTime());
				((TextView) view.findViewById(R.id.list_entry_band_details_medicine_alarm_time)).setText(new DateTime(time.getTime()).toString
						(getString(R.string.format_time)));
				((TextView) view.findViewById(R.id.list_entry_band_details_medicine_alarm_description)).setText(medicineAlarm.getDisplayName());

				Button deleteButton = (Button) view.findViewById(R.id.list_entry_band_details_medicine_alarm_delete_button);
				deleteButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle(R.string.band_details_medicine_alarms_fragment_delete_confirmation);
						builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								medicineAlarms.remove(position);
								setupList();
								syncListToServer();
							}
						});
						builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder.create().show();
					}
				});

				return view;
			}
		});
	}

	private void syncListToServer() {
		new ApiResponseTask() {
			@Override
			protected Void doInBackground(Void... params) {
				LOGGER.info("Synchronizing list of medicine alarms with server");
				CurrentUser currentUser = App.getInstance().getCurrentUser();
				String json;
				try {
					json = new HttpClient(ApiEndPoint.SET_MEDICINE_ALARM_LIST).doPost(new SetMedicineAlarmListRequest(currentUser.getUserId()
							, currentUser.getAuthenticationToken(), ((BandDetailsActivity) getActivity()).getImei(), medicineAlarms));
					final SetMedicineAlarmListResponse setMedicineAlarmListResponse = SetMedicineAlarmListResponse.fromString(json,
							SetMedicineAlarmListResponse.class);
					if (!isApiResponseCorrect(json)) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(getActivity(), R.string.login_fragment_multilogin, Toast.LENGTH_SHORT).show();
							}
						});
						loadingFinishedError();
						return null;
					}
					if (setMedicineAlarmListResponse.isListUpdated()) {
						LOGGER.info("Update OK");
						return null;
					}
				} catch (IOException e) {
					LOGGER.error("Failed to send data to server, server issue", e);
				} catch (JsonException e) {
					LOGGER.error("Failed to deserialize JSON data", e);
				}

				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getActivity(), R.string.band_details_medicine_alarms_fragment_update_failed, Toast.LENGTH_SHORT).show();
						}
					});
				}
				return null;
			}
		}.execute();
	}

	private void loadingFinishedError(){
		App.getInstance().setCurrentUser(null);
		Intent intent = new Intent(getActivity(), MainActivity.class);
		startActivity(intent);
	}

	private void loadingFinished(GetMedicineAlarmListResponse getMedicineAlarmListResponse) {
		root.findViewById(R.id.band_details_medicine_alarms_fragment).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		medicineAlarms = getMedicineAlarmListResponse.getMedicineAlarmList();

		setupList();
	}

	private void loadingFailed() {
		root.findViewById(R.id.band_details_medicine_alarms_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
	}

	private class DataSyncTask extends ApiResponseTask {
		private final long imei;

		public DataSyncTask(long imei) {
			super();

			this.imei = imei;
		}

		@Override
		protected Void doInBackground(Void... params) {
			CurrentUser user = App.getInstance().getCurrentUser();

			try {
				String json = new HttpClient(ApiEndPoint.GET_MEDICINE_ALARM_LIST).doPost(new GetMedicineAlarmListRequest(user.getUserId(), user
					.getAuthenticationToken(), imei));
				final GetMedicineAlarmListResponse getMedicineAlarmListResponse = GetMedicineAlarmListResponse.fromString(json,
					GetMedicineAlarmListResponse.class);
				if(isApiResponseCorrect(json)){
					getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						loadingFinished(getMedicineAlarmListResponse);
					}
				});
				}
			} catch (IOException e) {
				LOGGER.error("Failed to get data from server, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (NullPointerException e) {
				LOGGER.error("Failed to update user credentials", e);
				if(getActivity() != null) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						loadingFailed();
					}
				});
			}
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			}

			return null;
		}
	}
}
