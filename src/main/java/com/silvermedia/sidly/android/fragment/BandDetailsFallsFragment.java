package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.MapActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.GetFallListRequest;
import com.silvermedia.sidly.android.cdm.GetFallListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Locale;

public class BandDetailsFallsFragment extends Fragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsFallsFragment.class);

	private View root;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.band_details_falls_fragment, container, false);

		root.findViewById(R.id.band_details_falls_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		return root;
	}

	private void loadingFinished(GetFallListResponse getFallListResponse) {
		root.findViewById(R.id.band_details_falls_fragment).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		((TextView)root.findViewById(R.id.band_details_falls_fragment_count)).setText(String.format(Locale.getDefault(), getString(R.string
			.band_details_falls_fragment_total), getFallListResponse.getFallList().size()));

		ListView listView = (ListView)root.findViewById(R.id.band_details_falls_fragment_list);
        Collections.reverse(getFallListResponse.getFallList());
		listView.setAdapter(new ArrayAdapter<GetFallListResponse.Fall>(getActivity(), R.layout.list_entry_band_details_fall, R.id
			.list_entry_band_details_fall_date, getFallListResponse.getFallList()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);

				GetFallListResponse.Fall fall = getItem(position);
				DateTime fallDateTime = new DateTime(fall.getTimestampMillis());
				((TextView)view.findViewById(R.id.list_entry_band_details_fall_date)).setText(ImperialUtil.getYearsDate(fallDateTime));
				((TextView)view.findViewById(R.id.list_entry_band_details_fall_time)).setText(ImperialUtil.getHoursDate(fallDateTime));

				return view;
			}
		});
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				GetFallListResponse.Fall fall = (GetFallListResponse.Fall) parent.getItemAtPosition(position);
				Intent intent = new Intent(getActivity(), MapActivity.class);
					if (fall.getLatitude() == 0 && fall.getLongitude() == 0) {
						Toast.makeText(getActivity(), getResources().getString(R.string.map_activity_error), Toast.LENGTH_SHORT).show();
					} else {
						intent.putExtra("hardwareLocation", fall.getHardwareLocation());
						intent.putExtra("latitude", fall.getLatitude());
						intent.putExtra("longitude", fall.getLongitude());
						intent.putExtra("timestamp", fall.getTimestampMillis());
						startActivity(intent);
					}
				}
		});
	}

	private void loadingFailed() {
		root.findViewById(R.id.band_details_falls_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
	}

	private class DataSyncTask extends ApiResponseTask {
		private final long imei;

		public DataSyncTask(long imei) {
			super();

			this.imei = imei;
		}

		@Override
		protected Void doInBackground(Void... params) {
			BandDetailsActivity activity = (BandDetailsActivity)getActivity();
			CurrentUser user = App.getInstance().getCurrentUser();

			try {
				String json = new HttpClient(ApiEndPoint.GET_FALL_LIST).doPost(new GetFallListRequest(user.getUserId(), user.getAuthenticationToken
					(), imei, activity.getQueryStartTime(), activity.getQueryEndTime()));
				final GetFallListResponse getFallListResponse = GetFallListResponse.fromString(json, GetFallListResponse.class);
				if(isApiResponseCorrect(json)) {
					if(getActivity() != null) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								loadingFinished(getFallListResponse);
							}
						});
					}
				}
			} catch (IOException e) {
				LOGGER.error("Failed to get data from server, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (NullPointerException e){
				LOGGER.error("Failed to update user credentials", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			}

			return null;
		}
	}
}
