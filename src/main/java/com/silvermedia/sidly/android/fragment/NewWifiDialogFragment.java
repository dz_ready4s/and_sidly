package com.silvermedia.sidly.android.fragment;

import android.annotation.SuppressLint;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.silvermedia.sidly.android.R;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dabu on 16.02.16.
 */
public class NewWifiDialogFragment extends DialogFragment {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewMedicineAlarmDialogFragment.class);

    private OnNewWifiCreated onNewWifiCreated;

    private EditText newWifiDialogSsid;

    private EditText newWifiDialogPassword;

    private void positiveButtonClicked(DialogInterface dialog) {
        if (newWifiDialogPassword.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.new_wifi_dialog_password_required, Toast.LENGTH_SHORT).show();
            return;
        }
        if (newWifiDialogSsid.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.new_wifi_dialog_name_required, Toast.LENGTH_SHORT).show();
            return;
        }

        onNewWifiCreated.addNewWifi(newWifiDialogSsid.getText().toString(), newWifiDialogPassword.getText().toString());

        dialog.dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        @SuppressLint("InflateParams") final View root = getActivity().getLayoutInflater().inflate(R.layout.new_wifi_dialog, null);

        newWifiDialogSsid = (EditText)root.findViewById(R.id.new_wifi_dialog_ssid);
        newWifiDialogPassword = (EditText)root.findViewById(R.id.new_wifi_dialog_password);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(root);
        builder.setTitle(R.string.new_wifi_dialog_title);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positiveButtonClicked(dialog);
            }
        });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

        final AlertDialog dialog = (AlertDialog)getDialog();
        if (dialog == null) {
            LOGGER.error("Failed to override buttons for dialog!");
            return;
        }

        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positiveButtonClicked(dialog);
            }
        });
    }

    public void setOnNewWifiCreated(OnNewWifiCreated onNewWifiCreated) {
        this.onNewWifiCreated = onNewWifiCreated;
    }

    public interface OnNewWifiCreated {
        void addNewWifi(String newWifiSsid, String newWifiPassword);
    }
}
