package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.MainActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.AuthenticationRequest;
import com.silvermedia.sidly.android.cdm.AuthenticationResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.remote.requests.SetTimezoneTask;
import com.silvermedia.sidly.android.util.ViewUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class LoginFragment extends Fragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginFragment.class);

	private ProgressBar loginActivitySpinner;

	private LoginTask loginTask;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View root = inflater.inflate(R.layout.login_fragment, container, false);

		final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String tacUrl;// = preferences.getString(SettingsKeys.PREF_TAC_URL, null);
		tacUrl = getString(R.string.terms_url);
		if (tacUrl == null) {
			throw new RuntimeException("Failed to get TaC URL from preferences");
		}

		final CheckBox loginActivityAcceptTaC = (CheckBox)root.findViewById(R.id.login_fragment_accept_tac);
		loginActivityAcceptTaC.setText(Html.fromHtml(String.format(getString(R.string.login_fragment_accept_tac), tacUrl)));
		loginActivityAcceptTaC.setMovementMethod(LinkMovementMethod.getInstance());

		loginActivitySpinner = (ProgressBar)root.findViewById(R.id.login_fragment_spinner);

		root.findViewById(R.id.login_fragment_login).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (loginTask != null) {
					return;
				}

				ViewUtils.hideSoftKeyboard(getActivity());

				if (App.getInstance().getGcmInstanceId() == null || App.getInstance().getGcmInstanceId().isEmpty()) {
					Toast.makeText(getActivity(), R.string.login_fragment_gcm_required, Toast.LENGTH_SHORT).show();
					return;
				}

				EditText loginActivityEmail = (EditText)root.findViewById(R.id.login_fragment_email);
				EditText loginActivityPassword = (EditText)root.findViewById(R.id.login_fragment_password);

				if (loginActivityEmail.getText().toString().isEmpty()) {
					loginActivityEmail.requestFocus();
					Toast.makeText(getActivity(), R.string.login_fragment_email_required, Toast.LENGTH_SHORT).show();
					return;
				}

				if (loginActivityPassword.getText().toString().isEmpty()) {
					loginActivityPassword.requestFocus();
					Toast.makeText(getActivity(), R.string.login_fragment_password_required, Toast.LENGTH_SHORT).show();
					return;
				}

				if (!loginActivityAcceptTaC.isChecked()) {
					Toast.makeText(getActivity(), R.string.login_fragment_tac_required, Toast.LENGTH_SHORT).show();
					return;
				}

				loginActivitySpinner.setVisibility(View.VISIBLE);
				loginTask = new LoginTask();
				loginTask.execute(loginActivityEmail.getText().toString(), loginActivityPassword.getText().toString());
			}
		});
		root.findViewById(R.id.login_fragment_password_change).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW,
						Uri.parse(getString(R.string.forgot_pass_url))));
			}
		});

		root.findViewById(R.id.login_fragment_register).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW,
						Uri.parse(getString(R.string.register_url))));
			}
		});

		root.findViewById(R.id.login_fragment_privacy_policy).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW,
						Uri.parse(getString(R.string.privacy_policy_url))));
			}
		});

		return root;
	}

	private void onLoginFailed(int errorCode) {
		loginTask = null;
		loginActivitySpinner.setVisibility(View.GONE);
		if(errorCode == 1)
			Toast.makeText(getActivity(), R.string.login_fragment_login_failed, Toast.LENGTH_SHORT).show();
		else if(errorCode == 2)
			Toast.makeText(getActivity(), R.string.login_fragment_login_access_denied, Toast.LENGTH_SHORT).show();

		View root = getView();
		if (root != null) {
			((EditText)root.findViewById(R.id.login_fragment_password)).setText("");
		}
	}

	private void onLoginError() {
		loginTask = null;
		loginActivitySpinner.setVisibility(View.GONE);
		Toast.makeText(getActivity(), R.string.login_fragment_login_error, Toast.LENGTH_SHORT).show();
	}

	private void onLoginSucceeded(AuthenticationResponse authenticationResponse) {
		loginTask = null;
		loginActivitySpinner.setVisibility(View.GONE);
		//TODO change last argument from CurrentUser constructor
		CurrentUser currentUser = new CurrentUser(authenticationResponse.getUserId(), authenticationResponse.getAuthenticationToken(), authenticationResponse.isPremium(), authenticationResponse.isUser());
		App.getInstance().setCurrentUser(currentUser);
		((MainActivity)getActivity()).showMainView();
		SetTimezoneTask setTimezoneTask = new SetTimezoneTask();
		setTimezoneTask.execute();
	}

	private class LoginTask extends AsyncTask<String, Void, Void> {
		@Override
		protected Void doInBackground(String... params) {
			String email = params[0];
			String password = params[1];
			final AuthenticationResponse authenticationResponse;
			try {
				String json = new HttpClient(ApiEndPoint.AUTHENTICATION).doPost(new AuthenticationRequest(email, password, App.getInstance()
					.getDeviceInformation(), App.getInstance().getGcmInstanceId()));
				authenticationResponse = AuthenticationResponse.fromString(json, AuthenticationResponse.class);
			} catch (IOException e) {
				LOGGER.error("Failed to authenticate user, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							LoginFragment.this.onLoginError();
						}
					});
				}
				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							LoginFragment.this.onLoginError();
						}
					});
				}
				return null;
			}

			if (authenticationResponse.getErrorCode() != 0) {
				LOGGER.warn("Authentication failed with code {}", authenticationResponse.getErrorCode());
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							LoginFragment.this.onLoginFailed(authenticationResponse.getErrorCode());
						}
					});
				}
				return null;
			}

			LOGGER.info("Authentication OK; id: {}, token: {}", authenticationResponse.getUserId(), authenticationResponse.getAuthenticationToken());
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					LoginFragment.this.onLoginSucceeded(authenticationResponse);
				}
			});
			return null;
		}
	}
}
