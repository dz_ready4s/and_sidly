package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.silvermedia.sidly.android.ColorUtil;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.cdm.ListBandsResponse;
import com.silvermedia.sidly.android.util.BandFirmwareUpgradeUtil;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Locale;

public class DashboardBandsFragment extends Fragment {
	public static final String BANDS_ARGUMENT_KEY = "__bands_key";
	private View root;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle arguments = getArguments();
		if (!arguments.containsKey(BANDS_ARGUMENT_KEY)) {
			throw new RuntimeException("Missing bands list");
		}
		@SuppressWarnings("unchecked") ArrayList<ListBandsResponse.BandDetails> bands = (ArrayList<ListBandsResponse.BandDetails>)arguments
			.getSerializable(BANDS_ARGUMENT_KEY);
		if (bands == null) {
			throw new RuntimeException("Missing bands list");
		}

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		final int displayBandAs = Integer.parseInt(preferences.getString(SettingsKeys.PREF_BAND_DISPLAY, "0"));

		ListView listView = (ListView)inflater.inflate(R.layout.dashboard_bands_fragment, container, false);
		listView.setAdapter(new ArrayAdapter<ListBandsResponse.BandDetails>(getActivity(), R.layout.list_entry_dashboard_band, R.id
				.list_entry_dashboard_band_display_name, bands) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ListBandsResponse.BandDetails bandDetails = getItem(position);

				root = super.getView(position, convertView, parent);

				TextView listEntryDashboardBandDisplayName = (TextView) root.findViewById(R.id.list_entry_dashboard_band_display_name);
				TextView listEntryDashboardBandLastMeasurement = (TextView) root.findViewById(R.id.list_entry_dashboard_band_last_measurement);
				TextView listEntryDashboardBandTemperature = (TextView) root.findViewById(R.id.list_entry_dashboard_band_temperature);
				TextView listEntryDashboardBandHeartRate = (TextView) root.findViewById(R.id.list_entry_dashboard_band_heart_rate);
				TextView listEntryDashboardBandHardwareLocation = (TextView) root.findViewById(R.id.list_entry_dashboard_band_gps_text);
				TextView listEntryDashboardBandLastFall = (TextView) root.findViewById(R.id.list_entry_dashboard_band_last_fall);
				TextView listEntryDashboardBandLastSos = (TextView) root.findViewById(R.id.list_entry_dashboard_band_last_sos);
				TextView listEntryDashboardBandBateryStatus = (TextView) root.findViewById(R.id.list_entry_dashboard_battery_status);
				ImageView listEntryDashboardSignalIcon = (ImageView) root.findViewById(R.id.list_entry_dashboard_band_signal_icon);
				ImageView listEntryDashboardWearIcon = (ImageView) root.findViewById(R.id.list_entry_dashboard_band_iswear_icon);

				switch (displayBandAs) {
					case 1:
						listEntryDashboardBandDisplayName.setText(bandDetails.getLastName() + " " + bandDetails.getFirstName());
						break;
					case 2:
						listEntryDashboardBandDisplayName.setText(bandDetails.getFirstName() + " " + bandDetails.getLastName());
						break;
					case 0:
					default:
						listEntryDashboardBandDisplayName.setText(bandDetails.getDisplayName());
						break;
				}

				switch(getSignalLevel(bandDetails.getSignal())){
					case 0:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_0);
						break;
					case 1:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_1);
						break;
					case 2:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_2);
						break;
					case 3:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_3);
						break;
					case 4:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_4);
						break;
					case 5:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_5);
						break;
					default:
						listEntryDashboardSignalIcon.setImageResource(R.drawable.gsm_0);
						break;
				}

				if (bandDetails.getMeasurementHardwareLocation() != 0) {
					listEntryDashboardBandHardwareLocation.setText(getString(R.string.hardware_location_gps));
				} else {
					listEntryDashboardBandHardwareLocation.setText(getString(R.string.hardware_location_agps));
				}
				if(bandDetails.isBasic()) {
					root.findViewById(R.id.list_entry_dashboard_band_list_temperature_section).setVisibility(View.GONE);
					root.findViewById(R.id.list_entry_dashboard_band_last_pulse_section).setVisibility(View.GONE);
					root.findViewById(R.id.list_entry_dashboard_band_list_last_measurement_section).setVisibility(View.GONE);
					listEntryDashboardBandHardwareLocation.setVisibility(View.GONE);
				}

				if(bandDetails.getBandHardwareLocation() == 0){
						root.findViewById(R.id.list_entry_dashboard_band_signal_icon).setVisibility(View.GONE);
						root.findViewById(R.id.list_entry_dashboard_band_gps_text).setVisibility(View.GONE);
						root.findViewById(R.id.list_entry_dashboard_band_iswear_icon).setVisibility(View.GONE);

				}else{
					root.findViewById(R.id.list_entry_dashboard_band_signal_icon).setVisibility(View.VISIBLE);
					root.findViewById(R.id.list_entry_dashboard_band_gps_text).setVisibility(View.VISIBLE);
					root.findViewById(R.id.list_entry_dashboard_band_iswear_icon).setVisibility(View.VISIBLE);
				}

				if(bandDetails.isBasic()){
					listEntryDashboardWearIcon.setVisibility(View.GONE);
				}else{
					listEntryDashboardWearIcon.setVisibility(View.VISIBLE);
					if(bandDetails.isIsWear()){
						listEntryDashboardWearIcon.setImageResource(R.drawable.wear_1);
					}else{
						listEntryDashboardWearIcon.setImageResource(R.drawable.wear_0);
					}
				}

				if (bandDetails.getLastMeasurementMillis() > 0) {
					DateTime dateTime = new DateTime(bandDetails.getLastMeasurementMillis());
					listEntryDashboardBandLastMeasurement.setText(ImperialUtil.getFullDate(dateTime));
				}
				if (bandDetails.getLastTemperature() > 0.0) {
					listEntryDashboardBandTemperature.setText(ImperialUtil.getStringTemperature(bandDetails.getLastTemperature()));
				}
				if (bandDetails.getLastHeartRate() > 0) {
					listEntryDashboardBandHeartRate.setText(String.format(Locale.getDefault(), getString(R.string
							.list_entry_dashboard_band_heart_rate_value), bandDetails.getLastHeartRate()));
				}

				listEntryDashboardBandLastFall.setTextColor(ColorUtil.getColor(getActivity(), R.color.foreground));
				if (bandDetails.getLastFallMillis() > 0) {
					DateTime dateTime = new DateTime(bandDetails.getLastFallMillis());
					listEntryDashboardBandLastFall.setText(ImperialUtil.getFullDate(dateTime));
					if (dateTime.isAfter(DateTime.now().minusMinutes(30))) {
						listEntryDashboardBandLastFall.setTextColor(ColorUtil.getColor(getActivity(), R.color.alarm));
					}
				}
				listEntryDashboardBandLastSos.setTextColor(ColorUtil.getColor(getActivity(), R.color.foreground));
				if (bandDetails.getLastSosMillis() > 0) {
					DateTime dateTime = new DateTime(bandDetails.getLastSosMillis());
					listEntryDashboardBandLastSos.setText(ImperialUtil.getFullDate(dateTime));
					if (dateTime.isAfter(DateTime.now().minusMinutes(30))) {
						listEntryDashboardBandLastSos.setTextColor(ColorUtil.getColor(getActivity(), R.color.alarm));
					}
				}

				listEntryDashboardBandBateryStatus.setText(bandDetails.getBattery()+"%");
				return root;
			}
		});
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ListBandsResponse.BandDetails bandDetails = (ListBandsResponse.BandDetails) parent.getItemAtPosition(position);

				String displayName;
				switch (displayBandAs) {
					case 1:
						displayName = bandDetails.getLastName() + " " + bandDetails.getFirstName();
						break;
					case 2:
						displayName = bandDetails.getFirstName() + " " + bandDetails.getLastName();
						break;
					case 0:
					default:
						displayName = bandDetails.getDisplayName();
						break;
				}

				((DashboardFragment) getFragmentManager().findFragmentById(R.id.main_activity_frame)).showDetailsForBand(bandDetails.getImei(),
						displayName, bandDetails.getBandHardwareLocation(), bandDetails.isBasic());
			}
		});
		return listView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		BandFirmwareUpgradeUtil.getInstance().checkFirmwareUpgrade();
	}

	public int getSignalLevel(int signal) {
		signal = Math.abs(signal);
		if (signal < 119 && signal >= 105) {
			return 1;
		} else if (signal < 105 && signal >= 92) {
			return 2;
		} else if (signal < 92 && signal >= 78) {
			return 3;
		} else if (signal < 78 && signal >= 64) {
			return 4;
		} else if (signal <= 50) {
			return 5;
		}else{
			return 0;
		}
	}
}
