package com.silvermedia.sidly.android.fragment;

import android.annotation.SuppressLint;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.remote.requests.SetBandFirmwareUpgradeTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-12.
 */
public class BandUpgradeDialog extends DialogFragment {

    private static final Logger LOGGER = LoggerFactory.getLogger(BandUpgradeDialog.class);

    private TextView mTitleLine;
    private TextView mDescLine1;

    private TextView mDescLine2;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        @SuppressLint("InflateParams") final View root =
                getActivity().getLayoutInflater().inflate(R.layout.new_upgrade_dialog, null);
        mTitleLine = (TextView) root.findViewById(R.id.new_upgrade_title);
        mTitleLine.setText(R.string.firmware_upgrade);
        mDescLine1 = (TextView) root.findViewById(R.id.new_upgrade_desc1);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.getString(BaseActivity.UPGRADE_MESSAGE) != null) {
            mDescLine2 = (TextView) root.findViewById(R.id.new_upgrade_desc2);
            mDescLine2.setText(Html.fromHtml(bundle.getString(BaseActivity.UPGRADE_MESSAGE)));
            mDescLine2.setVisibility(View.VISIBLE);
        }



        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(getString(R.string.firmware_upgrade));
        builder.setView(root);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SetBandFirmwareUpgradeTask upgradeTask = new SetBandFirmwareUpgradeTask();
                upgradeTask.execute(SetBandFirmwareUpgradeTask.STATE_CANCEL);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.upgrade, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SetBandFirmwareUpgradeTask upgradeTask = new SetBandFirmwareUpgradeTask();
                upgradeTask.execute(SetBandFirmwareUpgradeTask.STATE_UPGRADE);
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
