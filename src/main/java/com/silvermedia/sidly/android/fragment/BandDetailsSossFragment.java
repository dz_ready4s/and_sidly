package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.MapActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.GetSosListRequest;
import com.silvermedia.sidly.android.cdm.GetSosListResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.util.ImperialUtil;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Locale;

public class BandDetailsSossFragment extends Fragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsSossFragment.class);

	private View root;
	private boolean isBasic;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		isBasic = args.getBoolean("isBasic", true);
		new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.band_details_soss_fragment, container, false);

		root.findViewById(R.id.band_details_soss_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		return root;
	}

	private void loadingFinished(GetSosListResponse getSosListResponse) {
		root.findViewById(R.id.band_details_soss_fragment).setVisibility(View.VISIBLE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

		((TextView)root.findViewById(R.id.band_details_soss_fragment_count)).setText(String.format(Locale.getDefault(), getString(R.string
				.band_details_soss_fragment_total), getSosListResponse.getSosList().size()));

		ListView listView = (ListView)root.findViewById(R.id.band_details_soss_fragment_list);
        Collections.reverse(getSosListResponse.getSosList());
		listView.setAdapter(new ArrayAdapter<GetSosListResponse.SOS>(getActivity(), R.layout.list_entry_band_details_sos, R.id
			.list_entry_band_details_sos_date, getSosListResponse.getSosList()) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				GetSosListResponse.SOS sos = getItem(position);
				DateTime sosDateTime = new DateTime(sos.getTimestampMillis());
				((TextView)view.findViewById(R.id.list_entry_band_details_sos_date)).setText(ImperialUtil.getYearsDate(sosDateTime));
				((TextView)view.findViewById(R.id.list_entry_band_details_sos_time)).setText(ImperialUtil.getHoursDate(sosDateTime));
				return view;
			}
		});

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				GetSosListResponse.SOS sos = (GetSosListResponse.SOS) parent.getItemAtPosition(position);
					if (sos.getLatitude() == 0 && sos.getLongitude() == 0) {
						Toast.makeText(getActivity(), getResources().getString(R.string.map_activity_error), Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(getActivity(), MapActivity.class);
						intent.putExtra("hardwareLocation", sos.getHardwareLocation());
						intent.putExtra("latitude", sos.getLatitude());
						intent.putExtra("longitude", sos.getLongitude());
						intent.putExtra("timestamp", sos.getTimestampMillis());
						startActivity(intent);
					}
			}
			});

	}

	private void loadingFailed() {
		root.findViewById(R.id.band_details_soss_fragment).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
		root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
	}

	private class DataSyncTask extends ApiResponseTask {
		private final long imei;

		public DataSyncTask(long imei) {
			super();

			this.imei = imei;
		}

		@Override
		protected Void doInBackground(Void... params) {
			BandDetailsActivity activity = (BandDetailsActivity)getActivity();
			CurrentUser user = App.getInstance().getCurrentUser();
			String json = "";
			try {
				json = new HttpClient(ApiEndPoint.GET_SOS_LIST).doPost(new GetSosListRequest(user.getUserId(), user.getAuthenticationToken
					(), imei, activity.getQueryStartTime(), activity.getQueryEndTime()));
				final GetSosListResponse getSosListResponse = GetSosListResponse.fromString(json, GetSosListResponse.class);
				if(isApiResponseCorrect(json)){
					if(getActivity() != null) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								loadingFinished(getSosListResponse);
							}
						});
					}
				}
			} catch (IOException e) {
				LOGGER.error("Failed to get data from server, server issue", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			} catch (NullPointerException e){
				LOGGER.error("Failed to update user credentials", e);
				if(getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
				}
			}

			return null;
		}
	}
}
