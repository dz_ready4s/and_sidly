package com.silvermedia.sidly.android.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.silvermedia.sidly.android.R;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

public class NewMedicineAlarmDialogFragment extends DialogFragment {
	private static final Logger LOGGER = LoggerFactory.getLogger(NewMedicineAlarmDialogFragment.class);

	private OnNewMedicineAlarmCreated onNewMedicineAlarmCreated;

	private TimePicker newMedicineAlarmDialogTime;

	private EditText newMedicineAlarmDialogDescription;

	private void positiveButtonClicked(DialogInterface dialog) {
		if (newMedicineAlarmDialogDescription.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), R.string.new_medicine_alarm_dialog_description_required, Toast.LENGTH_SHORT).show();
			return;
		}
		if (onNewMedicineAlarmCreated != null) {
			if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
				//noinspection deprecation
				onNewMedicineAlarmCreated.addNewMedicineAlarm(String.format(Locale.getDefault(), "%02d:%02d:00", newMedicineAlarmDialogTime
					.getCurrentHour(), newMedicineAlarmDialogTime.getCurrentMinute()), newMedicineAlarmDialogDescription.getText().toString());
			} else {
				onNewMedicineAlarmCreated.addNewMedicineAlarm(String.format(Locale.getDefault(), "%02d:%02d:00", newMedicineAlarmDialogTime
					.getHour(), newMedicineAlarmDialogTime.getMinute()), newMedicineAlarmDialogDescription.getText().toString());
			}
		}

		dialog.dismiss();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		@SuppressLint("InflateParams") final View root = getActivity().getLayoutInflater().inflate(R.layout.new_medicine_alarm_dialog, null);

		newMedicineAlarmDialogTime = (TimePicker)root.findViewById(R.id.new_medicine_alarm_dialog_time);
		newMedicineAlarmDialogDescription = (EditText)root.findViewById(R.id.new_medicine_alarm_dialog_description);

		DateTime now = DateTime.now();
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
			//noinspection deprecation
			newMedicineAlarmDialogTime.setCurrentHour(now.getHourOfDay());
			//noinspection deprecation
			newMedicineAlarmDialogTime.setCurrentMinute(now.getMinuteOfHour());
		} else {
			newMedicineAlarmDialogTime.setHour(now.getHourOfDay());
			newMedicineAlarmDialogTime.setMinute(now.getMinuteOfHour());
		}
		newMedicineAlarmDialogTime.setIs24HourView(true);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(root);
		builder.setTitle(R.string.new_medicine_alarm_dialog_title);
		builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				positiveButtonClicked(dialog);
			}
		});
		return builder.create();
	}

	@Override
	public void onStart() {
		super.onStart();

		final AlertDialog dialog = (AlertDialog)getDialog();
		if (dialog == null) {
			LOGGER.error("Failed to override buttons for dialog!");
			return;
		}

		dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				positiveButtonClicked(dialog);
			}
		});
	}

	public void setOnNewMedicineAlarmCreated(OnNewMedicineAlarmCreated onNewMedicineAlarmCreated) {
		this.onNewMedicineAlarmCreated = onNewMedicineAlarmCreated;
	}

	public interface OnNewMedicineAlarmCreated {
		void addNewMedicineAlarm(String newMedicineAlarmTime, String newMedicineAlarmDescription);
	}
}
