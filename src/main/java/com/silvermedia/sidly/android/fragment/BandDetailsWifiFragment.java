package com.silvermedia.sidly.android.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;
import com.silvermedia.sidly.android.activity.MainActivity;
import com.silvermedia.sidly.android.cdm.GetWifiListRequest;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.GetWifiListResponse;
import com.silvermedia.sidly.android.cdm.SetWifiListRequest;
import com.silvermedia.sidly.android.cdm.SetWifiListResponse;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by dabu on 16.02.16.
 */
public class BandDetailsWifiFragment extends Fragment implements NewWifiDialogFragment.OnNewWifiCreated {

    private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsWifiFragment.class);

    private View root;

    private List<GetWifiListResponse.WifiObject> wifiList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new DataSyncTask(((BandDetailsActivity)getActivity()).getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.band_details_wifi_fragment, container, false);

        root.findViewById(R.id.band_details_wifi_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        root.findViewById(R.id.band_details_wifi_fragment_add_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wifiList.size() >= 3) {
                    Toast.makeText(getActivity(), R.string.band_details_wifi_fragment_add_full, Toast.LENGTH_SHORT).show();
                    return;
                }

                NewWifiDialogFragment newWifiDialogFragment = new NewWifiDialogFragment();
                newWifiDialogFragment.setOnNewWifiCreated(BandDetailsWifiFragment.this);
                newWifiDialogFragment.show(getFragmentManager(), "TAG");
            }
        });

        return root;
    }

    @Override
    public void addNewWifi(String newWifiSsid, String newWifiPassword) {
        if (wifiList.size() >= 3) {
            LOGGER.error("Refusing to add new network, we already have 3!");
            return;
        }

        wifiList.add(new GetWifiListResponse.WifiObject(newWifiSsid, newWifiPassword));
        setupList();
        syncListToServer();
    }

    private void setupList() {
        ((ListView)root.findViewById(R.id.band_details_wifi_fragment_list)).setAdapter(new ArrayAdapter<GetWifiListResponse
                .WifiObject>(getActivity(), R.layout.list_entry_band_details_wifi, R.id.list_entry_band_details_wifi_ssid,
                wifiList) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                final GetWifiListResponse.WifiObject wifiObject = getItem(position);
                ((TextView) view.findViewById(R.id.list_entry_band_details_wifi_ssid)).setText(wifiObject.getName());
                ((TextView) view.findViewById(R.id.list_entry_band_details_wifi_password)).setText(wifiObject.getPass());

                Button deleteButton = (Button) view.findViewById(R.id.list_entry_band_details_wifi_delete_button);
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.band_details_wifi_fragment_delete_confirmation);
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                wifiList.remove(position);
                                setupList();
                                syncListToServer();
                            }
                        });
                        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.create().show();
                    }
                });

                return view;
            }
        });
    }

    private void syncListToServer() {
        new ApiResponseTask() {
            @Override
            protected Void doInBackground(Void... params) {
                LOGGER.info("Synchronizing list of wifi networks with server");
                CurrentUser currentUser = App.getInstance().getCurrentUser();
                String json;
                try {
                    json = new HttpClient(ApiEndPoint.SET_WIFI_LIST).doPost(new SetWifiListRequest(currentUser.getUserId()
                            , currentUser.getAuthenticationToken(), ((BandDetailsActivity)getActivity()).getImei(), wifiList));
                    final SetWifiListResponse setMedicineAlarmListResponse = SetWifiListResponse.fromString(json,
                            SetWifiListResponse.class);
                    if(!isApiResponseCorrect(json)){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), R.string.login_fragment_multilogin, Toast.LENGTH_SHORT).show();
                            }
                        });
                        loadingFinishedError();
                        return null;
                    }
                    if (setMedicineAlarmListResponse.isListUpdated()) {
                        LOGGER.info("Update OK");
                        return null;
                    }
                } catch (IOException e) {
                    LOGGER.error("Failed to send data to server, server issue", e);
                } catch (JsonException e) {
                    LOGGER.error("Failed to deserialize JSON data", e);
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), R.string.band_details_wifi_fragment_update_failed, Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }.execute();
    }

    private void loadingFinishedError(){
        App.getInstance().setCurrentUser(null);
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
    }

    private void loadingFinished(GetWifiListResponse getWifiResponse) {
        root.findViewById(R.id.band_details_wifi_fragment).setVisibility(View.VISIBLE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.GONE);

        wifiList = getWifiResponse.getBandWifiList();

        setupList();
    }

    private void loadingFailed() {
        root.findViewById(R.id.band_details_wifi_fragment).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_spinner).setVisibility(View.GONE);
        root.findViewById(R.id.fragment_error).setVisibility(View.VISIBLE);
    }


    private class DataSyncTask extends ApiResponseTask {
        private final long imei;

        public DataSyncTask(long imei) {
            super();

            this.imei = imei;
        }

        @Override
        protected Void doInBackground(Void... params) {
            CurrentUser user = App.getInstance().getCurrentUser();

           try {
                String json = new HttpClient(ApiEndPoint.GET_WIFI_LIST).doPost(new GetWifiListRequest(user.getUserId(), user
                        .getAuthenticationToken(), imei));
                final GetWifiListResponse getWifiListResponse = GetWifiListResponse.fromString(json,
                        GetWifiListResponse.class);
                if(isApiResponseCorrect(json)){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingFinished(getWifiListResponse);
                        }
                    });
                }
            } catch (IOException e) {
                LOGGER.error("Failed to get data from server, server issue", e);
               if(getActivity() != null) {
                   getActivity().runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           loadingFailed();
                       }
                   });
               }
            } catch (NullPointerException e){
                LOGGER.error("Failed to update user credentials", e);
               if(getActivity() != null) {
                   getActivity().runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           loadingFailed();
                       }
                   });
               }
            } catch (JsonException e) {
                LOGGER.error("Failed to deserialize JSON data", e);
               if(getActivity() != null) {
                   getActivity().runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           loadingFailed();
                       }
                   });
               }
            }

            return null;
        }
    }

}
