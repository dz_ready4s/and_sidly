package com.silvermedia.sidly.android.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.BandDetailsActivity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BandDetailsFragment extends Fragment implements BandDetailsActivity.Callback {

    private ViewPager viewPager;
    private int selectedTab = 0;

    private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsFragment.class);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.band_details_fragment, container, false);
        viewPager = (ViewPager)root.findViewById(R.id.band_details_fragment_pager);
        viewPager.setOffscreenPageLimit(Math.min(Math.max(App.getInstance().getNumberOfCores() * 2, 2), 7));
        LOGGER.info("Off screen page limit: {}", viewPager.getOffscreenPageLimit());
        onTimeRangeChanged();
        ((BandDetailsActivity)getActivity()).setCallback(this);
        return root;
    }

    @Override
    public void onTimeRangeAction() {
        setCurrentPageViewItem();
        new TimeRangeDialogFragment().show(getFragmentManager(), "TAG");
    }

    @Override
    public void setCurrentPageViewItem(){
        selectedTab = viewPager.getCurrentItem();
    }

	/*@SuppressWarnings("unused")
	public void onSettingsAction(MenuItem item) {
		Intent intent = new Intent(this, BandSettingsActivity.class);
		intent.putExtra(IMEI_KEY, imei);
		intent.putExtra(DISPLAY_NAME_KEY, displayName);
		startActivityForResult(intent, 0xf002);
	}*/

	@Override
    public void onTimeRangeChanged() {
        LOGGER.info("Recreating view pager adapter and auto-switching to tab {}", selectedTab);
        viewPager.setAdapter(new ViewPagerAdapter(getFragmentManager()));
        viewPager.getAdapter().notifyDataSetChanged();
        viewPager.setCurrentItem(selectedTab);
    }

    @Override
    public void switchActiveTab(int tabNumber) {
        viewPager.setCurrentItem(tabNumber);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> fragmentArrayList = new ArrayList<>();
        private List<String> mTitlePageList = new ArrayList<>();
        private boolean isUserPremium;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            isUserPremium = App.getInstance().getCurrentUser().isPremium();
            initFragmentsList();
            initTitleList();
        }

        @Override
        public Fragment getItem(int position) {
            LOGGER.info("Creating band details tab {} -> {}", position, getPageTitle(position));
            return fragmentArrayList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentArrayList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitlePageList.get(position);
        }

        private void initTitleList(){
            if(((BandDetailsActivity)getActivity()).isBasic) {
                mTitlePageList.add(getString(R.string.band_details_tab_0_summary));
                mTitlePageList.add(getString(R.string.band_details_tab_4_sos));
                mTitlePageList.add(getString(R.string.band_details_tab_3_falls));
                mTitlePageList.add(getString(R.string.band_details_tab_9_alarmzone));
                /*mTitlePageList.add(getString(R.string.band_details_tab_1_pulse));
                mTitlePageList.add(getString(R.string.band_details_tab_8_pedometer));
                mTitlePageList.add(getString(R.string.band_details_tab_7_barometer));
                mTitlePageList.add(getString(R.string.band_details_tab_6_wifi));*/
            }else{
                mTitlePageList.add(getString(R.string.band_details_tab_0_summary));
                mTitlePageList.add(getString(R.string.band_details_tab_1_pulse));
                mTitlePageList.add(getString(R.string.band_details_tab_2_temperature));
                mTitlePageList.add(getString(R.string.band_details_tab_8_pedometer));
                mTitlePageList.add(getString(R.string.band_details_tab_7_barometer));
                mTitlePageList.add(getString(R.string.band_details_tab_3_falls));
                mTitlePageList.add(getString(R.string.band_details_tab_4_sos));
                //if (isUserPremium) {
                    //if (((BandDetailsActivity)getActivity()).hardwareLocation == 1)
                        mTitlePageList.add(getString(R.string.band_details_tab_9_alarmzone));
                //}
                //mTitlePageList.add(getString(R.string.band_details_tab_5_medicine));
                //mTitlePageList.add(getString(R.string.band_details_tab_6_wifi));
            }
        }

        private void initFragmentsList(){

            Bundle arguments = new Bundle();
            arguments.putBoolean("isBasic", ((BandDetailsActivity)getActivity()).isBasic);
            BandDetailsSossFragment fragment = new BandDetailsSossFragment();
            fragment.setArguments(arguments);

            if(((BandDetailsActivity)getActivity()).isBasic) {
                fragmentArrayList.add(new BandDetailsOverviewFragment());
                fragmentArrayList.add(fragment);
                fragmentArrayList.add(new BandDetailsFallsFragment());
                fragmentArrayList.add(new BandDetailsAlarmZoneFragment());
                /*fragmentArrayList.add(new BandDetailsPulseFragment());
                fragmentArrayList.add(new BandDetailsPedometerFragment());
                fragmentArrayList.add(new BandDetailsBarometerFragment());
                fragmentArrayList.add(new BandDetailsWifiFragment());*/
            }else {
                fragmentArrayList.add(new BandDetailsOverviewFragment());
                fragmentArrayList.add(new BandDetailsPulseFragment());
                fragmentArrayList.add(new BandDetailsTemperatureFragment());
                fragmentArrayList.add(new BandDetailsPedometerFragment());
                fragmentArrayList.add(new BandDetailsBarometerFragment());
                fragmentArrayList.add(new BandDetailsFallsFragment());
                fragmentArrayList.add(fragment);
                //if (isUserPremium) {
                    //if (((BandDetailsActivity)getActivity()).hardwareLocation == 1)
                        fragmentArrayList.add(new BandDetailsAlarmZoneFragment());
                //}
                //fragmentArrayList.add(new BandDetailsMedicineAlarmsFragment());
                //fragmentArrayList.add(new BandDetailsWifiFragment());
            }
        }

    }


}
