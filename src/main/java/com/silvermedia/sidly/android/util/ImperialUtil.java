package com.silvermedia.sidly.android.util;

import org.joda.time.DateTime;

import java.util.Locale;

/**
 * Created by Volli on 2016-06-15.
 */
public class ImperialUtil {

    private volatile static boolean isImperial = false;

    public static float changeToFahrenheita(float temperature) {
        return temperature * 9 / 5 + 32;
    }


    public static String getStringTemperature(float temperature) {
        if (isImperial) {
            return String.format(Locale.getDefault(), " %.1f °F", changeToFahrenheita(temperature));
        }
        return String.format(Locale.getDefault(), " %.1f °C", temperature);
    }

    public static String getHoursDate(DateTime dateTime) {
        return dateTime.toString("HH:mm");
    }

    public static String getYearsDate(DateTime dateTime) {
        if (isImperial){
            return dateTime.toString("MM/dd/YYYY");
        }
        return dateTime.toString("dd/MM/YYYY");
    }

    public static String getFullDate(DateTime dateTime){
        return getYearsDate(dateTime) + ", " + getHoursDate(dateTime);
    }


    public static String getTempChar(){
        if (isImperial) return "°F";
        return "°C°";
    }


}