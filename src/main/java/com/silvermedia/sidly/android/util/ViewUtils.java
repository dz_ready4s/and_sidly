package com.silvermedia.sidly.android.util;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by dabu on 30.05.16.
 */
public class ViewUtils {
    public static void hideSoftKeyboard(Activity activity) {
        try{
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }catch (NullPointerException e){
        }
    }
}
