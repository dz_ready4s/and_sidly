package com.silvermedia.sidly.android.util;

import com.silvermedia.sidly.android.remote.requests.GetBandFirmwareUpgradeTask;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-12.
 */
public class BandFirmwareUpgradeUtil {
    private static boolean sCheckFirmwareUpgrade;
    private static BandFirmwareUpgradeUtil sInstance;

    private BandFirmwareUpgradeUtil(){
        sCheckFirmwareUpgrade = true;
    }

    public static BandFirmwareUpgradeUtil getInstance() {
        if (sInstance == null) {
            sInstance = new BandFirmwareUpgradeUtil();
        }
        return sInstance;
    }

    public void checkFirmwareUpgrade(){
        if(sCheckFirmwareUpgrade){
            sCheckFirmwareUpgrade = false;
        } else {
            return;
        }
        GetBandFirmwareUpgradeTask upgradeTask = new GetBandFirmwareUpgradeTask();
        upgradeTask.execute();
    }

    public void resetCheckFirmwareUpgrade(){
        sCheckFirmwareUpgrade = true;
    }
}
