package com.silvermedia.sidly.android.cdm.base;

import java.util.Map;

/**
 * Created by Piotrek on 2016-05-10.
 */
public abstract class Notification extends JsonSupport {
    public abstract Map<String, String> getParamsMap();
}
