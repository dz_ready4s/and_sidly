package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Response updating GCM instance ID.
 */
@SuppressWarnings("unused")
public class UpdateInstanceIdResponse extends Response {
	private boolean instanceIdUpdated;

	public UpdateInstanceIdResponse(boolean instanceIdUpdated) {
		this.instanceIdUpdated = instanceIdUpdated;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, UpdateInstanceIdResponse.class);
	}

	public boolean isInstanceIdUpdated() {
		return instanceIdUpdated;
	}

	public void setInstanceIdUpdated(boolean instanceIdUpdated) {
		this.instanceIdUpdated = instanceIdUpdated;
	}
}
