package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

import java.util.List;

/**
 * Request to update a list of medicine alarms.
 */
@SuppressWarnings("unused")
public class SetMedicineAlarmListRequest extends Request {
	private long userId;

	private String authenticationToken;

	private long imei;

	private List<GetMedicineAlarmListResponse.MedicineAlarm> medicineAlarmList;

	public SetMedicineAlarmListRequest(long userId, String authenticationToken, long imei, List<GetMedicineAlarmListResponse.MedicineAlarm>
		medicineAlarmList) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.imei = imei;
		this.medicineAlarmList = medicineAlarmList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, SetMedicineAlarmListRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

	public List<GetMedicineAlarmListResponse.MedicineAlarm> getMedicineAlarmList() {
		return medicineAlarmList;
	}

	public void setMedicineAlarmList(List<GetMedicineAlarmListResponse.MedicineAlarm> medicineAlarmList) {
		this.medicineAlarmList = medicineAlarmList;
	}
}
