package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with a list of falls for given time range.
 */
@SuppressWarnings("unused")
public class GetFallListResponse extends Response {
	private List<Fall> fallList;

	public GetFallListResponse(List<Fall> fallList) {
		this.fallList = fallList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetFallListResponse.class);
	}

	public List<Fall> getFallList() {
		return fallList;
	}

	public void setFallList(List<Fall> fallList) {
		this.fallList = fallList;
	}

	public static class Fall {
		private long timestampMillis;

		private double latitude;

		private double longitude;

		private int hardwareLocation;

		public Fall(long timestampMillis, double latitude, double longitude, int hardwareLocation) {
			this.timestampMillis = timestampMillis;
			this.latitude = latitude;
			this.longitude = longitude;
			this.hardwareLocation = hardwareLocation;
		}

		public int getHardwareLocation() {
			return hardwareLocation;
		}

		public void setHardwareLocation(int hardwareLocation) {
			this.hardwareLocation = hardwareLocation;
		}

		public long getTimestampMillis() {
			return timestampMillis;
		}

		public void setTimestampMillis(long timestampMillis) {
			this.timestampMillis = timestampMillis;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}
	}
}
