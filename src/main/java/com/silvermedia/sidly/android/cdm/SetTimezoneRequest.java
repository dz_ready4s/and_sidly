package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-04-12.
 */
public class SetTimezoneRequest extends Request {
    private String timezone;

    private String authenticationToken;

    private long userId;

    public SetTimezoneRequest(String timezone, String authenticationToken, long userId) {
        this.timezone = timezone;
        this.authenticationToken = authenticationToken;
        this.userId = userId;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this,SetTimezoneRequest.class);
    }
}
