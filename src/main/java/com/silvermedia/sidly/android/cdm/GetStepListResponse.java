package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Created by 8570p on 2016-03-29.
 */
public class GetStepListResponse extends Response {

    private List<Step> stepList;

    public GetStepListResponse(List<Step> stepList, int dailySteps) {
        this.stepList = stepList;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetStepListResponse.class);
    }

    public List<Step> getStepList() {
        return stepList;
    }

    public void setStepList(List<Step> stepList) {
        this.stepList = stepList;
    }

    public static class Step {
        private long timestampMillis;
        private int steps;
        private int dailySteps;

        public Step(long timestampMillis, int steps, int dailySteps) {
            this.timestampMillis = timestampMillis;
            this.steps = steps;
            this.dailySteps = dailySteps;
        }

        public long getTimestampMillis() {
            return timestampMillis;
        }

        public int getSteps(){
            return this.steps;
        }

        public void setSteps(int steps){
            this.steps = steps;
        }

        public void setTimestampMillis(long timestampMillis) {
            this.timestampMillis = timestampMillis;
        }

        public int getDailySteps(){return this.dailySteps;}

        public void setDailySteps(int dailySteps){
            this.dailySteps = dailySteps;
        }
    }
}
