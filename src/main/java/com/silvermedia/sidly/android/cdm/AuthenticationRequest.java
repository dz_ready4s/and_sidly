package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Authentication request sent over JSON.
 */
@SuppressWarnings("unused")
public class AuthenticationRequest extends Request {
	private String email;

	private String password;

	private String mobilePhoneModel;

	private String gcmInstanceId;

	public AuthenticationRequest(String email, String password, String mobilePhoneModel, String gcmInstanceId) {
		this.email = email;
		this.password = password;
		this.mobilePhoneModel = mobilePhoneModel;
		this.gcmInstanceId = gcmInstanceId;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, AuthenticationRequest.class);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobilePhoneModel() {
		return mobilePhoneModel;
	}

	public void setMobilePhoneModel(String mobilePhoneModel) {
		this.mobilePhoneModel = mobilePhoneModel;
	}

	public String getGcmInstanceId() {
		return gcmInstanceId;
	}

	public void setGcmInstanceId(String gcmInstanceId) {
		this.gcmInstanceId = gcmInstanceId;
	}
}
