package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Created by Piotrek on 2016-04-27.
 */
public class GetBandFirmwareUpgradeRequest extends Request {
    private long userId;

    private String authenticationToken;

    public GetBandFirmwareUpgradeRequest(long userId, String authenticationToken) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
    }

    public long getUserId() {
        return userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this,GetBandFirmwareUpgradeRequest.class);
    }
}
