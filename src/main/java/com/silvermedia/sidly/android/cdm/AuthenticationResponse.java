package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Authentication response sent over JSON.
 */
@SuppressWarnings("unused")
public class AuthenticationResponse extends Response {
	private long userId;

	private String authenticationToken;

	private int errorCode;

	private boolean isPremium;

	private boolean isUser;


	public AuthenticationResponse(long userId, String authenticationToken, boolean isPremium, int errorCode, boolean isUser) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.isPremium = isPremium;
		this.errorCode = errorCode;
		//TODO("Change isUser to get from argument of method")
		this.isUser = isUser;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, AuthenticationResponse.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public void setPremium(boolean premium) {
		isPremium = premium;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isUser(){return isUser;}

	public void setUser(boolean user){isUser = user;}
}
