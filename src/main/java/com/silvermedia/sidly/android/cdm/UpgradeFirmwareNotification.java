package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Notification;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Piotrek on 2016-04-26.
 */
public class UpgradeFirmwareNotification extends Notification {

    private Long imei;
    private Long oldfirmwareVersion;
    private Long newFirmwareVersion;
    private int sound;

    public UpgradeFirmwareNotification(Long imei, Long oldfirmwareVersion, Long newFirmwareVersion, int sound) {
        this.imei = imei;
        this.oldfirmwareVersion = oldfirmwareVersion;
        this.newFirmwareVersion = newFirmwareVersion;
        this.sound = sound;
    }

    @Override
    public Map<String, String> getParamsMap() {
        Map<String, String> params = new HashMap<>();
        params.put("imei", imei + "");
        params.put("oldfirmwareVersion", oldfirmwareVersion+"");
        params.put("newFirmwareVersion", newFirmwareVersion+"");
        return params;
    }


    @Override
    public String toJson() {
        return buildGson().toJson(this,UpgradeFirmwareNotification.class);
    }
}
