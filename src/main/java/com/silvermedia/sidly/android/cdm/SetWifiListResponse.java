package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Created by dabu on 16.02.16.
 */
@SuppressWarnings("unused")
public class SetWifiListResponse  extends Response {
    private boolean listUpdated;

    public SetWifiListResponse(boolean listUpdated) {
        this.listUpdated = listUpdated;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, SetWifiListResponse.class);
    }

    public boolean isListUpdated() {
        return listUpdated;
    }

    public void setListUpdated(boolean listUpdated) {
        this.listUpdated = listUpdated;
    }
}
