package com.silvermedia.sidly.android.cdm;

/**
 * Exception thrown by parsing code.
 */
@SuppressWarnings("WeakerAccess")
public class JsonException extends Exception {
	public JsonException(String message, Throwable cause) {
		super(message, cause);
	}
}
