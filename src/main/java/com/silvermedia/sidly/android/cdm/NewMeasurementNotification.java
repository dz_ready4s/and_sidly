package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Notification;

import java.util.HashMap;
import java.util.Map;

/**
 * notification about new measurement that's sent over WebSocket channel.
 */
@SuppressWarnings("unused")
public class NewMeasurementNotification extends Notification {
	private long imei;

	private String displayName;

	private String firstName;

	private String lastName;

	private float temperature;

	private int pulse;

	private boolean fall;

	private boolean sos;

	private boolean wearError;

	private boolean zone;

	private int sound;

	private boolean isBasic;

	private boolean pulseExceeded;

	public NewMeasurementNotification(long imei, String displayName, String firstName, String lastName, float temperature, int pulse, boolean fall,
	                                  boolean sos, boolean wearError, int sound, boolean isBasic, boolean pulseExceeded) {
		this.imei = imei;
		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.temperature = temperature;
		this.pulse = pulse;
		this.fall = fall;
		this.sos = sos;
		this.wearError = wearError;
		this.sound = sound;
		this.isBasic = isBasic;
		this.pulseExceeded = pulseExceeded;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, NewMeasurementNotification.class);
	}

	public Map<String, String> getParamsMap() {
		Map<String, String> params = new HashMap<>();
		params.put("imei", imei + "");
		params.put("displayName", displayName);
		params.put("firstName", firstName);
		params.put("lastName", lastName);
		params.put("temperature", temperature + "");
		params.put("pulse", pulse + "");
		params.put("fall", fall + "");
		params.put("sos", sos + "");
		params.put("wearError", wearError + "");
		params.put("zone",zone+"");
		params.put("isBasic", isBasic+"");
		params.put("pulseExceeded", pulseExceeded+"");
		return params;
	}

	public long getImei() {
		//imei = 865374030476504L;//TODO
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public boolean isFall() {
		return fall;
	}

	public void setFall(boolean fall) {
		this.fall = fall;
	}

	public boolean isSos() {
		return sos;
	}

	public void setSos(boolean sos) {
		this.sos = sos;
	}

	public boolean isWearError() {
		return wearError;
	}

	public void setWearError(boolean wearError) {
		this.wearError = wearError;
	}

	public boolean isZone() {
		return zone;
	}

	public int getSound() {
		return sound;
	}

	public void setSound(int sound) {
		this.sound = sound;
	}

	public void setZone(boolean zone) {

		this.zone = zone;
	}

	public boolean isBasic() {
		return isBasic;
	}

	public void setIsBasic(boolean isBasic) {
		this.isBasic = isBasic;
	}

	public boolean isPulseExceeded(){return pulseExceeded;}

	public void setPulseExceeded(boolean pulseExceeded){this.pulseExceeded = pulseExceeded;}
}
