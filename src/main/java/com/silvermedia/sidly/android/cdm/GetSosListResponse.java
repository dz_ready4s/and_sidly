package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with a list of SOS's for given time range.
 */
@SuppressWarnings("unused")
public class GetSosListResponse extends Response {
	private List<SOS> sosList;

	public GetSosListResponse(List<SOS> sosList) {
		this.sosList = sosList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetSosListResponse.class);
	}

	public List<SOS> getSosList() {
		return sosList;
	}

	public void setSosList(List<SOS> sosList) {
		this.sosList = sosList;
	}

	public static class SOS {
		private long timestampMillis;

		private double latitude;

		private double longitude;

		private int hardwareLocation;

		private boolean isBasic;

		public SOS(long timestampMillis, double latitude, double longitude, int hardwareLocation, boolean isBasic) {
			this.timestampMillis = timestampMillis;
			this.latitude = latitude;
			this.longitude = longitude;
			this.hardwareLocation = hardwareLocation;
			this.isBasic = isBasic;
		}

		public int getHardwareLocation() {
			return hardwareLocation;
		}

		public void setHardwareLocation(int hardwareLocation) {
			this.hardwareLocation = hardwareLocation;
		}

		public long getTimestampMillis() {
			return timestampMillis;
		}

		public void setTimestampMillis(long timestampMillis) {
			this.timestampMillis = timestampMillis;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		public boolean isBasic() {
			return isBasic;
		}

		public void setBasic(boolean basic) {
			isBasic = basic;
		}
	}
}
