package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.io.Serializable;
import java.util.List;

/**
 * List of associated bands.
 */
@SuppressWarnings("unused")
public class ListBandsResponse extends Response {
	private List<BandDetails> associatedBands;

	public ListBandsResponse(List<BandDetails> associatedBands) {
		this.associatedBands = associatedBands;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, ListBandsResponse.class);
	}

	public List<BandDetails> getAssociatedBands() {
		return associatedBands;
	}

	public void setAssociatedBands(List<BandDetails> associatedBands) {
		this.associatedBands = associatedBands;
	}

	public void addAssociatedBand(BandDetails bandDetails) {
		associatedBands.add(bandDetails);
	}

	public static class BandDetails implements Serializable {
		private long imei;

		private String displayName;

		private String firstName;

		private String lastName;

		private long lastMeasurementMillis;

		private float lastTemperature;

		private int lastHeartRate;

		private long lastFallMillis;

		private long lastSosMillis;

		private int battery;

		private boolean isWear;

		private int signal;

		private int bandHardwareLocation;

		private int measurementHardwareLocation;

		private boolean isBasic;

		public BandDetails(long imei, String displayName, String firstName, String lastName, long lastMeasurementMillis, float lastTemperature, int
			lastHeartRate, long lastFallMillis, long lastSosMillis, int battery, boolean wear, int signal, int bandHardwareLocation, int measurmentHardwareLocation, boolean isBasic) {
			this.imei = imei;
			this.displayName = displayName;
			this.firstName = firstName;
			this.lastName = lastName;
			this.lastMeasurementMillis = lastMeasurementMillis;
			this.lastTemperature = lastTemperature;
			this.lastHeartRate = lastHeartRate;
			this.lastFallMillis = lastFallMillis;
			this.lastSosMillis = lastSosMillis;
			this.battery = battery;
			this.isWear = wear;
			this.signal = signal;
			this.bandHardwareLocation = bandHardwareLocation;
			this.measurementHardwareLocation = measurmentHardwareLocation;
			this.isBasic = isBasic;
		}

		public int getSignal() {
			return signal;
		}

		public void setSignal(int signal) {
			this.signal = signal;
		}

		public boolean isIsWear() {
			return isWear;
		}

		public int getBandHardwareLocation() {
			return bandHardwareLocation;
		}

		public void setBandHardwareLocation(int bandHardwareLocation) {
			this.bandHardwareLocation = bandHardwareLocation;
		}

		public void setIsWear(boolean wear) {
			this.isWear = wear;
		}

		public long getImei() {
			return imei;
		}

		public void setImei(long imei) {
			this.imei = imei;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public long getLastMeasurementMillis() {
			return lastMeasurementMillis;
		}

		public void setLastMeasurementMillis(long lastMeasurementMillis) {
			this.lastMeasurementMillis = lastMeasurementMillis;
		}

		public float getLastTemperature() {
			return lastTemperature;
		}

		public void setLastTemperature(float lastTemperature) {
			this.lastTemperature = lastTemperature;
		}

		public int getLastHeartRate() {
			return lastHeartRate;
		}

		public void setLastHeartRate(int lastHeartRate) {
			this.lastHeartRate = lastHeartRate;
		}

		public long getLastFallMillis() {
			return lastFallMillis;
		}

		public void setLastFallMillis(long lastFallMillis) {
			this.lastFallMillis = lastFallMillis;
		}

		public long getLastSosMillis() {
			return lastSosMillis;
		}

		public void setLastSosMillis(long lastSosMillis) {
			this.lastSosMillis = lastSosMillis;
		}

		public int getBattery() { return battery; }

		public void setBattery(int battery) { this.battery = battery; }

		public boolean isBasic() {
			return isBasic;
		}

		public void setBasic(boolean basic) {
			isBasic = basic;
		}

		public boolean isWear() {
			return isWear;
		}

		public void setWear(boolean wear) {
			isWear = wear;
		}

		public int getMeasurementHardwareLocation() {
			return measurementHardwareLocation;
		}

		public void setMeasurementHardwareLocation(int measurementHardwareLocation) {
			this.measurementHardwareLocation = measurementHardwareLocation;
		}
	}
}
