package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-04-12.
 */
public class SetTimezoneResponse extends Response {
    private boolean success;

    @Override
    public String toJson() {
        return buildGson().toJson(this,SetTimezoneResponse.class);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public SetTimezoneResponse(boolean success) {

        this.success = success;
    }
}
