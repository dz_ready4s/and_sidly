package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Created by 8570p on 2016-03-29.
 */
public class GetStepListRequest extends Request {

    private long userId;

    private String authenticationToken;

    private long imei;

    private long startTimeMillis;
    private long endTimeMillis;

    public GetStepListRequest(long userId, String authenticationToken, long imei, long startTimeMillis, long endTimeMillis) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
        this.imei = imei;
        this.startTimeMillis = startTimeMillis;
        this.endTimeMillis = endTimeMillis;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetStepListRequest.class);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public long getDays() {
        return startTimeMillis;
    }

    public void setDays(long startTimeMillis) {
        this.startTimeMillis = startTimeMillis;
    }

    public long getEndTimeMillis() {
        return endTimeMillis;
    }

    public void setEndTimeMillis(long endTimeMillis) {
        this.endTimeMillis = endTimeMillis;
    }
}
