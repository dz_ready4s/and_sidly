package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with band data and information about last 12 hours of pulse and temperature.
 */
@SuppressWarnings("unused")
public class GetBandOverviewResponse extends Response {
	private long bandImei;

	private String displayName;

	private String firstName;

	private String lastName;

	private GetFallListResponse.Fall lastFall;

	private GetSosListResponse.SOS lastSos;

	private List<GetPulseListResponse.Pulse> pulseList;

	private List<GetTemperatureListResponse.Temperature> temperatureList;

	private int battery;

	private boolean configurationSynced;

	private boolean isBasic;

	public GetBandOverviewResponse(long bandImei, String displayName, String firstName, String lastName, GetFallListResponse.Fall lastFall,
	                               GetSosListResponse.SOS lastSos, List<GetPulseListResponse.Pulse> pulseList, List<GetTemperatureListResponse
		.Temperature> temperatureList, int battery, boolean configurationSynced, int hardwareLocation, boolean isBasic) {
		this.bandImei = bandImei;
		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastFall = lastFall;
		this.lastSos = lastSos;
		this.pulseList = pulseList;
		this.temperatureList = temperatureList;
		this.battery = battery;
		this.configurationSynced = configurationSynced;
		this.isBasic = isBasic;//TODO
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetBandOverviewResponse.class);
	}

	public long getBandImei() {
		return bandImei;
	}

	public void setBandImei(long bandImei) {
		this.bandImei = bandImei;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public GetFallListResponse.Fall getLastFall() {
		return lastFall;
	}

	public void setLastFall(GetFallListResponse.Fall lastFall) {
		this.lastFall = lastFall;
	}

	public GetSosListResponse.SOS getLastSos() {
		return lastSos;
	}

	public void setLastSos(GetSosListResponse.SOS lastSos) {
		this.lastSos = lastSos;
	}

	public List<GetPulseListResponse.Pulse> getPulseList() {
		return pulseList;
	}

	public void setPulseList(List<GetPulseListResponse.Pulse> pulseList) {
		this.pulseList = pulseList;
	}

	public List<GetTemperatureListResponse.Temperature> getTemperatureList() {
		return temperatureList;
	}

	public void setTemperatureList(List<GetTemperatureListResponse.Temperature> temperatureList) {
		this.temperatureList = temperatureList;
	}

	public int getBattery() {
		return battery;
	}

	public void setBattery(int battery) {
		this.battery = battery;
	}

	public boolean isConfigurationSynced() {
		return configurationSynced;
	}

	public void setConfigurationSynced(boolean configurationSynced) {
		this.configurationSynced = configurationSynced;
	}

	public boolean isBasic() {
		return isBasic;
	}

	public void setBasic(boolean basic) {
		isBasic = true;
	}//TODO
}
