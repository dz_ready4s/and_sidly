package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Response with band data.
 */
@SuppressWarnings("unused")
public class GetBandPropertiesResponse extends Response {
	private long bandImei;

	private String displayName;

	private String firstName;

	private String lastName;

	private int age;

	private int dailySteps;

	private int measurementInterval;

	private List<String> icePhoneNumbers;

	private boolean configurationSynced;

	public GetBandPropertiesResponse(){
		icePhoneNumbers = new ArrayList<>();
	}

	public GetBandPropertiesResponse(long bandImei, String displayName, String firstName, String lastName, int age, int dailySteps, int measurementInterval,
	                                 List<String> icePhoneNumbers, boolean configurationSynced) {
		this.bandImei = bandImei;
		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.dailySteps = dailySteps;
		this.measurementInterval = measurementInterval;
		this.icePhoneNumbers = icePhoneNumbers;
		this.configurationSynced = configurationSynced;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetBandPropertiesResponse.class);
	}

	public long getBandImei() {
		return bandImei;
	}

	public void setBandImei(long bandImei) {
		this.bandImei = bandImei;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getDailySteps() {
		return dailySteps;
	}

	public void setDailySteps(int dailySteps) {
		this.dailySteps = dailySteps;
	}

	public int getMeasurementInterval() {
		return measurementInterval;
	}

	public void setMeasurementInterval(int measurementInterval) {
		this.measurementInterval = measurementInterval;
	}

	public List<String> getIcePhoneNumbers() {
		return icePhoneNumbers;
	}

	public void setIcePhoneNumbers(List<String> icePhoneNumbers) {
		this.icePhoneNumbers = icePhoneNumbers;
	}

	public boolean isConfigurationSynced() {
		return configurationSynced;
	}

	public void setConfigurationSynced(boolean configurationSynced) {
		this.configurationSynced = configurationSynced;
	}
}
