package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with a list of pulse measurements for given time range.
 */
@SuppressWarnings("unused")
public class GetPulseListResponse extends Response {
	private List<Pulse> pulseList;

	public GetPulseListResponse(List<Pulse> pulseList) {
		this.pulseList = pulseList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetPulseListResponse.class);
	}

	public List<Pulse> getPulseList() {
		return pulseList;
	}

	public void setPulseList(List<Pulse> pulseList) {
		this.pulseList = pulseList;
	}

	public static class Pulse {
		private long timestampMillis;

		private int pulse;

		public Pulse(long timestampMillis, int pulse) {
			this.timestampMillis = timestampMillis;
			this.pulse = pulse;
		}

		public long getTimestampMillis() {
			return timestampMillis;
		}

		public void setTimestampMillis(long timestampMillis) {
			this.timestampMillis = timestampMillis;
		}

		public int getPulse() {
			return pulse;
		}

		public void setPulse(int pulse) {
			this.pulse = pulse;
		}
	}
}
