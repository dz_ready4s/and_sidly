package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 8570p on 2016-04-08.
 */
public class SetLanguageRequest extends Request {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetLanguageRequest.class);
    private long userId;

    private String authenticationToken;

    private String language;

    public SetLanguageRequest(long userId, String authenticationToken, String language) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
        this.language = language;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, SetLanguageRequest.class);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getLanguage() {
        if(language==null){
            return "EN";
        }
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
