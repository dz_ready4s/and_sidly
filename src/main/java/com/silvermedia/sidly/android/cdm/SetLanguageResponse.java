package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Created by 8570p on 2016-04-08.
 */
public class SetLanguageResponse extends Response {
    public static final int ERR_USER_AUTHENTICATE = 0;
    public static final int ERR_BAD_LANGUAGE = 1;

    private boolean success;
    private Integer errorCode;

    public SetLanguageResponse(boolean success) {
        this.success = success;
    }

    public SetLanguageResponse(boolean success, Integer errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, SetLanguageResponse.class);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
