package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Created by Piotrek on 2016-04-27.
 */
public class SetBandFirmwareUpgradeStateRequest extends Request {

    private long userId;

    private String authenticationToken;

    private Integer state;

    public SetBandFirmwareUpgradeStateRequest(long userId, String authenticationToken, int state) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
        this.state = state;
    }

    public long getUserId() {
        return userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public Integer getState() {
        return state;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this,SetBandFirmwareUpgradeStateRequest.class);
    }
}
