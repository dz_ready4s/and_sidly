package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Created by 8570p on 2016-03-29.
 */
public class GetBarometerListResponse extends Response {
    private List<Barometer> barometerList;

    public GetBarometerListResponse(List<Barometer> barometerList) {
        this.barometerList = barometerList;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetPulseListResponse.class);
    }

    public List<Barometer> getBarometerList() {
        return barometerList;
    }

    public void setBarometerList(List<Barometer> barometerList) {
        this.barometerList = barometerList;
    }

    public static class Barometer {
        private long timestampMillis;

        private float externalTemperature;

        private float externalPressure;

        public Barometer(long timestampMillis, float externalTemperature, float externalPressure) {
            this.timestampMillis = timestampMillis;
            this.externalTemperature = externalTemperature;
            this.externalPressure = externalPressure;
        }

        public long getTimestampMillis() {
            return timestampMillis;
        }

        public void setTimestampMillis(long timestampMillis) {
            this.timestampMillis = timestampMillis;
        }

        public float getExternalTemperature() {
            return externalTemperature;
        }

        public void setExternalTemperature(float externalTemperature) {
            this.externalTemperature = externalTemperature;
        }

        public float getExternalPressure() {
            return externalPressure;
        }

        public void setExternalPressure(int externalPressure) {
            this.externalPressure = externalPressure;
        }
    }
}
