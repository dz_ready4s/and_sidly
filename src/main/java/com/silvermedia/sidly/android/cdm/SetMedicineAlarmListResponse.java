package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Response for updating list of medicine alarms.
 */
@SuppressWarnings("unused")
public class SetMedicineAlarmListResponse extends Response {
	private boolean listUpdated;

	public SetMedicineAlarmListResponse(boolean listUpdated) {
		this.listUpdated = listUpdated;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, SetMedicineAlarmListResponse.class);
	}

	public boolean isListUpdated() {
		return listUpdated;
	}

	public void setListUpdated(boolean listUpdated) {
		this.listUpdated = listUpdated;
	}
}
