package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Request for list of associated bands.
 */
@SuppressWarnings("unused")
public class ListBandsRequest extends Request {
	private long userId;

	private String authenticationToken;

	public ListBandsRequest(long userId, String authenticationToken) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, ListBandsRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}
}
