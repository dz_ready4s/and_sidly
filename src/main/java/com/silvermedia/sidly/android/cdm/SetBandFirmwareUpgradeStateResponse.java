package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Created by Piotrek on 2016-04-27.
 */
public class SetBandFirmwareUpgradeStateResponse extends Response {
    private boolean success;
    private Integer errorCode;

    public SetBandFirmwareUpgradeStateResponse(boolean success, Integer errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }

    public SetBandFirmwareUpgradeStateResponse(boolean success) {
        this.success = success;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this,SetBandFirmwareUpgradeStateResponse.class);
    }

    public boolean isSuccess() {
        return success;
    }

    public Integer getErrorCode() {
        return errorCode;
    }
}
