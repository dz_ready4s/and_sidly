package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with a list of medicine alarms.
 */
@SuppressWarnings("unused")
public class GetMedicineAlarmListResponse extends Response {
	private List<MedicineAlarm> medicineAlarmList;

	public GetMedicineAlarmListResponse(List<MedicineAlarm> medicineAlarmList) {
		this.medicineAlarmList = medicineAlarmList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetMedicineAlarmListResponse.class);
	}

	public List<MedicineAlarm> getMedicineAlarmList() {
		return medicineAlarmList;
	}

	public void setMedicineAlarmList(List<MedicineAlarm> medicineAlarmList) {
		this.medicineAlarmList = medicineAlarmList;
	}

	public static class MedicineAlarm {
		private String time;

		private String displayName;

		public MedicineAlarm(String time, String displayName) {
			this.time = time;
			this.displayName = displayName;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}
	}
}
