package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Response with a list of temperatures for given time range.
 */
@SuppressWarnings("unused")
public class GetTemperatureListResponse extends Response {
	private List<Temperature> temperatureList;

	public GetTemperatureListResponse(List<Temperature> temperatureList) {
		this.temperatureList = temperatureList;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetTemperatureListResponse.class);
	}

	public List<Temperature> getTemperatureList() {
		return temperatureList;
	}

	public void setTemperatureList(List<Temperature> temperatureList) {
		this.temperatureList = temperatureList;
	}

	public static class Temperature {
		private long timestampMillis;

		private float temperature;

		public Temperature(long timestampMillis, float temperature) {
			this.timestampMillis = timestampMillis;
			this.temperature = temperature;
		}

		public long getTimestampMillis() {
			return timestampMillis;
		}

		public void setTimestampMillis(long timestampMillis) {
			this.timestampMillis = timestampMillis;
		}

		public float getTemperature() {
			return temperature;
		}

		public void setTemperature(float temperature) {
			this.temperature = temperature;
		}
	}
}
