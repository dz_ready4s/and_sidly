package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Update GCM instance ID.
 */
@SuppressWarnings("unused")
public class UpdateInstanceIdRequest extends Request {
	private long userId;

	private String authenticationToken;

	private String instanceId;

	public UpdateInstanceIdRequest(long userId, String authenticationToken, String instanceId) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.instanceId = instanceId;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, UpdateInstanceIdRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
