package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Created by dabu on 08.02.16.
 */
public class GetErrorResponse extends Response{
    private String error;

    public GetErrorResponse(){}

    public void setError(String error){
        this.error = error;
    }

    public String getError(){
        return error;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetErrorResponse.class);
    }
}
