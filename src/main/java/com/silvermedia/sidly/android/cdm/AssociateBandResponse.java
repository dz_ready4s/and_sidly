package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Response for band association request.
 */
@SuppressWarnings("unused")
public class AssociateBandResponse extends Response {
	private boolean bandAssociated;

	public AssociateBandResponse(boolean bandAssociated) {
		this.bandAssociated = bandAssociated;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, AssociateBandResponse.class);
	}

	public boolean isBandAssociated() {
		return bandAssociated;
	}

	public void setBandAssociated(boolean bandAssociated) {
		this.bandAssociated = bandAssociated;
	}
}
