package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Request for details for given band for overview view (capped to last 12 hours).
 */
@SuppressWarnings("unused")
public class GetBandOverviewRequest extends Request {
	private long userId;

	private String authenticationToken;

	private long imei;

	public GetBandOverviewRequest(long userId, String authenticationToken, long imei) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.imei = imei;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetBandOverviewRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}
}
