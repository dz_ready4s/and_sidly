package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

public class SetAcceptAlarmRequest extends Request {

    private String authenticationToken;

    private long imei;

    public SetAcceptAlarmRequest(String authenticationToken, long imei) {
        this.authenticationToken = authenticationToken;
        this.imei = imei;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, SetAcceptAlarmRequest.class);
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }
}
