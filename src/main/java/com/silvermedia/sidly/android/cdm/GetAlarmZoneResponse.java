package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Created by Volli on 2016-06-08.
 */
 public class GetAlarmZoneResponse extends Response {
        private List<SOS> alarmZoneList;

        public GetAlarmZoneResponse(List<SOS> alarmZoneList) {
            this.alarmZoneList = alarmZoneList;
        }

        @Override
        public String toJson() {
            return buildGson().toJson(this, GetAlarmZoneResponse.class);
        }

        public List<SOS> getSosList() {
            return alarmZoneList;
        }

        public void setSosList(List<SOS> alarmZoneList) {
            this.alarmZoneList = alarmZoneList;
        }

        public static class SOS {
            private long timestampMillis;

            private double latitude;

            private double longitude;

            private int hardwareLocation;

            public SOS(long timestampMillis, double latitude, double longitude, int hardwareLocation) {
                this.timestampMillis = timestampMillis;
                this.latitude = latitude;
                this.longitude = longitude;
                this.hardwareLocation = hardwareLocation;
            }

            public long getTimestampMillis() {
                return timestampMillis;
            }

            public void setTimestampMillis(long timestampMillis) {
                this.timestampMillis = timestampMillis;
            }

            public int getHardwareLocation() {
                return hardwareLocation;
            }

            public void setHardwareLocation(int hardwareLocation) {
                this.hardwareLocation = hardwareLocation;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }
        }
    }