package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Created by dabu on 16.02.16.
 */
@SuppressWarnings("unused")
public class GetWifiListRequest extends Request {
    private long userId;

    private String authenticationToken;

    private long imei;

    public GetWifiListRequest(long userId, String authenticationToken, long imei) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
        this.imei = imei;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetWifiListRequest.class);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }
}
