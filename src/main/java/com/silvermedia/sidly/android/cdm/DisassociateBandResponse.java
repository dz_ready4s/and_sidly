package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Response for band disassociation request.
 */
@SuppressWarnings("unused")
public class DisassociateBandResponse extends Response {
	public DisassociateBandResponse() { }

	@Override
	public String toJson() {
		return buildGson().toJson(this, DisassociateBandResponse.class);
	}
}
