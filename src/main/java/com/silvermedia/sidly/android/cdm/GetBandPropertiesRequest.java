package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Request to get band data.
 */
@SuppressWarnings("unused")
public class GetBandPropertiesRequest extends Request {
	private long userId;

	private String authenticationToken;

	private long bandImei;

	public GetBandPropertiesRequest(long userId, String authenticationToken, long bandImei) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.bandImei = bandImei;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetBandPropertiesRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public long getBandImei() {
		return bandImei;
	}

	public void setBandImei(long bandImei) {
		this.bandImei = bandImei;
	}
}
