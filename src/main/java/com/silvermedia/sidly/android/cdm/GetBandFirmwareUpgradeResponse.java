package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Created by Piotrek on 2016-04-27.
 */
public class GetBandFirmwareUpgradeResponse extends Response {
    private boolean success;

    private boolean canUpgradeFirmware;

    private List<Long> bandImeiToUpgrade;

    private String message;

    private Integer errorCode;

    public GetBandFirmwareUpgradeResponse(boolean success, boolean canUpgradeFirmware, List<Long>
            bandImeiToUpgrade, String message) {
        this.success = success;
        this.canUpgradeFirmware = canUpgradeFirmware;
        this.bandImeiToUpgrade = bandImeiToUpgrade;
        this.message = message;
    }

    public GetBandFirmwareUpgradeResponse(boolean success, Integer errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this,GetBandFirmwareUpgradeResponse.class);
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isCanUpgradeFirmware() {
        return canUpgradeFirmware;
    }

    public List<Long> getBandImeiToUpgrade() {
        return bandImeiToUpgrade;
    }

    public String getMessage() {
        return message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }
}
