package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

import java.util.List;

/**
 * Created by dabu on 16.02.16.
 */
@SuppressWarnings("unused")
public class GetWifiListResponse extends Response {

    private List<WifiObject> bandWifiList;

    public GetWifiListResponse(List<WifiObject> wifiList) {
        this.bandWifiList = wifiList;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, GetWifiListResponse.class);
    }

    public List<WifiObject> getBandWifiList() {
        return bandWifiList;
    }

    public void setBandWifiList(List<WifiObject> bandWifiList) {
        this.bandWifiList = bandWifiList;
    }

    public static class WifiObject {
        private String name;

        private String pass;

        public WifiObject(String name, String pass) {
            this.name = name;
            this.pass = pass;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }
    }

}
