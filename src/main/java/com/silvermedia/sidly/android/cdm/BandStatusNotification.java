package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Notification;

import java.util.HashMap;
import java.util.Map;

/**

 * Created by Kacper on 07.09.2016.
 */
public class BandStatusNotification extends Notification {

    private long imei;

    private String displayName;

    private String firstName;

    private String lastName;

    private Boolean isBasic;

    private int notificationType;
    private int sound;

    public BandStatusNotification(long imei, String displayName, String firstName, String lastName, int notificationType, int sound, boolean isBasic) {
        this.imei = imei;
        this.displayName = displayName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.notificationType = notificationType;
        this.sound = sound;
        this.isBasic = isBasic;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, BandStatusNotification.class);
    }

    @Override
    public Map<String, String> getParamsMap() {
        Map<String, String> params = new HashMap<>();
        params.put("notificationType", notificationType +"");
        params.put("imei", imei + "");
        params.put("displayName", displayName);
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        return params;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notificationType) {
        this.notificationType = notificationType;
    }

    public int getSound() {
        return sound;
    }

    public void setSound(int sound) {
        this.sound = sound;
    }

    public void setIsBasic(boolean isBasic){
        this.isBasic = isBasic;
    }

    public boolean isBasic(){
        return isBasic;
    }
}
