package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

/**
 * Request for a list of falls for given band.
 */
@SuppressWarnings("unused")
public class GetFallListRequest extends Request {
	private long userId;

	private String authenticationToken;

	private long imei;

	private long startTimeMillis;

	private long endTimeMillis;

	public GetFallListRequest(long userId, String authenticationToken, long imei, long startTimeMillis, long endTimeMillis) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.imei = imei;
		this.startTimeMillis = startTimeMillis;
		this.endTimeMillis = endTimeMillis;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, GetFallListRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public long getImei() {
		return imei;
	}

	public void setImei(long imei) {
		this.imei = imei;
	}

	public long getStartTimeMillis() {
		return startTimeMillis;
	}

	public void setStartTimeMillis(long startTimeMillis) {
		this.startTimeMillis = startTimeMillis;
	}

	public long getEndTimeMillis() {
		return endTimeMillis;
	}

	public void setEndTimeMillis(long endTimeMillis) {
		this.endTimeMillis = endTimeMillis;
	}
}
