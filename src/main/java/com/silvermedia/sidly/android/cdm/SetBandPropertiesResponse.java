package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Response;

/**
 * Response for band update request.
 */
@SuppressWarnings("unused")
public class SetBandPropertiesResponse extends Response {
	private boolean bandUpdated;

	public SetBandPropertiesResponse(boolean bandUpdated) {
		this.bandUpdated = bandUpdated;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, SetBandPropertiesResponse.class);
	}

	public boolean isBandUpdated() {
		return bandUpdated;
	}

	public void setBandUpdated(boolean bandUpdated) {
		this.bandUpdated = bandUpdated;
	}
}
