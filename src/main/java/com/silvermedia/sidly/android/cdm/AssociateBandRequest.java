package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

import java.util.List;

/**
 * Request to associate a band with a user, also sets display name.
 */
@SuppressWarnings("unused")
public class AssociateBandRequest extends Request {
	private long userId;

	private String authenticationToken;

	private long bandImei;

	private String displayName;

	private String firstName;

	private String lastName;

	private int age;

	private int dailySteps;

	private List<String> icePhoneNumbers;

	public AssociateBandRequest(long userId, String authenticationToken, long bandImei, String displayName, String firstName, String lastName, int
		age, int dailySteps, List<String> icePhoneNumbers) {
		this.userId = userId;
		this.authenticationToken = authenticationToken;
		this.bandImei = bandImei;
		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.dailySteps = dailySteps;
		this.icePhoneNumbers = icePhoneNumbers;
	}

	@Override
	public String toJson() {
		return buildGson().toJson(this, AssociateBandRequest.class);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public long getBandImei() {
		return bandImei;
	}

	public void setBandImei(long bandImei) {
		this.bandImei = bandImei;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getDailySteps() {
		return dailySteps;
	}

	public void setDailySteps(int dailySteps) {
		this.dailySteps = dailySteps;
	}

	public List<String> getIcePhoneNumbers() {
		return icePhoneNumbers;
	}

	public void setIcePhoneNumbers(List<String> icePhoneNumbers) {
		this.icePhoneNumbers = icePhoneNumbers;
	}
}
