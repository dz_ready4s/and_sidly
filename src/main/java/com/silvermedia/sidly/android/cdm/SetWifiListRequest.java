package com.silvermedia.sidly.android.cdm;

import com.silvermedia.sidly.android.cdm.base.Request;

import java.util.List;

/**
 * Created by dabu on 16.02.16.
 */
@SuppressWarnings("unused")
public class SetWifiListRequest extends Request {
    private long userId;

    private String authenticationToken;

    private long imei;

    private List<GetWifiListResponse.WifiObject> bandWifiList;

    public SetWifiListRequest(long userId, String authenticationToken, long imei, List<GetWifiListResponse.WifiObject>
            wifiList) {
        this.userId = userId;
        this.authenticationToken = authenticationToken;
        this.imei = imei;
        this.bandWifiList = wifiList;
    }

    @Override
    public String toJson() {
        return buildGson().toJson(this, SetWifiListRequest.class);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public List<GetWifiListResponse.WifiObject> getBandWifiList() {
        return bandWifiList;
    }

    public void setBandWifiList(List<GetWifiListResponse.WifiObject> bandWifiList) {
        this.bandWifiList = bandWifiList;
    }
}
