package com.silvermedia.sidly.android.cdm.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.silvermedia.sidly.android.cdm.JsonException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Base class for JSON-serializable protocol classes.
 */
@SuppressWarnings("unused")
public abstract class JsonSupport {
	@NotNull
	public static <T extends JsonSupport> T fromStream(@NotNull InputStream stream, Class<T> tClass) throws JsonException {
		try {
			return buildGson().fromJson(new InputStreamReader(stream), tClass);
		} catch (JsonParseException e) {
			throw new JsonException("Failed to parse data from stream", e);
		}
	}

	@NotNull
	public static <T extends JsonSupport> T fromString(@NotNull String string, Class<T> tClass) throws JsonException {
		try {
			return buildGson().fromJson(string, tClass);
		} catch (JsonParseException e) {
			throw new JsonException("Failed to parse data from string", e);
		}
	}

	@NotNull
	public static Gson buildGson() {
		return new GsonBuilder().setVersion(1.0).serializeNulls().create();
	}

	public static String serializedNull() {
		return buildGson().toJson(JsonNull.INSTANCE);
	}

	public abstract String toJson();

	public final void writeToStream(@NotNull OutputStream stream) throws IOException {
		OutputStreamWriter writer = new OutputStreamWriter(stream);
		writer.write(toJson());
		writer.flush();
	}
}
