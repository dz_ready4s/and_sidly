package com.silvermedia.sidly.android.activity;

import android.support.v7.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.cdm.ListBandsResponse;
import com.silvermedia.sidly.android.fragment.DashboardFragment;
import com.silvermedia.sidly.android.fragment.LoginFragment;
import com.silvermedia.sidly.android.remote.requests.SetLanguageTask;
import com.silvermedia.sidly.android.remote.requests.SetTimezoneTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainActivity.class);
    public static final String FILTER_UPDATE_UI = "updateDashboardUi";

    private DashboardFragment dashboardFragment;
    protected BroadcastReceiver mUpdateDashboardReceiver;
    final private ArrayList<ListBandsResponse.BandDetails> associatedBands = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.main_activity);

        mUpdateDashboardReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Fragment f = getFragmentManager().findFragmentById(R.id.main_activity_frame);
                if(f != null){
                    if(f instanceof DashboardFragment){
                        ((DashboardFragment) f).refreshFragment();
                    }
                }
            }
        };
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            Toast.makeText(this, R.string.main_activity_no_network, Toast.LENGTH_SHORT).show();
        }

        App app = (App) getApplication();
        if (app.getCurrentUser() != null) {
            if(app.getCurrentUser().isUser()){
                LOGGER.info("Current user: {}", app.getCurrentUser());
                Intent intent = new Intent(this, BandDetailsActivity.class);
                startActivity(intent);
                return;
            }


            LOGGER.info("Current user: {}", app.getCurrentUser());
            createDashboardFragment();
            getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).add(R.id
                    .main_activity_frame, dashboardFragment).commit();

            SetTimezoneTask setTimezoneTask = new SetTimezoneTask();
            setTimezoneTask.execute();
            SetLanguageTask setLanguageTask = new SetLanguageTask();
            setLanguageTask.execute();
            return;
        }

        // Show login fragment:
        LOGGER.info("No current user in settings, showing login fragment");
        getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).add(R.id
                .main_activity_frame, new LoginFragment()).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mUpdateDashboardReceiver, new IntentFilter(FILTER_UPDATE_UI));
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        LOGGER.debug("Checking Google Play services availability: {}", status);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.showErrorDialogFragment(this, status, 0xf00f, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        MainActivity.this.finish();
                    }
                });
            } else {
                googleApiAvailability.showErrorNotification(this, status);
                finish();
            }
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mUpdateDashboardReceiver);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (dashboardFragment != null) {
            dashboardFragment.onConfigurationChanged(newConfig);
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return (dashboardFragment != null && dashboardFragment.onOptionsItemSelected(item)) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (dashboardFragment != null) {
            dashboardFragment.syncDrawerState();
        }
    }

    public void showMainView() {
        if(App.getInstance().getCurrentUser().isUser()){
            Intent intent = new Intent(this, BandDetailsActivity.class);
            startActivity(intent);
        }
        createDashboardFragment();
        getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id
                .main_activity_frame, dashboardFragment).commit();
    }

    private void createDashboardFragment() {
        if (dashboardFragment == null) {
            dashboardFragment = new DashboardFragment();

            Bundle arguments = new Bundle();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            if (!preferences.contains(SettingsKeys.RT_SEEN_DRAWER)) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(SettingsKeys.RT_SEEN_DRAWER, true);
                editor.apply();
                arguments.putBoolean("openDrawer", true);
            }
            Fragment f = getFragmentManager().findFragmentById(R.id.main_activity_frame);
            if(f != null){
                if(f instanceof DashboardFragment){
                    ((DashboardFragment) f).refreshFragment();
                }
            }

            dashboardFragment.setArguments(arguments);
        }
    }

    @Override
    public void onBackPressed() {
        if (dashboardFragment != null) {
            if (dashboardFragment.getDrawerLayout().isDrawerOpen(GravityCompat.START)) {
                dashboardFragment.getDrawerLayout().closeDrawer(GravityCompat.START);
            } else {
                showCloseAlertDialog();
            }
        } else {
            showCloseAlertDialog();
        }
    }

    private void showCloseAlertDialog() {
        new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle(R.string.dashboard_fragment_close_message)
                .setPositiveButton(R.string.dashboard_fragment_close_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .setNegativeButton(R.string.dashboard_fragment_close_no, null)
                .create()
                .show();
    }
}
