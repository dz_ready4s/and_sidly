package com.silvermedia.sidly.android.activity;

import android.os.Bundle;

import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.fragment.SettingsFragment;

public class SettingsActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.settings_activity);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().replace(R.id.settings_activity_frame, new SettingsFragment()).commit();
		}
	}
}
