package com.silvermedia.sidly.android.activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.ListBandsRequest;
import com.silvermedia.sidly.android.cdm.ListBandsResponse;
import com.silvermedia.sidly.android.cdm.SetAcceptAlarmRequest;
import com.silvermedia.sidly.android.fragment.BandDetailsFragment;

import com.silvermedia.sidly.android.fragment.LoginFragment;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BandDetailsActivity extends BaseActivity {
	public static final String IMEI_KEY = "__imei";

	public static final String DISPLAY_NAME_KEY = "__displayName";

	public static final String HARDWARE_LOCATION = "__isWear";

	public static final String IS_BASIC = "__isBasic";

	private static final String CACHE_KEY = "__associatedBands";

	public static final String DISPLAY_ALARM_DIALOG = "__displayAlarmDialog";

	private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailsActivity.class);

	public long imei = -1;

    public String displayName;

    public int hardwareLocation;

    public boolean isBasic;

	final private ArrayList<ListBandsResponse.BandDetails> associatedBands = new ArrayList<>();

	private ListBandsTask listBandsTask;

	private Callback callback;

	private ActionBarDrawerToggle actionBarDrawerToggle;

	private DrawerLayout drawerLayout;

	private ListView drawerList;

	public DrawerLayout getDrawerLayout() {
		return drawerLayout;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (App.getInstance().getCurrentUser().isUser()){ //do when user is patient
			super.onCreate(savedInstanceState);
			setContentView(R.layout.band_details_activity);
			drawerLayout = (DrawerLayout)findViewById(R.id.band_details_activity_drawer_layout);
			drawerList = (ListView)drawerLayout.findViewById(R.id.band_details_activity_drawer);
			listBandsTask = new ListBandsTask();
			listBandsTask.execute();
			setDrawerLayout();
            getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id
                    .band_details_activity_frame, new BandDetailsFragment()).commit();
			return;
		}

		if (savedInstanceState != null) {
			imei = savedInstanceState.getLong(IMEI_KEY, -1);
			displayName = savedInstanceState.getString(DISPLAY_NAME_KEY, null);
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.band_details_activity);
		drawerLayout = (DrawerLayout)findViewById(R.id.band_details_activity_drawer_layout);
		drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		imei = getIntent().getLongExtra(IMEI_KEY, -1);
		if (imei == -1) {
			LOGGER.error("Failed to retrieve IMEI from Intent");
			finish();
		}
		displayName = getIntent().getStringExtra(DISPLAY_NAME_KEY);
		if (displayName != null) {
			setTitle(displayName);
		} else {
			LOGGER.warn("Failed to find {} in extra data in intent", DISPLAY_NAME_KEY);
			displayName = getString(R.string.app_name);
		}

		hardwareLocation = getIntent().getIntExtra(HARDWARE_LOCATION, -1);
		if(hardwareLocation == -1){
			LOGGER.warn("Failed to find {} in extra data in intent", DISPLAY_NAME_KEY);
		}

		isBasic = getIntent().getBooleanExtra(IS_BASIC, true);
        getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id
                .band_details_activity_frame, new BandDetailsFragment()).commit();
	}

	private void setDrawerLayout(){
		if(drawerList != null) {
			drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					@SuppressWarnings("unchecked") Map<String, Object> item = (Map<String, Object>) parent.getItemAtPosition(position);

					if ("static".equals(item.get("type"))) {
						switch ((Integer) item.get("key")) {
							case 1:
								closeDrawer();
								callback.setCurrentPageViewItem();
								listBandsTask = new ListBandsTask();
								listBandsTask.execute();
								break;
							case 2:
								closeDrawer();
								startActivity(new Intent(BandDetailsActivity.this, SettingsActivity.class));
								break;
							case 3:
								SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(BandDetailsActivity.this);
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SettingsKeys.PREF_HELPDESK_URL)));
								break;
							//Logout
							case 4:
								closeDrawer();
								App.getInstance().setCurrentUser(null);
								App.getInstance().clearAssociatedBandsCache();
								startActivity(new Intent(BandDetailsActivity.this, MainActivity.class));
								break;
							default:
								LOGGER.info("Unhandled static button: {}", item);
								break;
						}
						return;
					}
					LOGGER.warn("Unhandled drawer click: {}", item);
				}
			});

		}

		actionBarDrawerToggle = new ActionBarDrawerToggle(BandDetailsActivity.this, drawerLayout, R.string.dashboard_fragment_drawer_open, R.string
				.dashboard_fragment_drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				BandDetailsActivity.this.invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				BandDetailsActivity.this.invalidateOptionsMenu();
			}
		};

		actionBarDrawerToggle.setDrawerIndicatorEnabled(true);


		drawerLayout.setDrawerListener(actionBarDrawerToggle);

		ActionBar actionBar = this.getActionBar();
		if (actionBar != null) {
			if(Build.VERSION.SDK_INT == 16 || Build.VERSION.SDK_INT == 17) {
				LayoutInflater inflator = (LayoutInflater) this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View v = inflator.inflate(R.layout.actionbar_custom, null);
				actionBar.setDisplayHomeAsUpEnabled(false);
				actionBar.setDisplayShowHomeEnabled(false);
				actionBar.setDisplayShowCustomEnabled(true);
				actionBar.setDisplayShowTitleEnabled(false);
				actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
				actionBar.setCustomView(v);
				this.findViewById(R.id.action_bar_custom_button).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (drawerLayout.isDrawerOpen(GravityCompat.START))
							closeDrawer();
						else
							openDrawer();
					}
				});
			}
			else{
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setHomeButtonEnabled(true);
			}
		} else {
			LOGGER.error("Failed to get acti bar for activity from DashboardFragment!");
		}

		syncDrawerState();
		setDrawerList();
	}

	private void openDrawer(){
		drawerLayout.openDrawer(GravityCompat.START);
	}

	private void closeDrawer(){
		drawerLayout.closeDrawer(GravityCompat.START);
	}

	public void syncDrawerState() {
		actionBarDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(App.getInstance().getCurrentUser().isUser()) {
			return actionBarDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
		}else{
			onBackPressed();
			return false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.band_details, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/*@Override
	public boolean onNavigateUp() {
		// "Up" creates a new instance of BandDetailsActivity, prevent this by handling "Up" the same way as "Back".
		onBackPressed();
		return false;
	}*/

	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(IMEI_KEY, imei);
		outState.putString(DISPLAY_NAME_KEY, displayName);
	}

	@Override
	public void onResume(){
		super.onResume();
		if(App.getInstance().getCurrentUser().isUser()) {
			synchronized (this) {
				new ListBandsTask().execute();
			}
		}
        if(getIntent().getBooleanExtra(DISPLAY_ALARM_DIALOG, false)){
			showAlarmAlertDialog();
        }
	}

	@Override
	public void onPause() {
		super.onPause();
		synchronized (this) {
			if (listBandsTask != null) {
				listBandsTask.cancel(true);
			}
		}
	}

	private void initDetailsForBand(long imei, String initialDisplayName, int hardwareLocation, boolean isBasic){
		this.imei = imei;
		this.displayName = initialDisplayName;
		this.hardwareLocation = hardwareLocation;
		this.isBasic = isBasic;
	}

	public long getImei() {
		return imei;
	}

	public long getQueryStartTime() {
		final int selectedTimeRange = PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKeys.PREF_BAND_DETAILS_TIME_RANGE, 0);
		switch (selectedTimeRange) {
			case 0:
				return DateTime.now().minusDays(1).toInstant().getMillis();
			case 1:
				return DateTime.now().minusWeeks(1).toInstant().getMillis();
			case 2:
				return DateTime.now().minusMonths(1).toInstant().getMillis();
			case 3:
				return DateTime.now().minusYears(1).toInstant().getMillis();
			case 4:
				return new DateTime(0).toInstant().getMillis();
			default:
				return DateTime.now().minusDays(1).toInstant().getMillis();
		}
	}

	public long getQueryEndTime() {
		return System.currentTimeMillis();
	}

	public void onTimeRangeAction(MenuItem item) {
		callback.onTimeRangeAction();
		/*try {//TODO
            String message = "{\"lastName\":\"Stanuszek\",\"wearError\":false,\"displayName\":\"Kamil Stanuszek\",\"sound\":0,\"notificationType\":1,\"pulseExceeded\":true,\"firstName\":\"Kamil\",\"fall\":false,\"zone\":false,\"sos\":true,\"temperature\":32.16,\"pulse\":66,\"imei\":\"865374030476504\",\"isBasic\":false}";
			new AlarmNotification(AlarmNotification.parse(message)).show();
		} catch (JsonException e) {
			LOGGER.warn("Failed to parse new measurement notification", e);
		}
		try {//TODO
			String message = "{\"lastName\":\"Stanuszek\",\"wearError\":false,\"displayName\":\"Kamil Stanuszek\",\"sound\":0,\"notificationType\":1,\"pulseExceeded\":true,\"firstName\":\"Kamil\",\"fall\":false,\"zone\":false,\"sos\":false,\"temperature\":32.16,\"pulse\":66,\"imei\":\"865374030476504\",\"isBasic\":true}";
			new AlarmNotification(AlarmNotification.parse(message)).show();
		} catch (JsonException e) {
			LOGGER.warn("Failed to parse new measurement notification", e);
		}*/
	}

	@SuppressWarnings("unused")
	public void onSettingsAction(MenuItem item) {
		Intent intent = new Intent(this, BandSettingsActivity.class);
		intent.putExtra(IMEI_KEY, imei);
		intent.putExtra(DISPLAY_NAME_KEY, displayName);
		startActivityForResult(intent, 0xf002);
	}

	public void onTimeRangeChanged() {
		callback.onTimeRangeChanged();
	}

	public void switchActiveTab(int tabNumber) {
		callback.switchActiveTab( tabNumber);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0xf002) {
			switch (resultCode) {
				case BandSettingsActivity.RESULT_DELETED:
					// Band has need deleted from account, signal this to main fragment, so a full reload happens to update data
					setResult(BandSettingsActivity.RESULT_DELETED);
					finish();
					return;
				case RESULT_OK:
					// BandSettingsActivity says the band was saved, signal this to main fragment, so a full reload happens to update data
					setResult(RESULT_OK);
					// Update our title
					if (data != null) {
						setTitle(data.getAction());
					}
					return;
				default:
					LOGGER.warn("Unhandled resultCode={}", Integer.toHexString(resultCode));
					return;
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private class  ListBandsTask extends ApiResponseTask {
		@Override
		protected Void doInBackground(Void... params) {
			associatedBands.clear();
			if(BandDetailsActivity.this != null) {
				BandDetailsActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//updateFragmentState(true);
					}
				});
			}
			String json;
			App app = App.getInstance();
			final ListBandsResponse listBandsResponse;
			try {
				json = new HttpClient(ApiEndPoint.LIST_BANDS).doPost(new ListBandsRequest(app.getCurrentUser().getUserId(), app
						.getCurrentUser().getAuthenticationToken()));
				LOGGER.debug("JSON: {}", json.toString());
				listBandsResponse = ListBandsResponse.fromString(json, ListBandsResponse.class);
			} catch (IOException e) {

				LOGGER.error("Failed to get a list of bands, server issue", e);
				if(BandDetailsActivity.this != null) {
					BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							//updateFragmentState(false);
							Toast.makeText(BandDetailsActivity.this, R.string.dashboard_fragment_refresh_failed, Toast.LENGTH_SHORT).show();
						}
					});
				}

				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				if(BandDetailsActivity.this != null) {
					BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							//updateFragmentState(false);
							Toast.makeText(BandDetailsActivity.this, R.string.dashboard_fragment_refresh_failed, Toast.LENGTH_SHORT).show();
						}
					});
				}
				return null;
			}

			if (listBandsResponse.getAssociatedBands() == null) {
				// Invalid token
				if(BandDetailsActivity.this != null) {
					BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(BandDetailsActivity.this, R.string.dashboard_fragment_not_authenticated, Toast.LENGTH_SHORT).show();
						}
					});
				}
				App.getInstance().setCurrentUser(null);
				getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id
						.main_activity_frame, new LoginFragment()).commit();
				return null;
			}

			if (isCancelled()) {
				return null;
			}

			LOGGER.info("Server returned bands: {}", listBandsResponse.getAssociatedBands().size());
			associatedBands.addAll(listBandsResponse.getAssociatedBands());
			initDetailsForBand(associatedBands.get(0).getImei(),
					displayName, associatedBands.get(0).getBandHardwareLocation(), associatedBands.get(0).isBasic());

			App.getInstance().getAssociatedBandsCache().put(CACHE_KEY, associatedBands);
			BandDetailsActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					//updateFragmentState(false);
					onTimeRangeChanged();
				}
			});
			return null;
		}

	}

	private void setDrawerList() {
		final List<Map<String, Object>> menuContent = new ArrayList<>();
		Map<String, Object> menuEntry;

		menuEntry = new HashMap<>();
		menuEntry.put("type", "label");
		menuEntry.put("text", R.string.dashboard_fragment_drawer_menu);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 2);
		menuEntry.put("icon", R.drawable.ic_settings_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_settings);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 3);
		menuEntry.put("icon", R.drawable.ic_help_outline_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_helpdesk);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 4);
		menuEntry.put("icon", R.drawable.ic_power_settings_new_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_logout);
		menuContent.add(menuEntry);

		menuEntry = new HashMap<>();
		menuEntry.put("type", "static");
		menuEntry.put("key", 1);
		menuEntry.put("icon", R.drawable.ic_refresh_black_36dp);
		menuEntry.put("text", R.string.dashboard_fragment_drawer_refresh);
		menuContent.add(menuEntry);

		drawerList.setAdapter(new ArrayAdapter<Map<String, Object>>(this, R.layout.list_entry_dashboard_drawer, R.id
				.list_entry_dashboard_drawer_label, menuContent) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				Map<String, Object> item = getItem(position);
				if ("label".equals(item.get("type"))) {
					View view = View.inflate(BandDetailsActivity.this, R.layout.list_entry_dashboard_drawer_label, null);
					((TextView)view.findViewById(R.id.list_entry_dashboard_drawer_label_label)).setText((Integer)item.get("text"));
					return view;
				} else if ("static".equals(item.get("type"))) {
					View view = View.inflate(BandDetailsActivity.this, R.layout.list_entry_dashboard_drawer, null);
					view.findViewById(R.id.list_entry_dashboard_drawer_icon).setBackgroundResource((Integer)item.get("icon"));
					((TextView)view.findViewById(R.id.list_entry_dashboard_drawer_label)).setText((Integer)item.get("text"));
					return view;
				}
				throw new RuntimeException("Type " + item.get("type") + " not supported");
			}


			@Override
			public boolean isEnabled(int position) {
				return !"label".equals(getItem(position).get("type"));
			}
		});
	}

	@Override
	public void onBackPressed() {
		if(App.getInstance().getCurrentUser().isUser()) {
			if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
				drawerLayout.closeDrawer(GravityCompat.START);
			} else {
				showCloseAlertDialog();
			}
		}else{
			super.onBackPressed();
		}
	}

	private void showAlarmAlertDialog(){
        new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setTitle(R.string.dashboard_fragment_alarm_dialog_message)
                .setPositiveButton(R.string.dashboard_fragment_alarm_dialog_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
						new AcceptedAlarmTask(BandDetailsActivity.this.getImei()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
						getIntent().putExtra(DISPLAY_ALARM_DIALOG, false);
                    }
                })
                .setNegativeButton(R.string.dashboard_fragment_alarm_dialog_no, null)
                .create()
                .show();
    }

	private void showCloseAlertDialog() {
		new AlertDialog.Builder(this, R.style.AlertDialogTheme)
				.setTitle(R.string.dashboard_fragment_close_message)
				.setPositiveButton(R.string.dashboard_fragment_close_yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finishAffinity();
					}
				})
				.setNegativeButton(R.string.dashboard_fragment_close_no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						getIntent().putExtra(DISPLAY_ALARM_DIALOG, false);
					}
				})
				.create()
				.show();
	}

	public void setCallback(Callback callback){
	    this.callback = callback;
    }

	public interface Callback{
        void onTimeRangeAction();
        void onTimeRangeChanged();
        void switchActiveTab(int tabNumber);
		void setCurrentPageViewItem();
    }

    private void loadingFinished(){
		Toast.makeText(BandDetailsActivity.this, R.string.dashboard_fragment_toast_alarm_accepted, Toast.LENGTH_LONG).show();
	}

	private void loadingFailed(){
		Toast.makeText(BandDetailsActivity.this, R.string.dashboard_fragment_toast_alarm_not_accepted, Toast.LENGTH_LONG).show();
	}

	private class AcceptedAlarmTask extends ApiResponseTask {
		private final long imei;

		public AcceptedAlarmTask(long imei) {
			super();
			this.imei = imei;
		}

		@Override
		protected Void doInBackground(Void... params) {
			CurrentUser user = App.getInstance().getCurrentUser();

			try {
				String json = new HttpClient(ApiEndPoint.SET_ACCEPTED_ALARM).doPost(new SetAcceptAlarmRequest(user.getAuthenticationToken(), imei));
				if(isApiResponseCorrect(json)) {
					BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFinished();
						}
					});
				}else{
					loadingFailed();
				}
			} catch (IOException e) {
				LOGGER.error("Failed to get data from server, server issue", e);
				BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
			} catch (NullPointerException e){
				LOGGER.error("Failed to update user credentials", e);
				BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
			} /*catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				BandDetailsActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							loadingFailed();
						}
					});
			}*/
			return null;
		}
	}
}
