package com.silvermedia.sidly.android.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.silvermedia.sidly.android.R;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MyLocationOverlay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapActivity extends FragmentActivity {
	private static final Logger LOGGER = LoggerFactory.getLogger(MapActivity.class);

	private LatLng location;
	private int hardwareLocation = 0;

	private String title;

	private MapView mapView;
	private MapController mapController;

	MyItemizedOverlay myItemizedOverlay = null;
	MyLocationOverlay myLocationOverlay = null;
	MyRadiusOverlay myRadiusOverlay = null;

	private boolean mHasPermissions = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// ask for permissions - Android 6.0+
		if (Build.VERSION.SDK_INT >= 23) {
			checkPermissions();
		}

		Intent intent = getIntent();
		if (!intent.hasExtra("latitude") || !intent.hasExtra("longitude")) {
			throw new RuntimeException("Missing required data in Intent");
		}
		location = new LatLng(intent.getDoubleExtra("latitude", 0.0), intent.getDoubleExtra("longitude", 0.0));
		hardwareLocation = intent.getExtras().getInt("hardwareLocation");
		setContentView(R.layout.osm_map_activity);
		MapView map = (MapView) findViewById(R.id.map);
		map.setTileSource(TileSourceFactory.MAPNIK);

		map.setBuiltInZoomControls(true);
		map.setMultiTouchControls(true);

		Drawable myMarker = this.getResources().getDrawable(R.drawable.marker_default);
		Drawable marker = getResources().getDrawable(android.R.drawable.ic_menu_compass);
		int markerWidth = marker.getIntrinsicWidth();
		int markerHeight = marker.getIntrinsicHeight();
		myMarker.setBounds(0, markerHeight, markerWidth, 0);
		ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());

		IMapController mapController = map.getController();
		mapController.setZoom(14);

		GeoPoint startPoint = new GeoPoint(location.latitude, location.longitude);
		mapController.setCenter(startPoint);

		myLocationOverlay = new MyLocationOverlay(this, map);
		map.getOverlays().add(myLocationOverlay);
		myLocationOverlay.enableMyLocation();

		if(hardwareLocation != 1){
			myRadiusOverlay = new MyRadiusOverlay(this, location.longitude, location.latitude);
			map.getOverlays().add(myRadiusOverlay);
		}

		myItemizedOverlay = new MyItemizedOverlay(myMarker, resourceProxy);
		map.getOverlays().add(myItemizedOverlay);
		myItemizedOverlay.addItem(startPoint, "startPoint", "startPoint");
	}

	private void drawLocationCircle(){

	}

    @Override
    public void onBackPressed() {
        this.mapController = null;
        this.mapView = null;
        this.finish();
    }

	// START PERMISSION CHECK
	final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

	private void checkPermissions() {
		mHasPermissions = true;
		List<String> permissions = new ArrayList<>();
//		String message = "osmdroid permissions:";
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			mHasPermissions = false;
			permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
//			message += "\nLocation to show user location.";
		}
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			mHasPermissions = false;
			permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//			message += "\nStorage access to store map tiles.";
		}
		if (!permissions.isEmpty()) {
			mHasPermissions = false;
//			Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
			String[] params = permissions.toArray(new String[permissions.size()]);

			requestPermissions(params, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
		} // else: We already have permissions, so handle as normal
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:	{
				Map<String, Integer> perms = new HashMap<>();
				// Initial
				perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
				perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
				// Fill with results
				for (int i = 0; i < permissions.length; i++)
					perms.put(permissions[i], grantResults[i]);
				// Check for ACCESS_FINE_LOCATION and WRITE_EXTERNAL_STORAGE
				Boolean location = perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
				Boolean storage = perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
				if (location && storage) {
					// All Permissions Granted
//					Toast.makeText(getActivity(), "All permissions granted", Toast.LENGTH_SHORT).show();
					mHasPermissions = true;
//						root.findViewById(R.id.band_details_overview_fragment_last_fall_location).performClick();
//					} else {
//						root.findViewById(R.id.band_details_overview_fragment_last_sos_location).performClick();
//					}
				} else if (location) {
					mHasPermissions = false;
//					Toast.makeText(getActivity(), "Storage permission is required to store map tiles to reduce data usage and for offline usage.", Toast.LENGTH_LONG).show();
				} else if (storage) {
					mHasPermissions = false;
//					Toast.makeText(getActivity(),"Location permission is required to show the user's location on map.", Toast.LENGTH_LONG).show();
				} else { // !location && !storage case
					// Permission Denied
					Toast.makeText(this, getString(R.string.map_activity_no_permissions), Toast.LENGTH_LONG).show();
					mHasPermissions = false;

				}
			}
			break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
}
