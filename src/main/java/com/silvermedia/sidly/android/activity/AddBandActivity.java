package com.silvermedia.sidly.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.StringRes;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.ContactsUtil;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseAddBandTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.AssociateBandRequest;
import com.silvermedia.sidly.android.cdm.AssociateBandResponse;
import com.silvermedia.sidly.android.cdm.JsonException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddBandActivity extends BaseActivity implements View.OnClickListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(AddBandActivity.class);

	private static final int CONTACT_PICKER_REQUEST_ID_BASE = 0xe000;

	private ContactsUtil contactsUtil;

	private AddBandTask addBandTask;

	private EditText addBandActivityDisplayName;

	private EditText addBandActivityFirstName;

	private EditText addBandActivityLastName;

	private EditText addBandActivityImei;

	private EditText addBandActivityAge;

	private EditText addBandActivityDailySteps;

	private Button addBandActivityIcePhone1;

	private Button addBandActivityIcePhone2;

	private Button addBandActivityIcePhone3;

	private Button addBandActivityAdd;

	private RelativeLayout addBandActivitySpinner;

	private Context context;

	private EditText editTextAlert;

	private String result;

	private int requestCode;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_band_activity);

		contactsUtil = new ContactsUtil(this);
		this.context = this;

		ContactsUtil.checkPermissions(this);

		addBandActivityAdd = (Button)findViewById(R.id.add_band_activity_add_button);
		addBandActivityAdd.setOnClickListener(this);

		addBandActivityDisplayName = (EditText)findViewById(R.id.add_band_activity_display_name);
		addBandActivityFirstName = (EditText)findViewById(R.id.add_band_activity_first_name);
		addBandActivityLastName = (EditText)findViewById(R.id.add_band_activity_last_name);
		addBandActivityImei = (EditText)findViewById(R.id.add_band_activity_imei);
		addBandActivityAge = (EditText)findViewById(R.id.add_band_activity_age);
		addBandActivityDailySteps = (EditText)findViewById(R.id.add_band_activity_daily_steps);
		addBandActivityIcePhone1 = (Button)findViewById(R.id.add_band_activity_ice_phone_1);
		addBandActivityIcePhone2 = (Button)findViewById(R.id.add_band_activity_ice_phone_2);
		addBandActivityIcePhone3 = (Button)findViewById(R.id.add_band_activity_ice_phone_3);

		addBandActivitySpinner = (RelativeLayout)findViewById(R.id.add_band_activity_spinner);

		addBandActivityIcePhone1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ContactsUtil.checkPermissions(AddBandActivity.this)){
					if(contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 1);
					else
						showAlertDialogNumber(1);
				}
			}
		});
		addBandActivityIcePhone1.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				addBandActivityIcePhone1.setTag(null);
				addBandActivityIcePhone1.setText(R.string.add_band_activity_ice_phone);
				return true;
			}
		});
		addBandActivityIcePhone1.setTag(null);
		addBandActivityIcePhone2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ContactsUtil.checkPermissions(AddBandActivity.this)){
					if(contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 2);
					else
						showAlertDialogNumber(2);
				}
			}
		});
		addBandActivityIcePhone2.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				addBandActivityIcePhone2.setTag(null);
				addBandActivityIcePhone2.setText(R.string.add_band_activity_ice_phone_optional);
				return true;
			}
		});
		addBandActivityIcePhone2.setTag(null);
		addBandActivityIcePhone3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ContactsUtil.checkPermissions(AddBandActivity.this)){
					if(contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 3);
					else
						showAlertDialogNumber(3);
				}
			}
		});

		addBandActivityIcePhone3.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				addBandActivityIcePhone3.setTag(null);
				addBandActivityIcePhone3.setText(R.string.add_band_activity_ice_phone_optional);
				return true;
			}
		});
		addBandActivityIcePhone3.setTag(null);
	}

	@Override
	protected void onResume() {
		super.onResume();
		addBandTask = null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (addBandTask != null) {
			addBandTask.cancel(true);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case ContactsUtil.MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted
				} else {
					String permission = permissions[0];
					if(grantResults[0] == PackageManager.PERMISSION_DENIED) {
						if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
							boolean showRationale = shouldShowRequestPermissionRationale(permission);
							if(!showRationale){
								Toast.makeText(this, getResources().getString(R.string.band_settings_activity_permissions_required), Toast.LENGTH_SHORT).show();
							}
						}
					}
				}
				return;
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode <= CONTACT_PICKER_REQUEST_ID_BASE || requestCode > CONTACT_PICKER_REQUEST_ID_BASE + 3) {
			super.onActivityResult(requestCode, resultCode, data);
			return;
		}

		if (resultCode != RESULT_OK) {
			return;
		}

		Cursor cursor = null;
		try {
			cursor = getContentResolver().query(data.getData(), null, null, null, null);
			if (cursor == null) {
				LOGGER.warn("null cursor from content resolver for {}, ignored", data.getData());
				return;
			}
			cursor.moveToFirst();
			String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			if (phoneNumber != null) {
				phoneNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
				switch (requestCode) {
					case CONTACT_PICKER_REQUEST_ID_BASE + 1:
						addBandActivityIcePhone1.setTag(phoneNumber);
						addBandActivityIcePhone1.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					case CONTACT_PICKER_REQUEST_ID_BASE + 2:
						addBandActivityIcePhone2.setTag(phoneNumber);
						addBandActivityIcePhone2.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					case CONTACT_PICKER_REQUEST_ID_BASE + 3:
						addBandActivityIcePhone3.setTag(phoneNumber);
						addBandActivityIcePhone3.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					default:
						LOGGER.warn("Resolved contact to number for unknown ID {}", requestCode);
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	private void disableAll() {
		addBandActivityDisplayName.setEnabled(false);
		addBandActivityFirstName.setEnabled(false);
		addBandActivityLastName.setEnabled(false);
		addBandActivityImei.setEnabled(false);
		addBandActivityAge.setEnabled(false);
		addBandActivityAdd.setEnabled(false);
		addBandActivityDailySteps.setEnabled(false);
		addBandActivityIcePhone1.setEnabled(false);
		addBandActivityIcePhone2.setEnabled(false);
		addBandActivityIcePhone3.setEnabled(false);

		addBandActivitySpinner.setVisibility(View.VISIBLE);
	}

	private void enableAll() {
		addBandActivityDisplayName.setEnabled(true);
		addBandActivityFirstName.setEnabled(true);
		addBandActivityLastName.setEnabled(true);
		addBandActivityImei.setEnabled(true);
		addBandActivityAge.setEnabled(true);
		addBandActivityAdd.setEnabled(true);
		addBandActivityDailySteps.setEnabled(true);
		addBandActivityIcePhone1.setEnabled(true);
		addBandActivityIcePhone2.setEnabled(true);
		addBandActivityIcePhone3.setEnabled(true);

		addBandActivitySpinner.setVisibility(View.GONE);
	}

	private void showToastAndHideSpinner(@StringRes int stringResourceId) {
		Toast.makeText(this, stringResourceId, Toast.LENGTH_SHORT).show();
		enableAll();
	}

	@Override
	public void onClick(View ignored) {
		disableAll();

		String displayName = ((EditText)findViewById(R.id.add_band_activity_display_name)).getText().toString().trim();
		if (displayName.isEmpty()) {
			showToastAndHideSpinner(R.string.add_band_activity_display_name_missing);
			return;
		}

		String firstName = ((EditText)findViewById(R.id.add_band_activity_first_name)).getText().toString().trim();
		if (firstName.isEmpty()) {
			showToastAndHideSpinner(R.string.add_band_activity_first_name_missing);
			return;
		}

		String lastName = ((EditText)findViewById(R.id.add_band_activity_last_name)).getText().toString().trim();
		if (lastName.isEmpty()) {
			showToastAndHideSpinner(R.string.add_band_activity_last_name_missing);
			return;
		}

		String imeiText = ((EditText)findViewById(R.id.add_band_activity_imei)).getText().toString().trim();
		if (imeiText.isEmpty()) {
			showToastAndHideSpinner(R.string.add_band_activity_imei_missing);
			return;
		}
		if (imeiText.length() != 15) {
			showToastAndHideSpinner(R.string.add_band_activity_imei_invalid);
			return;
		}
		long imei;
		try {
			imei = Long.valueOf(imeiText);
		} catch (NumberFormatException e) {
			showToastAndHideSpinner(R.string.add_band_activity_imei_invalid);
			return;
		}

		int age;
		try {
			age = Integer.valueOf(((EditText)findViewById(R.id.add_band_activity_age)).getText().toString());
			if (age < 1 || age > 99) {
				showToastAndHideSpinner(R.string.add_band_activity_age_missing);
				return;
			}
		} catch (NumberFormatException e) {
			showToastAndHideSpinner(R.string.add_band_activity_age_missing);
			return;
		}

		int dailySteps;
		try {
			dailySteps = Integer.valueOf(((EditText)findViewById(R.id.add_band_activity_daily_steps)).getText().toString());
		} catch (NumberFormatException e) {
			showToastAndHideSpinner(R.string.add_band_activity_daily_steps_missing);
			return;
		}

		List<String> icePhoneNumbers = new ArrayList<>();
		if (addBandActivityIcePhone1.getTag() != null) {
			icePhoneNumbers.add((String)addBandActivityIcePhone1.getTag());
		}
		if (addBandActivityIcePhone2.getTag() != null) {
			icePhoneNumbers.add((String)addBandActivityIcePhone2.getTag());
		}
		if (addBandActivityIcePhone3.getTag() != null) {
			icePhoneNumbers.add((String)addBandActivityIcePhone3.getTag());
		}

		if (icePhoneNumbers.isEmpty()) {
			showToastAndHideSpinner(R.string.add_band_activity_ice_phone_required);
			return;
		}

		// Looks good!
		App app = (App)getApplication();
		AssociateBandRequest associateBandRequest = new AssociateBandRequest(app.getCurrentUser().getUserId(), app.getCurrentUser()
			.getAuthenticationToken(), imei, displayName, firstName, lastName, age, dailySteps, icePhoneNumbers);
		addBandTask = new AddBandTask();
		addBandTask.execute(associateBandRequest);
	}

	private void loadingFinishedError(){
//		App.getInstance().setCurrentUser(null);
//		Intent intent = new Intent(this, MainActivity.class);
//		startActivity(intent);

		App.getInstance().setCurrentUser(null);
		App.getInstance().clearAssociatedBandsCache();
		startActivity(new Intent(this, MainActivity.class));
	}


	private class AddBandTask extends ApiResponseAddBandTask {
		@Override
		protected Void doInBackground(AssociateBandRequest... params) {
			final AssociateBandResponse associateBandResponse;
			try {
				String json = new HttpClient(ApiEndPoint.ASSOCIATE_BAND).doPost(params[0]);
				associateBandResponse = AssociateBandResponse.fromString(json, AssociateBandResponse.class);
				if(!isApiResponseCorrect(json)){
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(AddBandActivity.this, R.string.login_fragment_multilogin, Toast.LENGTH_SHORT).show();
						}
					});
					loadingFinishedError();
					return null;
				}
			} catch (IOException e) {
				LOGGER.error("Failed to associate band, server issue", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AddBandActivity.this.showToastAndHideSpinner(R.string.add_band_activity_error);
					}
				});
				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AddBandActivity.this.showToastAndHideSpinner(R.string.add_band_activity_error);
					}
				});
				return null;
			}

			if (!associateBandResponse.isBandAssociated()) {
				LOGGER.error("Server refused to associate band");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AddBandActivity.this.showToastAndHideSpinner(R.string.add_band_activity_failed);
					}
				});
				return null;
			}

			setResult(Activity.RESULT_OK);
			if (isCancelled()) {
				setResult(RESULT_CANCELED);
			}
			finish();
			return null;
		}
	}

	private void showAlertDialogNumber(int code){
		FrameLayout layout = new FrameLayout(context);
		FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(40,20,40,20);
		editTextAlert = new EditText(context);
		editTextAlert.setHint(R.string.add_band_activity_ice_phone_prompt);
		editTextAlert.setInputType(InputType.TYPE_CLASS_NUMBER);
		editTextAlert.setPadding(15, 15, 15, 15);
		editTextAlert.setLayoutParams(params);
		layout.addView(editTextAlert);
		this.requestCode = code;
		new AlertDialog.Builder(this)
				.setTitle(R.string.add_band_activity_ice_phone_prompt_title)
				.setView(layout)
				.setPositiveButton(R.string.add_band_activity_ice_phone_prompt_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						result = editTextAlert.getText().toString();
						result = result.replaceAll("[^0-9\\+]", "");
						addNumberIndependently(requestCode, result);
					}
				})
				.setNegativeButton(R.string.add_band_activity_ice_phone_prompt_cancel, null)
				.create()
				.show();
	}

	private void addNumberIndependently(int requestCode, String phoneNumber){
		switch (requestCode) {
			case 1:
				addBandActivityIcePhone1.setTag(phoneNumber);
				addBandActivityIcePhone1.setText(phoneNumber);
				break;
			case 2:
				addBandActivityIcePhone2.setTag(phoneNumber);
				addBandActivityIcePhone2.setText(phoneNumber);
				break;
			case 3:
				addBandActivityIcePhone3.setTag(phoneNumber);
				addBandActivityIcePhone3.setText(phoneNumber);
				break;
			default:
				LOGGER.warn("Resolved contact to number for unknown ID {}", requestCode);
		}
	}
}
