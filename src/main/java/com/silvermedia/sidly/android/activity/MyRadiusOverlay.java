package com.silvermedia.sidly.android.activity;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;


import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Overlay;

/**
 * Created by Kacper on 12.09.2016.
 */
public class MyRadiusOverlay extends Overlay {

    private Context ctx;
    private double longitude;
    private double latitude;
    private float radius = 1000f;

    public MyRadiusOverlay(Context ctx, double longitude, double latitude) {
        super(ctx);
        this.ctx = ctx;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    protected void draw(Canvas canvas, MapView mapView, boolean shadow) {

        Point point = new Point();
        GeoPoint geoPoint = new GeoPoint((int) (latitude *1e6), (int)(longitude * 1e6));

        Projection projection = mapView.getProjection();
        projection.toPixels(geoPoint, point);
        float projectedRadius = projection.metersToEquatorPixels(radius);

        Paint paint = new Paint();
        paint.setARGB(20, 0, 0, 255);

        Paint paint2 = new Paint();
        paint2.setARGB(80, 0, 0, 153);
        paint2.setStrokeWidth(4);
        paint2.setStrokeCap(Paint.Cap.ROUND);
        paint2.setAntiAlias(true);
        paint2.setDither(false);
        paint2.setStyle(Paint.Style.STROKE);

        canvas.drawCircle((float)point.x, (float)point.y, projectedRadius, paint2);
        canvas.drawCircle((float)point.x, (float)point.y, projectedRadius, paint);
    }
}