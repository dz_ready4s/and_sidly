package com.silvermedia.sidly.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Bundle;
import android.widget.TextView;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.R;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView versionNumber = (TextView) findViewById(R.id.splash_screen_version);
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = packageInfo.versionName;

        versionNumber.setText(version);
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //CheckForSessionId
                if(App.getInstance().getCurrentUser() != null){
                    if (App.getInstance().getCurrentUser().isUser()){
                        Intent intent = new Intent(SplashActivity.this, BandDetailsActivity.class);
                        startActivity(intent);
                    }
                }
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);

            }
        }, 2000);
    }

}
