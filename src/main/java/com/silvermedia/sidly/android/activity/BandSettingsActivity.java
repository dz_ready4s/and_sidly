package com.silvermedia.sidly.android.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.ColorUtil;
import com.silvermedia.sidly.android.ContactsUtil;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.activity.base.BaseActivity;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.ApiResponseTask;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.DisassociateBandRequest;
import com.silvermedia.sidly.android.cdm.DisassociateBandResponse;
import com.silvermedia.sidly.android.cdm.GetBandPropertiesRequest;
import com.silvermedia.sidly.android.cdm.GetBandPropertiesResponse;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.SetBandPropertiesRequest;
import com.silvermedia.sidly.android.cdm.SetBandPropertiesResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BandSettingsActivity extends BaseActivity {
	public static final int RESULT_DELETED = 3;

	private static final int CONTACT_PICKER_REQUEST_ID_BASE = 0xe000;

	private static final Logger LOGGER = LoggerFactory.getLogger(BandSettingsActivity.class);

	private ContactsUtil contactsUtil;

	private long imei;

	private EditText bandSettingsActivityDisplayName;

	private EditText bandSettingsActivityFirstName;

	private EditText bandSettingsActivityLastName;

	private EditText bandSettingsActivityImei;

	private EditText bandSettingsActivityAge;

	private EditText bandSettingsActivityDailySteps;

	private Button bandSettingsActivityRemoveButton;

	private Button bandSettingsActivitySaveButton;

	private SeekBar bandSettingsActivityMeasurementInterval;

	private TextView bandSettingsActivityMeasurementIntervalText;

	private Button bandSettingsActivityIcePhone1;

	private Button bandSettingsActivityIcePhone2;

	private Button bandSettingsActivityIcePhone3;

	private RelativeLayout bandSettingsActivitySpinner;

	private Context context;

	private EditText editTextAlert;

	private String result;

	private int requestCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.band_settings_activity);

		contactsUtil = new ContactsUtil(this);
		this.context = this;


		ContactsUtil.checkPermissions(this);

		imei = getIntent().getLongExtra(BandDetailsActivity.IMEI_KEY, -1);
		if (imei == -1) {
			LOGGER.error("Failed to get IMEI from Intent");
			finish();
		}
		String displayName = getIntent().getStringExtra(BandDetailsActivity.DISPLAY_NAME_KEY);
		if (displayName != null) {
			setTitle(displayName);
		}

		bandSettingsActivityDisplayName = (EditText)findViewById(R.id.band_settings_activity_display_name);
		bandSettingsActivityFirstName = (EditText)findViewById(R.id.band_settings_activity_first_name);
		bandSettingsActivityLastName = (EditText)findViewById(R.id.band_settings_activity_last_name);
		bandSettingsActivityImei = (EditText)findViewById(R.id.band_settings_activity_imei);
		bandSettingsActivityAge = (EditText)findViewById(R.id.band_settings_activity_age);
		bandSettingsActivityDailySteps = (EditText)findViewById(R.id.band_settings_activity_daily_steps);
		bandSettingsActivityRemoveButton = (Button)findViewById(R.id.band_settings_activity_remove_button);
		bandSettingsActivitySaveButton = (Button)findViewById(R.id.band_settings_activity_save_button);
		bandSettingsActivityMeasurementInterval = (SeekBar)findViewById(R.id.band_settings_activity_measurement_interval);
		bandSettingsActivityMeasurementIntervalText = (TextView)findViewById(R.id.band_settings_activity_measurement_interval_text);
		bandSettingsActivityIcePhone1 = (Button)findViewById(R.id.band_settings_activity_ice_phone_1);
		bandSettingsActivityIcePhone2 = (Button)findViewById(R.id.band_settings_activity_ice_phone_2);
		bandSettingsActivityIcePhone3 = (Button)findViewById(R.id.band_settings_activity_ice_phone_3);
		bandSettingsActivitySpinner = (RelativeLayout)findViewById(R.id.band_settings_activity_spinner);

		bandSettingsActivityMeasurementInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				updateMeasurementIntervalDisplay();
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
		});
		bandSettingsActivityIcePhone1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ContactsUtil.checkPermissions(BandSettingsActivity.this)) {
					if (contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 1);
					else
						showAlertDialogNumber(1);
				}
			}
		});
		bandSettingsActivityIcePhone1.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				bandSettingsActivityIcePhone1.setTag(null);
				bandSettingsActivityIcePhone1.setText(R.string.add_band_activity_ice_phone);
				return true;
			}
		});
		bandSettingsActivityIcePhone2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ContactsUtil.checkPermissions(BandSettingsActivity.this)) {
					if (contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 2);
					else
						showAlertDialogNumber(2);
				}
			}
		});
		bandSettingsActivityIcePhone2.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				bandSettingsActivityIcePhone2.setTag(null);
				bandSettingsActivityIcePhone2.setText(R.string.band_settings_activity_ice_phone);
				return true;
			}
		});
		bandSettingsActivityIcePhone3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ContactsUtil.checkPermissions(BandSettingsActivity.this)) {
					if (contactsUtil.hasPhoneCapabilities(context))
						startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_PICKER_REQUEST_ID_BASE + 3);
					else
						showAlertDialogNumber(3);
				}
			}
		});
		bandSettingsActivityIcePhone3.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				bandSettingsActivityIcePhone3.setTag(null);
				bandSettingsActivityIcePhone3.setText(R.string.band_settings_activity_ice_phone);
				return true;
			}
		});

		updateMeasurementIntervalDisplay();
		disableAll();

		new LoadTask().execute();
	}

	@Override
	public boolean onNavigateUp() {
		// "Up" creates a new instance of BandDetailsActivity, prevent this by handling "Up" the same way as "Back".
		onBackPressed();
		return false;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case ContactsUtil.MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted
				} else {
					String permission = permissions[0];
					if(grantResults[0] == PackageManager.PERMISSION_DENIED) {
						if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
							boolean showRationale = shouldShowRequestPermissionRationale(permission);
							if(!showRationale){
								Toast.makeText(this, getResources().getString(R.string.band_settings_activity_permissions_required), Toast.LENGTH_SHORT).show();
							}
						}
					}
				}
				return;
			}
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode <= CONTACT_PICKER_REQUEST_ID_BASE || requestCode > CONTACT_PICKER_REQUEST_ID_BASE + 3) {
			super.onActivityResult(requestCode, resultCode, data);
			return;
		}

		if (resultCode != RESULT_OK) {
			return;
		}

		Cursor cursor = null;
		try {
			cursor = getContentResolver().query(data.getData(), null, null, null, null);
			if (cursor == null) {
				LOGGER.warn("null cursor from content resolver for {}, ignored", data.getData());
				return;
			}
			cursor.moveToFirst();
			String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			if (phoneNumber != null) {
				phoneNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
				switch (requestCode) {
					case CONTACT_PICKER_REQUEST_ID_BASE + 1:
						bandSettingsActivityIcePhone1.setTag(phoneNumber);
						bandSettingsActivityIcePhone1.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					case CONTACT_PICKER_REQUEST_ID_BASE + 2:
						bandSettingsActivityIcePhone2.setTag(phoneNumber);
						bandSettingsActivityIcePhone2.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					case CONTACT_PICKER_REQUEST_ID_BASE + 3:
						bandSettingsActivityIcePhone3.setTag(phoneNumber);
						bandSettingsActivityIcePhone3.setText(contactsUtil.getDisplayNameForNumber(phoneNumber));
						break;
					default:
						LOGGER.warn("Resolved contact to number for unknown ID {}", requestCode);
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	@SuppressWarnings("unused")
	public void onRemoveButtonClicked(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.band_settings_activity_remove_question);
		builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				new DisassociateTask().execute();
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}

	@SuppressWarnings("unused")
	public void onSaveButtonClicked(View view) {
		CurrentUser currentUser = App.getInstance().getCurrentUser();

		List<String> icePhoneNumbers = new ArrayList<>();
		if (bandSettingsActivityIcePhone1.getTag() != null) {
			icePhoneNumbers.add((String)bandSettingsActivityIcePhone1.getTag());
		}
		if (bandSettingsActivityIcePhone2.getTag() != null) {
			icePhoneNumbers.add((String)bandSettingsActivityIcePhone2.getTag());
		}
		if (bandSettingsActivityIcePhone3.getTag() != null) {
			icePhoneNumbers.add((String)bandSettingsActivityIcePhone3.getTag());
		}

		if (bandSettingsActivityDisplayName.getText().toString().trim().isEmpty()) {
			Toast.makeText(this, R.string.band_settings_activity_display_name_missing, Toast.LENGTH_SHORT).show();
			return;
		}
		if (bandSettingsActivityFirstName.getText().toString().trim().isEmpty()) {
			Toast.makeText(this, R.string.band_settings_activity_first_name_missing, Toast.LENGTH_SHORT).show();
			return;
		}
		if (bandSettingsActivityLastName.getText().toString().trim().isEmpty()) {
			Toast.makeText(this, R.string.band_settings_activity_last_name_missing, Toast.LENGTH_SHORT).show();
			return;
		}
		if (bandSettingsActivityAge.getText().toString().trim().isEmpty()) {
			Toast.makeText(this, R.string.band_settings_activity_age_missing, Toast.LENGTH_SHORT).show();
			return;
		}

		if(icePhoneNumbers.size() == 0){
			Toast.makeText(this, R.string.add_band_activity_ice_phone_required, Toast.LENGTH_SHORT).show();
			return;
		}

		try {
			int age = Integer.valueOf(bandSettingsActivityAge.getText().toString());
			if (age < 1 || age > 99) {
				Toast.makeText(this, R.string.band_settings_activity_age_missing, Toast.LENGTH_SHORT).show();
				return;
			}
		} catch (NumberFormatException e) {
			Toast.makeText(this, R.string.band_settings_activity_age_missing, Toast.LENGTH_SHORT).show();
			return;
		}

        try {
            int dailySteps = Integer.valueOf(bandSettingsActivityDailySteps.getText().toString());
        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.band_settings_activity_daily_steps_missing, Toast.LENGTH_SHORT).show();
            return;
        }

		SetBandPropertiesRequest setBandPropertiesRequest = new SetBandPropertiesRequest(
			currentUser.getUserId(),
			currentUser.getAuthenticationToken(),
			imei,
			bandSettingsActivityDisplayName.getText().toString(),
			bandSettingsActivityFirstName.getText().toString(),
			bandSettingsActivityLastName.getText().toString(),
			Integer.valueOf(bandSettingsActivityAge.getText().toString()),
			Integer.valueOf(bandSettingsActivityDailySteps.getText().toString()),
			bandSettingsActivityMeasurementInterval.getProgress() * 5 + 10,
			icePhoneNumbers
		);

		new SaveTask().execute(setBandPropertiesRequest);
	}

	private void updateMeasurementIntervalDisplay() {
		int minutes = bandSettingsActivityMeasurementInterval.getProgress() * 5 + 10;
		if (minutes < 60) {
			bandSettingsActivityMeasurementIntervalText.setText(String.format(Locale.getDefault(), getString(R.string
				.band_settings_activity_measurement_interval_value_min), minutes));
		} else {
			int hours = minutes / 60;
			minutes %= 60;
			bandSettingsActivityMeasurementIntervalText.setText(String.format(Locale.getDefault(), getString(R.string
				.band_settings_activity_measurement_interval_value_h_min), hours, minutes));
		}
	}

	private void disableAll() {
		bandSettingsActivityDisplayName.setEnabled(false);
		bandSettingsActivityFirstName.setEnabled(false);
		bandSettingsActivityLastName.setEnabled(false);
		bandSettingsActivityAge.setEnabled(false);
		bandSettingsActivityDailySteps.setEnabled(false);
		bandSettingsActivityRemoveButton.setEnabled(false);
		bandSettingsActivitySaveButton.setEnabled(false);
		bandSettingsActivityMeasurementInterval.setEnabled(false);
		bandSettingsActivityIcePhone1.setEnabled(false);
		bandSettingsActivityIcePhone2.setEnabled(false);
		bandSettingsActivityIcePhone3.setEnabled(false);

		bandSettingsActivityImei.setEnabled(false);

		bandSettingsActivitySpinner.setVisibility(View.VISIBLE);
	}

	private void enableAll() {
		bandSettingsActivityDisplayName.setEnabled(true);
		bandSettingsActivityFirstName.setEnabled(true);
		bandSettingsActivityLastName.setEnabled(true);
		bandSettingsActivityAge.setEnabled(true);
		bandSettingsActivityDailySteps.setEnabled(true);
		bandSettingsActivityRemoveButton.setEnabled(true);
		bandSettingsActivitySaveButton.setEnabled(true);
		bandSettingsActivityMeasurementInterval.setEnabled(true);
		bandSettingsActivityIcePhone1.setEnabled(true);
		bandSettingsActivityIcePhone2.setEnabled(true);
		bandSettingsActivityIcePhone3.setEnabled(true);

		bandSettingsActivityImei.setEnabled(false);

		bandSettingsActivitySpinner.setVisibility(View.GONE);
	}

	private void loadingFinishedError(){
		App.getInstance().setCurrentUser(null);
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	private class LoadTask extends ApiResponseTask {
		@Override
		protected Void doInBackground(Void... params) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					disableAll();
				}
			});

			CurrentUser currentUser = App.getInstance().getCurrentUser();
			String json;

			final GetBandPropertiesResponse getBandPropertiesResponse;
			try {
				json = new HttpClient(ApiEndPoint.GET_BAND_PROPERTIES).doPost(new GetBandPropertiesRequest(currentUser.getUserId(),
					currentUser.getAuthenticationToken(), imei));
				getBandPropertiesResponse = GetBandPropertiesResponse.fromString(json, GetBandPropertiesResponse.class);
			} catch (IOException e) {
				LOGGER.error("Failed to get band properties, server issue", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_load_failed, Toast.LENGTH_SHORT).show();
						bandSettingsActivitySpinner.setVisibility(View.GONE);
					}
				});
				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_load_failed, Toast.LENGTH_SHORT).show();
						bandSettingsActivitySpinner.setVisibility(View.GONE);
					}
				});
				return null;
			}

			if(isApiResponseCorrect(json)) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						bandSettingsActivityDisplayName.setText(getBandPropertiesResponse.getDisplayName());
						bandSettingsActivityFirstName.setText(getBandPropertiesResponse.getFirstName());
						bandSettingsActivityLastName.setText(getBandPropertiesResponse.getLastName());
						bandSettingsActivityImei.setText(String.valueOf(getBandPropertiesResponse.getBandImei()));
						bandSettingsActivityAge.setText(String.valueOf(getBandPropertiesResponse.getAge()));
						bandSettingsActivityDailySteps.setText(String.valueOf(getBandPropertiesResponse.getDailySteps()));
						bandSettingsActivityMeasurementInterval.setProgress((getBandPropertiesResponse.getMeasurementInterval() - 10) / 5);
						List<String> icePhoneNumbers = getBandPropertiesResponse.getIcePhoneNumbers();
						bandSettingsActivityIcePhone1.setTag(null);
						if (icePhoneNumbers.size() > 0) {
							try {

							bandSettingsActivityIcePhone1.setTag(icePhoneNumbers.get(0));
							bandSettingsActivityIcePhone1.setText(contactsUtil.getDisplayNameForNumber(icePhoneNumbers.get(0)));
							} catch(Exception e){
								e.printStackTrace();
								bandSettingsActivityIcePhone1.setText(icePhoneNumbers.get(0));
							}
						}
						bandSettingsActivityIcePhone2.setTag(null);
						if (icePhoneNumbers.size() > 1) {
							try {
								bandSettingsActivityIcePhone2.setTag(icePhoneNumbers.get(1));
								bandSettingsActivityIcePhone2.setText(contactsUtil.getDisplayNameForNumber(icePhoneNumbers.get(1)));
							} catch(Exception e){
								e.printStackTrace();
								bandSettingsActivityIcePhone1.setText(icePhoneNumbers.get(1));
							}
						}
						bandSettingsActivityIcePhone3.setTag(null);
						if (icePhoneNumbers.size() > 2) {
							try {
								bandSettingsActivityIcePhone3.setTag(icePhoneNumbers.get(2));
								bandSettingsActivityIcePhone3.setText(contactsUtil.getDisplayNameForNumber(icePhoneNumbers.get(2)));
							} catch(Exception e){
								e.printStackTrace();
								bandSettingsActivityIcePhone1.setText(icePhoneNumbers.get(2));
							}
						}

						TextView bandSettingsActivityConfigurationSyncStatus = (TextView) findViewById(R.id
								.band_settings_activity_configuration_sync_status);
						if (getBandPropertiesResponse.isConfigurationSynced()) {
							bandSettingsActivityConfigurationSyncStatus.setText(R.string.band_settings_activity_sync_ok);
							bandSettingsActivityConfigurationSyncStatus.setTextColor(ColorUtil.getColor(BandSettingsActivity.this, R.color.dark_green));
						} else {
							bandSettingsActivityConfigurationSyncStatus.setText(R.string.band_settings_activity_sync_pending);
							bandSettingsActivityConfigurationSyncStatus.setTextColor(ColorUtil.getColor(BandSettingsActivity.this, R.color.dark_red));
						}

						enableAll();
					}
				});

			}
			else {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.login_fragment_multilogin, Toast.LENGTH_SHORT).show();
					}
				});
				loadingFinishedError();
			}
			return null;
		}
	}



	private class SaveTask extends AsyncTask<SetBandPropertiesRequest, Void, Void> {
		@Override
		protected Void doInBackground(final SetBandPropertiesRequest... params) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					disableAll();
				}
			});

			SetBandPropertiesResponse setBandPropertiesResponse;
			try {
				String json = new HttpClient(ApiEndPoint.SET_BAND_PROPERTIES).doPost(params[0]);
				setBandPropertiesResponse = SetBandPropertiesResponse.fromString(json, SetBandPropertiesResponse.class);
			} catch (IOException e) {
				LOGGER.error("Failed to save band, server issue", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_save_failed, Toast.LENGTH_SHORT).show();
						enableAll();
					}
				});
				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_save_failed, Toast.LENGTH_SHORT).show();
						enableAll();
					}
				});
				return null;
			}

			if (!setBandPropertiesResponse.isBandUpdated()) {
				LOGGER.warn("Band save failed: not updated by server");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_save_failed, Toast.LENGTH_SHORT).show();
						enableAll();
					}
				});
				return null;
			}

			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(BandSettingsActivity.this);
			final int displayBandAs = Integer.parseInt(preferences.getString(SettingsKeys.PREF_BAND_DISPLAY, "0"));
			final String displayName;
			switch (displayBandAs) {
				case 1:
					displayName = params[0].getLastName() + " " + params[0].getFirstName();
					break;
				case 2:
					displayName = params[0].getFirstName() + " " + params[0].getLastName();
					break;
				case 0:
				default:
					displayName = params[0].getDisplayName();
					break;
			}

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					enableAll();
					setResult(RESULT_OK, new Intent(displayName));
					finish();
				}
			});
			return null;
		}
	}

	private class DisassociateTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					disableAll();
				}
			});

			CurrentUser currentUser = App.getInstance().getCurrentUser();
			try {
				String json = new HttpClient(ApiEndPoint.DISASSOCIATE_BAND).doPost(new DisassociateBandRequest(currentUser.getUserId(),
					currentUser.getAuthenticationToken(), imei));
				DisassociateBandResponse.fromString(json, DisassociateBandResponse.class);
			} catch (IOException e) {
				LOGGER.error("Failed to disassociate band, server issue", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_remove_failed, Toast.LENGTH_SHORT).show();
						enableAll();
					}
				});
				return null;
			} catch (JsonException e) {
				LOGGER.error("Failed to deserialize JSON data", e);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(BandSettingsActivity.this, R.string.band_settings_activity_remove_failed, Toast.LENGTH_SHORT).show();
						enableAll();
					}
				});
				return null;
			}

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					enableAll();
					setResult(RESULT_DELETED);
					finish();
				}
			});
			return null;
		}
	}


	private void showAlertDialogNumber(int code){
		FrameLayout layout = new FrameLayout(context);
		FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(40,20,40,20);
		editTextAlert = new EditText(context);
		editTextAlert.setHint(R.string.add_band_activity_ice_phone_prompt);
		editTextAlert.setInputType(InputType.TYPE_CLASS_NUMBER);
		editTextAlert.setPadding(15, 15, 15, 15);
		editTextAlert.setLayoutParams(params);
		layout.addView(editTextAlert);
		this.requestCode = code;
		new AlertDialog.Builder(this)
				.setTitle(R.string.add_band_activity_ice_phone_prompt_title)
				.setView(layout)
				.setPositiveButton(R.string.add_band_activity_ice_phone_prompt_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						result = editTextAlert.getText().toString();
						result = result.replaceAll("[^0-9\\+]", "");
						addNumberIndependently(requestCode, result);
					}
				})
				.setNegativeButton(R.string.add_band_activity_ice_phone_prompt_cancel, null)
				.create()
				.show();
	}

	private void addNumberIndependently(int requestCode, String phoneNumber){
		switch (requestCode) {
			case 1:
				bandSettingsActivityIcePhone1.setTag(phoneNumber);
				bandSettingsActivityIcePhone1.setText(phoneNumber);
				break;
			case 2:
				bandSettingsActivityIcePhone2.setTag(phoneNumber);
				bandSettingsActivityIcePhone2.setText(phoneNumber);
				break;
			case 3:
				bandSettingsActivityIcePhone3.setTag(phoneNumber);
				bandSettingsActivityIcePhone3.setText(phoneNumber);
				break;
			default:
				LOGGER.warn("Resolved contact to number for unknown ID {}", requestCode);
		}
	}
}
