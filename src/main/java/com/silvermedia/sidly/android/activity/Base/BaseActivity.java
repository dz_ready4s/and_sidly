package com.silvermedia.sidly.android.activity.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.fragment.BandUpgradeDialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by piotr.sobon@ready4s.pl on 2016-05-12.
 */
public class BaseActivity extends Activity {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseActivity.class);
    public static final String FILTER_UPGRADE = "upgradeFilter";
    public static final String UPGRADE_MESSAGE = "upgradeMessage";
    public static final String UPGRADE_VERSION = "upgradeVersion";

    protected BroadcastReceiver mUpgradeFirmwareReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUpgradeFirmwareReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(App.getInstance().getCurrentUser() == null){
                    LOGGER.warn("Receive intent but user is not logged");
                    return;
                }
                BandUpgradeDialog upgradeDialog = new BandUpgradeDialog();
                upgradeDialog.setArguments(intent.getExtras());
                upgradeDialog.show(getFragmentManager(),"UpgradeFragment");
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mUpgradeFirmwareReceiver != null) {
            LocalBroadcastManager.getInstance(this)
                    .registerReceiver(mUpgradeFirmwareReceiver, new IntentFilter(FILTER_UPGRADE));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mUpgradeFirmwareReceiver!=null){
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpgradeFirmwareReceiver);
        }

    }
}
