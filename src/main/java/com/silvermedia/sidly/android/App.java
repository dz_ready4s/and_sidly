package com.silvermedia.sidly.android;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.crashlytics.android.Crashlytics;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.silvermedia.sidly.android.cdm.ListBandsResponse;
import com.silvermedia.sidly.android.gcm.InstanceIdListenerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.LevelFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import io.fabric.sdk.android.Fabric;

public class App extends Application {
    private static final String PREF_VERSION_CODE = "prefVersionCode";

	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	private static App instance;

	private static final String CACHE_KEY = "__associatedBands";

	private CurrentUser currentUser = null;

	private Cache<String, List<ListBandsResponse.BandDetails>> bandDetailsCache;

	private Thread.UncaughtExceptionHandler androidUncaughtExceptionHandler;

	public static App getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		instance = this;

		androidUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                LOGGER.error("Application crash", ex);
                androidUncaughtExceptionHandler.uncaughtException(thread, ex);
            }
        });

		setupLogging();

		LOGGER.info("Initializing application on '{}', locale={}, debug={}", getDeviceInformation(), getString(R.string.app_locale), BuildConfig
                .DEBUG);

		bandDetailsCache = CacheBuilder.newBuilder().maximumSize(1).expireAfterWrite(5, TimeUnit.MINUTES).build();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String gcmInstanceIdTest = preferences.getString(SettingsKeys.RT_GCM_INSTANCE_ID, null);
        int verCode = 0;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(),0);
            verCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        int prefVerCode = preferences.getInt(PREF_VERSION_CODE,0);
        if(prefVerCode!=verCode){
            preferences.edit().clear().commit();
            preferences.edit().putString(SettingsKeys.RT_GCM_INSTANCE_ID,gcmInstanceIdTest).commit();
            preferences.edit().putInt(PREF_VERSION_CODE,verCode).commit();
        }
		PreferenceManager.setDefaultValues(this, R.xml.preferences, true);

        if (preferences.contains(SettingsKeys.RT_CURRENT_USER)) {
			String json = preferences.getString(SettingsKeys.RT_CURRENT_USER, null);
			if (json != null) {
				try {
					currentUser = new Gson().fromJson(json, CurrentUser.class);
					LOGGER.info("Loaded current user: {}", currentUser);
				} catch (JsonParseException e) {
					LOGGER.warn("Failed to parse JSON data for current user", e);
				}
			}
		}

		LOGGER.info("Checking GCM instance ID");
		String gcmInstanceId = preferences.getString(SettingsKeys.RT_GCM_INSTANCE_ID, null);
		if (gcmInstanceId == null) {
			InstanceIdListenerService.generateNewInstanceId();
		} else {
			LOGGER.info("GCM instance ID from prefs: {}", gcmInstanceId);
		}
	}

	private void setupLogging() {
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		LoggerContext loggerContext = (LoggerContext)LoggerFactory.getILoggerFactory();
		loggerContext.reset();

		PatternLayoutEncoder patternLayoutEncoderForLogcat = new PatternLayoutEncoder();
		patternLayoutEncoderForLogcat.setContext(loggerContext);
		patternLayoutEncoderForLogcat.setPattern("%msg%n");
		patternLayoutEncoderForLogcat.start();

		LevelFilter logcatFilter = new LevelFilter();
		logcatFilter.setLevel(Level.INFO);
		logcatFilter.start();

		LogcatAppender logcatAppender = new LogcatAppender();
		logcatAppender.setContext(loggerContext);
		logcatAppender.setEncoder(patternLayoutEncoderForLogcat);
		logcatAppender.addFilter(logcatFilter);
		logcatAppender.start();

		root.setLevel(Level.ALL);
		root.addAppender(logcatAppender);

		if (BuildConfig.DEBUG) {
			PatternLayoutEncoder patternLayoutEncoderForFile = new PatternLayoutEncoder();
			patternLayoutEncoderForFile.setContext(loggerContext);
			patternLayoutEncoderForFile.setPattern("%d{YYYY.MM.dd HH:mm:ss.SSS} %-5level %logger{36} - %msg%n");
			patternLayoutEncoderForFile.start();

			FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
			fileAppender.setContext(loggerContext);
			fileAppender.setFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getApplicationInfo().packageName + ".log");
			fileAppender.setEncoder(patternLayoutEncoderForFile);
			fileAppender.setAppend(true);
			fileAppender.start();

			root.addAppender(fileAppender);
		}
	}

	public String getDeviceInformation() {
		return Build.MANUFACTURER + " " + Build.MODEL;
	}

	public int getNumberOfCores() {
		final Pattern pattern = Pattern.compile("^cpu[0-9]+$");
		int coreCount = new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pattern.matcher(pathname.getName()).matches();
			}
		}).length;
		return Math.max(coreCount, 1);
	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean notifyAboutAlarms = preferences.getBoolean(SettingsKeys.PREF_NOTIFICATIONS_EMERGENCY, true);
		SharedPreferences.Editor editor = preferences.edit();
		if (currentUser == null) {
			editor.remove(SettingsKeys.RT_CURRENT_USER);
		} else {
			editor.putString(SettingsKeys.RT_CURRENT_USER, new Gson().toJson(currentUser));
		}
		editor.apply();
	}

	public String getGcmInstanceId() {
		return PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKeys.RT_GCM_INSTANCE_ID, null);
	}

	public Cache<String, List<ListBandsResponse.BandDetails>> getAssociatedBandsCache() {
		return bandDetailsCache;
	}

	public void clearAssociatedBandsCache(){
		getAssociatedBandsCache().put(CACHE_KEY, new ArrayList<ListBandsResponse.BandDetails>());
	}
}
