package com.silvermedia.sidly.android;

import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorRes;

public class ColorUtil {
	private ColorUtil() { }

	public static int getColor(Context context, @ColorRes int colorResId) {
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
			//noinspection deprecation
			return context.getResources().getColor(colorResId);
		} else {
			return context.getColor(colorResId);
		}
	}
}
