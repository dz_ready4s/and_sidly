package com.silvermedia.sidly.android.gcm;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.silvermedia.sidly.android.AlarmNotification;
import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.cdm.JsonException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListenerService extends GcmListenerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ListenerService.class);

	@Override
	public void onMessageReceived(String from, Bundle data) {
		LOGGER.debug("Received new GCM message from {}: {}", from, data);
		if(App.getInstance().getCurrentUser() == null){
			return;
		}

		String message = data.getString("data");
		if (message == null || message.isEmpty()) {
			LOGGER.warn("Empty data payload from {}", from);
			return;
		}

		try {
			new AlarmNotification(AlarmNotification.parse(message)).show();
		} catch (JsonException e) {
			LOGGER.warn("Failed to parse new measurement notification", e);
		}
	}
}
