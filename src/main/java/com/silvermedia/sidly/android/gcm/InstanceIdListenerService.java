package com.silvermedia.sidly.android.gcm;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.silvermedia.sidly.android.App;
import com.silvermedia.sidly.android.CurrentUser;
import com.silvermedia.sidly.android.R;
import com.silvermedia.sidly.android.SettingsKeys;
import com.silvermedia.sidly.android.remote.ApiEndPoint;
import com.silvermedia.sidly.android.remote.HttpClient;
import com.silvermedia.sidly.android.cdm.JsonException;
import com.silvermedia.sidly.android.cdm.UpdateInstanceIdRequest;
import com.silvermedia.sidly.android.cdm.UpdateInstanceIdResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class InstanceIdListenerService extends InstanceIDListenerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(InstanceIdListenerService.class);

	public static void generateNewInstanceId() {
		LOGGER.info("Creating new instance ID");

		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				App app = App.getInstance();

				String gcmInstanceId;
				while (true) {
					try {
						gcmInstanceId = InstanceID.getInstance(app).getToken(app.getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging
							.INSTANCE_ID_SCOPE, null);

						LOGGER.info("Got GCM instance ID: {}", gcmInstanceId);
						SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString(SettingsKeys.RT_GCM_INSTANCE_ID, gcmInstanceId);
						editor.apply();
						break;
					} catch (IOException e) {
						LOGGER.error("Failed to get Instance ID, retrying after 5 seconds...", e);

						try {
							Thread.sleep(5000);
						} catch (InterruptedException ignore) { }
					}
				}

				CurrentUser currentUser = app.getCurrentUser();
				if (currentUser == null) {
					LOGGER.warn("Skipping GCM instance ID update due to current user being null");
					return null;
				}

				final UpdateInstanceIdResponse updateInstanceIdResponse;
				try {
					String json = new HttpClient(ApiEndPoint.UPDATE_INSTANCE_ID).doPost(new UpdateInstanceIdRequest(currentUser.getUserId(),
						currentUser.getAuthenticationToken(), gcmInstanceId));
					updateInstanceIdResponse = UpdateInstanceIdResponse.fromString(json, UpdateInstanceIdResponse.class);
				} catch (IOException e) {
					LOGGER.error("Failed to update GCM instance ID, server issue", e);
					return null;
				} catch (JsonException e) {
					LOGGER.error("Failed to deserialize JSON data", e);
					return null;
				}

				if (!updateInstanceIdResponse.isInstanceIdUpdated()) {
					LOGGER.error("Failed to update GCM instance ID, refused by server");
				} else {
					LOGGER.info("Updated GCM instance ID on server");
				}

				return null;
			}
		}.execute();
	}

	@Override
	public void onTokenRefresh() {
		LOGGER.info("Refreshing Instance ID token");
		generateNewInstanceId();
	}
}
